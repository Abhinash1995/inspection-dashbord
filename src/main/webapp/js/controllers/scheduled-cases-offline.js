jaduApp.controller('ScheduledCasesOfflineController',['$scope', '$timeout', '$location', '$compile', 'ajaxService', '$q','$mdDialog',  function($scope, $timeout, $location, $compile, ajaxService, $q, $mdDialog) {

  $scope.refreshDeamonActive = false;
  $scope.appLink=null;
  
  

  $scope.fetchScheduledCases = function(spinnerText){
    $q.all([
      ajaxService.getOfflineScheduledCases(null, spinnerText),
      ajaxService.getVehicleTypes(null, 'Please Wait!...')
    ])
    .then(function(res){
      $scope.qcCases = res[0].data;
      $scope.vehicleTypes = [];
      
      _.map(_.uniqBy(res[1].data, 'id'), function (item) {
              $scope.vehicleTypes.push(item);
            });

      if($scope.qcCases != undefined){

        $scope.qcCases.forEach(function(qcCase, index){
          if(qcCase.inspectionType == 'SELF_INSPECT'){
            $scope.qcCases[index].inspectionTypeDisplay = 'Self';
          }
          else
          if(qcCase.inspectionType == 'ASSIGN_TO_CUSTOMER'){
            $scope.qcCases[index].inspectionTypeDisplay = 'Customer';
            if($scope.qcCases[index].inspectionTime == null){
              $scope.qcCases[index].inspectionTime = $scope.qcCases[index].creationTime;
            }
          }
          else
          if(qcCase.inspectionType == 'ASSIGN_TO_INSPECTOR'){
            $scope.qcCases[index].inspectionTypeDisplay = 'Inspector';
          }
          // console.log('case ', index, qcCase, $scope.qcCases[index]);
        });

        $scope.flagCalculator();
        // $scope.refreshDeamon();
      }
      else{
        console.log('No Scheduled Cases Recieved!');
      }
    });
  };
  
  $scope.flagOnlineCalculator = function(){
	    $scope.currentTime = moment((moment().unix())*1000);
	      for(i=0;i<$scope.qcOflineCases.length;i++){
	        if($scope.qcOflineCases[i].inspectionStartTime == null){// insepection
																	// not
																// started
	          var insTime = moment($scope.qcOflineCases[i].inspectionTime);
	          var diff = $scope.currentTime.diff(insTime, 'minutes', true);
	          if(diff >= 15 && diff < 30){
	            $scope.qcOflineCases[i].flagStatus = 'b-status-orange';
	          }
	          else
	          if(diff >= 30){
	            $scope.qcOflineCases[i].flagStatus = 'a-status-red';
	          }
	          else{
	            $scope.qcOflineCases[i].flagStatus = '';
	          }
	        }
	        else{
	          var insStartTime = moment($scope.qcOflineCases[i].inspectionStartTime);

	          var diff = $scope.currentTime.diff(insStartTime, 'minutes', true);

	          if(diff >= 15 && diff < 30){
	            $scope.qcOflineCases[i].flagStatus = 'b-status-orange';
	          }
	          else
	          if(diff >= 30){
	            $scope.qcOflineCases[i].flagStatus = 'a-status-red';
	          }
	          else{
	            $scope.qcOflineCases[i].flagStatus = '';
	          }
	        }
	    }



	    $timeout(function(){
	      $scope.flagOnlineCalculator();
	    },6000)

	  }

// angular.element(document).ready(function () {
// $scope.fetchScheduledCases("Fetching scheduled cases. Please wait!");
// });

  $scope.flagCalculator = function(){
    $scope.currentTime = moment((moment().unix())*1000);
      for(i=0;i<$scope.qcCases.length;i++){
        if($scope.qcCases[i].inspectionStartTime == null){// insepection not
															// started
          var insTime = moment($scope.qcCases[i].inspectionTime);
          var diff = $scope.currentTime.diff(insTime, 'minutes', true);
          if(diff >= 15 && diff < 30){
            $scope.qcCases[i].flagStatus = 'b-status-orange';
          }
          else
          if(diff >= 30){
            $scope.qcCases[i].flagStatus = 'a-status-red';
          }
          else{
            $scope.qcCases[i].flagStatus = '';
          }
        }
        else{
          var insStartTime = moment($scope.qcCases[i].inspectionStartTime);

          var diff = $scope.currentTime.diff(insStartTime, 'minutes', true);

          if(diff >= 15 && diff < 30){
            $scope.qcCases[i].flagStatus = 'b-status-orange';
          }
          else
          if(diff >= 30){
            $scope.qcCases[i].flagStatus = 'a-status-red';
          }
          else{
            $scope.qcCases[i].flagStatus = '';
          }
        }
    }



    $timeout(function(){
      $scope.flagCalculator();
    },6000)

  }
  
  

  $scope.refreshDeamon = function(){

    $timeout(function(){
      $scope.fetchScheduledCases('refresh');
    },60000)
  }

  $scope.newCaseData = {};

  $scope.editCase = function(id){
       $scope.editCaseId = id;
        $scope.currentCase = getCaseById(id);

        var timestamp = moment.unix($scope.currentCase.inspectionTime/1000);
        $scope.currentCase.inspectionTimeModel = timestamp;
        $scope.currentCase.inspectionTime = ( timestamp.format("YYYY/MM/DD HH:mm:ss") );

        $scope.newCaseData.newCustomerPhoneNumber = $scope.currentCase.customerPhoneNumber;
        $scope.newCaseData.newRequestorPhoneNumber = $scope.currentCase.requestorPhoneNumber;
        $scope.newCaseData.newVehicleNumber = $scope.currentCase.vehicleNumber;
        $("#modal-2").modal('toggle');
    };

  $scope.allotCase = function(id){
        $scope.currentCase = getCaseById(id);

        var timestamp = moment.unix($scope.currentCase.inspectionTime/1000);
        $scope.currentCase.inspectionTimeModel = timestamp;
        $scope.currentCase.inspectionTime = ( timestamp.format("YYYY/MM/DD HH:mm:ss") );
        $("#modal-3").modal('toggle');
    };

  $scope.cancelCase = function(){

      $("#modal-2").modal('hide');
      ajaxService.cancelCase({case_id:$scope.currentCase.id},'Cancelling Case. Please Wait...')
      .then(function(res){
        alert('Case Cancelled!');
        $scope.fetchScheduledCases("Updating Case. Please wait!");
      })
      .catch(function(e){
        console.error('Case Cancellation Error',e);
      })
    };

  $scope.rescheduleCaseCallback = function(){
      console.log('rescheduleCaseCallback called');
      $("#modal-2").modal('hide');
      $scope.fetchScheduledCases("Updating Case. Please wait!");
    };

  $scope.closeCaseCallback = function(){
      console.log('rescheduleCaseCallback called');
      $("#modal-2").modal('hide');
      $scope.fetchScheduledCases("Updating Case. Please wait!");
    };

  function getCaseById(id){
    for(var i=0; i< $scope.qcCases.length; i++){
      if($scope.qcCases[i].id === id)
        return $scope.qcCases[i];
    }
    return null;
  }

  $scope.showTimeline = function(){
      $("#timelineModal").modal('toggle');
    }

// -------------------------------------------------

  $scope.updateCaseDetails = function(){
    var dataCPN = {
      case_id:$scope.editCaseId,
      customer_phone_number:$scope.newCaseData.newCustomerPhoneNumber
    }
    var dataRPN = {
      case_id:$scope.editCaseId,
      phone_number:$scope.newCaseData.newRequestorPhoneNumber
    }
    var dataVN = {
      case_id:$scope.editCaseId,
      vehicle_number:$scope.newCaseData.newVehicleNumber
    }
    console.log('=================>***********',dataVN,dataCPN,dataRPN);
    $q.all([
      ajaxService.updateCustomerPhoneNumber(dataCPN),
      ajaxService.updateAgentPhoneNumber(dataRPN),
      ajaxService.updateVehicleNumber(dataVN)
    ])
    .then(function(res){
      alert('Case Details Updated!');
      $scope.fetchScheduledCases('refresh');
    })
    .catch(function(){
      alert('Error in Case Details!');
    });

  };

  $scope.manualQCInit = function(data){
      $scope.selectedCaseForManualQc = data;
      $("#manual-qc-container").modal('show');
      $("#manual-qc-container .modal-body").html($compile("<bucket-tree callback='onFileSelect(file)'></bucket-tree>")($scope));
  };

  $scope.onFileSelect = function(file){
        $("#manual-qc-container").modal('hide');
        var ask = window.confirm("Do you want to use file: " + file + " for QC of case: " + $scope.selectedCaseForManualQc.id);
        if (ask) {
            ajaxService.manualQC({
                case_id : $scope.selectedCaseForManualQc.id,
                zip_file : file
            }, "Doing QC with file selected. Please wait!").then(function(){
                alert("QC done successfully");
            }, function(err){
                alert("Unable to use file for QC. Please try again later.");
                console.log(err);
            });
        }
  };

    $scope.showComments = function(data){        
      
       $scope.selectedCaseForManualQc = data;
      
       $("#manual-qc-container3").modal('show');
       $("#manual-qc-container3 .modal-body").html($compile("<case-comments inspection-case='selectedCaseForManualQc'></case-comments>")($scope));
    };
    $scope.redirectToGoogle = function () {
        $window.open('https://www.google.com', '_blank');
    };
    
    $scope.whatsAppCustomer = function(data){  
    	 ajaxService.getAppLink({
                           case_id: data.id,
                    company: data.insuranceCompanyName
                   
                }, "Fetching App Link").then(function(resp){
                    console.log(resp);
                   var link = 'https://api.whatsapp.com/send?phone=91';var message='&text=Dear%20Sir%2FMadam%2C%0A%0ASelf-Inspection%20request%20created%20for%20vehicle%20';var terminal='.%0A%0APlease%20view%20the%20attached%20PDF%20before%20taking%20the%20photos%20of%20your%20vehicle.%20Make%20sure%20you%20know%20the%20Chassis%20%26%20VIN%20plate%20location%20before%20starting%20the%20inspection.%20%0A%0A%E0%A4%87%E0%A4%82%E0%A4%B8%E0%A5%8D%E0%A4%AA%E0%A5%87%E0%A4%95%E0%A5%8D%E0%A4%B6%E0%A4%A8%20%E0%A4%B8%E0%A5%8D%E0%A4%9F%E0%A4%BE%E0%A4%B0%E0%A5%8D%E0%A4%9F%20%E0%A4%95%E0%A4%B0%E0%A4%A8%E0%A5%87%20%E0%A4%B8%E0%A5%87%20%E0%A4%AA%E0%A4%B9%E0%A4%B2%E0%A5%87%20Chassis%20%26%20VIN%20%E0%A4%AA%E0%A5%8D%E0%A4%B2%E0%A5%87%E0%A4%9F%20%E0%A4%95%E0%A5%80%20%E0%A4%B2%E0%A5%8B%E0%A4%95%E0%A5%87%E0%A4%B6%E0%A4%A8%20%E0%A5%9B%E0%A4%B0%E0%A5%82%E0%A4%B0%20%E0%A4%9C%E0%A4%BE%E0%A4%A8%20%E0%A4%B2%E0%A5%87%E0%A4%82.%20%E0%A4%AC%E0%A4%BF%E0%A4%A8%E0%A4%BE%20Chassis%20%20%26%20VIN%20%E0%A4%95%E0%A5%80%20%E0%A4%AB%E0%A5%8B%E0%A4%9F%E0%A5%8B%20%E0%A4%95%E0%A5%87%20%E0%A4%87%E0%A4%82%E0%A4%B8%E0%A5%8D%E0%A4%AA%E0%A5%87%E0%A4%95%E0%A5%8D%E0%A4%B6%E0%A4%A8%20Recommend%20%E0%A4%A8%E0%A4%B9%E0%A5%80%E0%A4%82%20%E0%A4%B9%E0%A5%8B%E0%A4%97%E0%A4%BE.%0A%0ACall%207290049100%20for%20any%20support.';var custMessage='&text=Dear%20Customer%2C%20%0A%0AWe%20have%20received%20inspection%20request%20for%20your%20vehicle%20';var custAppMessage='.%0A%0APlease%20download%20the%20Self%20Inspection%20Application%20from%20link%3A%20';var custTerminal='.%20%0A%0APlease%20view%20the%20attached%20PDF%20before%20taking%20the%20photos%20of%20your%20vehicle.%20Make%20sure%20you%20know%20the%20Chassis%20%26%20VIN%20plate%20location%20before%20starting%20the%20inspection.%20%0A%0A%E0%A4%87%E0%A4%82%E0%A4%B8%E0%A5%8D%E0%A4%AA%E0%A5%87%E0%A4%95%E0%A5%8D%E0%A4%B6%E0%A4%A8%20%E0%A4%B8%E0%A5%8D%E0%A4%9F%E0%A4%BE%E0%A4%B0%E0%A5%8D%E0%A4%9F%20%E0%A4%95%E0%A4%B0%E0%A4%A8%E0%A5%87%20%E0%A4%B8%E0%A5%87%20%E0%A4%AA%E0%A4%B9%E0%A4%B2%E0%A5%87%20Chassis%20%20%26%20VIN%20%E0%A4%AA%E0%A5%8D%E0%A4%B2%E0%A5%87%E0%A4%9F%20%E0%A4%95%E0%A5%80%20%E0%A4%B2%E0%A5%8B%E0%A4%95%E0%A5%87%E0%A4%B6%E0%A4%A8%20%E0%A5%9B%E0%A4%B0%E0%A5%82%E0%A4%B0%20%E0%A4%9C%E0%A4%BE%E0%A4%A8%20%E0%A4%B2%E0%A5%87%E0%A4%82.%20%E0%A4%AC%E0%A4%BF%E0%A4%A8%E0%A4%BE%20Chassis%20%20%26%20VIN%20%E0%A4%95%E0%A5%80%20%E0%A4%AB%E0%A5%8B%E0%A4%9F%E0%A5%8B%20%E0%A4%95%E0%A5%87%20%E0%A4%87%E0%A4%82%E0%A4%B8%E0%A5%8D%E0%A4%AA%E0%A5%87%E0%A4%95%E0%A5%8D%E0%A4%B6%E0%A4%A8%20Recommend%20%E0%A4%A8%E0%A4%B9%E0%A5%80%E0%A4%82%20%E0%A4%B9%E0%A5%8B%E0%A4%97%E0%A4%BE.%0A%0ACall%207290049100%20for%20any%20support.';
					  /*
						 * var link =
						 * 'https://api.whatsapp.com/send?phone=91';var
						 * salutation='&text=Dear%20Customer';var
						 * message='%2C%20We%20have%20received%20inspection%20request%20for%20your%20vehicle%20';var
						 * terminal1='.%20Please%20download%20the%20Self%20Inspection%20Application%20from%20link%20%3A%20';var
						 * terminal2='Please%20view%20the%20attached%20PDF%20before%20taking%20the%20photos%20of%20your%20vehicle.%20Make%20sure%20you%20know%20the%20Chassis%20%26%20VIN%20plate%20location%20before%20starting%20the%20inspection.%20Call%207290049100%20for%20any%20support.';
						 */ window.open('link'+data.customerPhoneNumber+'custMessage'+data.vehicleNumber+'custAppMessage'+resp+'custTerminal',"_blank");
					  
                    }).catch( function(a){
                    alert("Unable to fetch link!",a);
                   			  
                    
                })
            }
            

    $scope.uploadInspectionFileModel = function(data){
        $scope.selectedCaseForManualQc = data;
      
        $("#manual-qc-container2").modal('show');
    };
  
    $scope.showComments = function(data){        
      
       $scope.selectedCaseForManualQc = data;
      
       $("#manual-qc-container3").modal('show');
       $("#manual-qc-container3 .modal-body").html($compile("<case-comments inspection-case='selectedCaseForManualQc'></case-comments>")($scope));
    };
  
    $scope.uploadFileManually = function(){
        var file = document.getElementById('manualUploadFile').files[0];
        var formData = new FormData();
        formData.append('case_file', file);
        formData.append('case_id', $scope.selectedCaseForManualQc.id);

        console.log(formData)

        $q.all([
                ajaxService.uploadInspectionFile(formData, "Uploading file. Please wait!"),
                
        ]).then(
                function(){
                    ajaxService.reupload($scope.selectedCaseForManualQc.id, "Uploading file. Please wait!");
                    alert("File uploaded successfully!");
                    $("#manual-qc-container2").modal('hide');
                    $location.url('/qc');
                }, function(err){

                    if(err.data.error_message)
                        alert(err.data.error_message);
                    else
                        alert("Unable to upload file!");
                    console.log(err);
                });
    };
    
    $scope.moveToQC  = function(ic){
        $scope.selectedCaseForQC = ic;
        $("#move-to-qc").modal('show');
        $("#move-to-qc .modal-title").html('Move case ' + ic + " to QC!");
    };
    
    $scope.moveToQCSubmit = function(){
        ajaxService.moveToQC({
            case_id: $scope.selectedCaseForQC,
            vehicle_type: $scope.selectedCaseForQCVehicleType
        }, "Moving case "+$scope.selectedCaseForQC+" to QC. Please wait!").then(function(){
            $("#move-to-qc").modal('hide');
            $(".modal-backdrop").hide();
            alert("Successfully moved to QC!");
            
            window.location.href = '#/qc';
        }, function(a){
            console.log(a);
            alert("Unable to update!");
        })
    };
    
  // Trigger
    $scope.radioChanged = function(value){
    	var data = {
    			secret_key:"magic@W!mwisure",
        		last_one_day:true,
        		self_inspection:true
        }
    	if(value=="start"){
    		data.self_inspection = true;   	      
    	    $("#initiate-system-call-offline").modal('show');
    	    $("#initiate-system-call-offline .modal-body").html($compile("<initiate-system-call-offline inspection-case='initiateSysCall'></initiate-system-call-offline>")($scope));

    	}
    	if(value=="stop"){
    		ajaxService.stopSystemCall({    		
    			case_type:"offline"
                },'Stoping initiated system calls')
    		.then(function(result){
        		alert("System call has been stopped successfully");
        		console.log(result)
        	}).catch(function(err){
        		console.log(err);
        	})
    	}
    }
    $scope.getOTP = function(customerPhoneNumber) {
		ajaxService
				.getUnexpiredOTP(
						{
							 username : customerPhoneNumber
						},
						"Fetching data. Please wait!")
				.then(
						function(result) {
							var data = result.data;
							$scope.otps = data;
							 $("#modal-n").modal('toggle');
						},
						function(err) {
							console.log(err);
							alert(err.data.error_message);
						});
	}; 
	
	 $scope.sendCaseCreatedSMS = function(caseId) {
			ajaxService
					.sendCaseCreatedSMS(
							{
								case_id : caseId
							},
							"Sending message, Please wait...")
					.then(
							function() {
				                alert("Message sent successfully");
<<<<<<< HEAD

=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
							},
							function(err) {
								console.log(err);
								alert(err.data);
							});
		};    
    

}]);
