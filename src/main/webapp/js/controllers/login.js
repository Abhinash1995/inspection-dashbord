jaduApp.controller('LoginController',['$scope', '$timeout', '$location', '$compile' , '$auth', '$q', function($scope, $timeout, $location, $compile, $auth, $q) {

  console.log('This is login controller');
    $scope.doLogin = function(){
        var user = {
            email: $scope.username,
            password: $scope.password
        };

        console.log(user);

        $auth.login(user, {
            method: 'POST',
            url: 'oauth/token'
        })
        .then(function(response) {
          console.log('$auth.login response===> ',response);
          // Redirect user here after a successful log in.
        })
        .catch(function(response) {
            console.log(response);
          // Handle errors here, such as displaying a notification
          // for invalid email and/or password.
        });

    };
}]);
