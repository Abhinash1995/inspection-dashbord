/* global angular */

jaduApp.controller('QCCheckController',['$scope', '$timeout', '$location', '$compile' , '$auth', '$q','ajaxService','NgTableParams','$routeParams','Lightbox', function($scope, $timeout, $location, $compile, $auth, $q, ajaxService, NgTableParams, $routeParams, Lightbox) {

    $scope.caseId = $routeParams.id;
    $scope.logQcOpen = true;
    $scope.uniqueCaseQuestion = [];
	$scope.unique = [];    
	$scope.recomend = [];
    $scope.activeGroup = "details";

    $scope.groups = [
        {id : "details", value : "Company, Agent and Inspection Details", tags : []},
        {id : "vehicle-details", value : "Vehicle Details", tags : ["input"]},
        {id : "coordinates", value : "Coordinates", tags : []},
        {id : "video", value : "Video", tags : ["video"], showCount : false},
        {id : "front", value : "Front", tags : ["front"], showCount : true, questionTag: "front"},
        {id : "left", value : "Left", tags : ["left"], showCount : true, questionTag: "left"},
        {id : "back", value : "Back", tags : ["back"], showCount : true, questionTag: "back"},
        {id : "right", value : "Right", tags : ["right"], showCount : true, questionTag: "right"},
        {id : "left_right", value : "Left Right", tags : ["left_right", "left", "right"], showCount : true, questionTag: "left_right"},
        {id : "others", value : "Others", tags : ["bottom", "others"], showCount : true, questionTag: "others"}
    ];

    $scope.recommendation = "";

    $scope.recommendations = [
      {id : "hold", value : "Hold"},
      {id : "recommended", value : "Recommended"},
      {id : "not-recommended", value : "Not Recommended"},
      {id : "underwriter", value : "Reffered to underwriter"}
    ];

    $scope.jkc = [];// ud

    $scope.changeActive = function(id){
        $scope.activeGroup = id;
        console.log('SETTING GROUP-->', $scope.activeGroup);
    };

    $scope.checkInActiveGroup = function(index){
        var item = $scope.casePhotos[index];
        // console.log('active-group',$scope.activeGroup,' from--- check in
		// group',item);
        var showFlag = false;
        item.groups.forEach(function(group, index){
          // console.info('###$scope.activeGroup == group
			// ',$scope.activeGroup, '==' ,group,' ', $scope.activeGroup ==
			// group);
          if($scope.activeGroup == group){
            // console.log('**********Tag Success');
            showFlag =  true;
          }
          // return true;
        });

        return showFlag;
    };

    $scope.getActiveGroupById = function(id){
        for(var i=0; i<$scope.groups.length; i++){
            if($scope.groups[i].id === id)
                return $scope.groups[i];
        }

        return null;
    };

    $scope.ifTagInActiveGroup = function(tag, groupId){
        var group  = $scope.getActiveGroupById(groupId);
        if(!group)
            return false;
        if(group.tags.indexOf(tag) > -1)
            return true;
        return false;
    };

    $scope.getTotalAnswerCount = function(){
        var count = 0;
        if($scope.caseQuestions){
            for(var j=0; j<$scope.caseQuestions.length; j++){
                var question = $scope.caseQuestions[j];
                if(question.answer)
                   count ++;
            }
        }

        return count;
    };

    $scope.getAnswersCount =  function(groupId){
        var group = $scope.getActiveGroupById(groupId);
        var count = 0;
        var totalCount = 0;
        if(group.showCount && $scope.caseQuestions){
            for(var j=0; j<$scope.caseQuestions.length; j++){
                var question = $scope.caseQuestions[j];
                if(group.tags.indexOf(question.photoTag) > -1){
                    totalCount ++;
                    if(question.answer)
                        count ++;
                }
            }

            return "(" + count + "/" + totalCount + ")";
        }else{
            return "";
        }


    };

	 $scope.checkQcReport = function(data){
		$scope.getUniqueCaseQuestion();
		$scope.getRecommendedCount();
		if(angular.equals($scope.recommendation, 'recommended')){
			$scope.uniqueQuestion = $scope.uniqueCaseQuestion;
			$scope.question = $scope.caseQuestions;
			$("#qc-recommend-container").modal('show');
	       $("#qc-recommend-container .modal-body")
               .html($compile("<qc-recommended callback='onConfirmQcRecomendCallBack()' case-id='caseId' unique-question='uniqueQuestion' case-questions='caseQuestions'></qc-recommended>")($scope));	
		}else if(angular.equals($scope.recommendation, 'hold'))   {
			$("#qc-hold-container").modal('show');
	       $("#qc-hold-container .modal-body")
               .html($compile("<qc-hold callback='onConfirmQcHoldCallBack()' case-id='caseId' photo-types='photoTypes' ></qc-hold>")($scope));
		}else{
			$scope.submitReport();
		}     
      
   	 };

	$scope.getRecommendedCount = function(){
		for(var i=0; i<$scope.uniqueCaseQuestion.length; i++){
			var count = 0;
			for(var j=0; j<$scope.caseQuestions.length; j++){
				if (angular.equals($scope.uniqueCaseQuestion[i].answer, $scope.caseQuestions[j].answer)) {
							count++;
				}
			}
			$scope.uniqueCaseQuestion[i].count = ""+count;
			/*$scope.recomend.push(uniqueQuestion);*/
			/*$scope.recomend.count = count;*/
		}
    };

	
	
	$scope.onConfirmQcHoldCallBack = function(){
	        $("#qc-hold-container").modal('hide');
	       	$scope.submitReport();
	 };

	$scope.onConfirmQcRecomendCallBack = function(){
	        $("#qc-recomend-container").modal('hide');
	       	$scope.submitReport();
	 };

	$scope.getUniqueCaseQuestion = function(){
        for(var i=0; i<$scope.caseQuestions.length; i++){
	          if( $scope.unique[$scope.caseQuestions[i].group]) continue;
             $scope.unique[$scope.caseQuestions[i].group] = true;
             $scope.uniqueCaseQuestion.push($scope.caseQuestions[i]);
        }

        return $scope.uniqueCaseQuestion;
    };


    $scope.submitReport = function(){
        if($scope.remarks && $scope.recommendation.length>0){
          var items = [];
          for(var i=0; i<$scope.caseQuestions.length; i++){
            items.push({
              id : $scope.caseQuestions[i].id,
              answer : $scope.caseQuestions[i].answer
            });
          }

          var data = {
            answers : JSON.stringify(items),
            recommendation : $scope.recommendation,
            case_id : $scope.caseDetails.id,
            remarks : $scope.remarks
          };

          ajaxService.sendInspectionReport(data, "Submitting inspection report. Please wait!").then(function(){
            alert("Inspection report submitted");
            $location.url('/qc');
          }, function(error){
            console.log(error);
            if(_.isObject(error.data))
                alert(error.data.error_message);
          });
        }
        else{
          if($scope.recommendation.length == 0){
            alert('Please select recommendation')
          }
          else{
            alert('Please check remarks.[maximum 50 characters]')
          }
        }


    };

    function getPhotoTagsByPhotoType(photoType){

        for(var i=0; i< $scope.photoTypes.length; i++){
            if($scope.photoTypes[i].id === photoType){
                var result = [];
                for(var j =0;j<$scope.photoTypes[i].photoTags.length; j++){
                    result.push($scope.photoTypes[i].photoTags[j].id);
                }

                return result;
            }
        }

    }

    function getGroupsByPhotoTags(photoTags){
        var result = [];

        for(var i=0; i< photoTags.length; i++){
            var item = photoTags[i];
            for(var j=0; j < $scope.groups.length; j++){
                var group = $scope.groups[j];

                if(group.tags.indexOf(item) > -1)
                    result.push(group.id);

            }

        }

        return result;
    }

    function disableNonQuestionGroups(){
        var questionTags = [];

        for(var i=0 ; i < $scope.caseQuestions.length; i++){
            if(questionTags.indexOf($scope.caseQuestions[i].photoTag) === -1){
                questionTags.push($scope.caseQuestions[i].photoTag)
            }
        }

        for(var i=0; i < $scope.groups.length; i++){
            var item = $scope.groups[i];

            if(item.questionTag && questionTags.indexOf(item.questionTag) === -1)
                item.hide = true;
        }
    }

    function getDefaultGroupOption(group){
        for(var i=0; i<$scope.caseOptions.length; i++){
            var item = $scope.caseOptions[i];
            if(item.group === group && item.isDefault)
                return item;
        }

        return null;
    }

    $scope.getCaseData = function(){
      angular.element(document).ready(function () {
        $q.all([
            ajaxService.getCasePhotos({ case_id : $routeParams.id}, "Fetching data. Please wait!"),
            ajaxService.getCaseQuestions({ case_id : $routeParams.id}, "Fetching data. Please wait!"),
            ajaxService.getCaseDetails({ case_id : $routeParams.id}, "Fetching data. Please wait!"),
            ajaxService.getQuestionOptions(null, "Fetching data. Please wait!"),
            ajaxService.getPhotoTypes(null, "Fetching data. Please wait!"),
<<<<<<< HEAD
            ajaxService.logQcOpened({ case_id : $routeParams.id}, "Fetching data. Please wait!"),
=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb


            ajaxService.getVehicleTypes(null, 'Please Wait!...'),
            ajaxService.getVehicleFuelTypes(null, 'Please Wait!...'),
            ajaxService.getVehicleMakeModels(null, 'Please Wait!...'),
            ajaxService.getFilesForCase({ case : $routeParams.id, regex: ".mp4"}, 'Please Wait!...'),
            ajaxService.logQcOpened({ case_id : $routeParams.id, logQcOpen : $scope.logQcOpen}, "Fetching data. Please wait!")
        ]).then(function(res){
            console.log('res==================>*', res);
           // $scope.casePhotos = res[0].data;
            res[0].data.sort(compare);
            $scope.casePhotos = res[0].data;
            $scope.caseQuestions = res[1].data;
            $scope.caseDetails = res[2].data;
            $scope.caseOptions = res[3].data;
            $scope.photoTypes = res[4].data;

            $scope.newCustName = $scope.caseDetails.customerName;
            disableNonQuestionGroups();

			$scope.logQcOpen = false;
            // $scope.vehicleTypes = res[5].data;
            // $scope.vehicleFuelTypes = res[6].data;
            // $scope.vehicleMakeModels = res[7].data;
            $scope.vehicleTypes = [];
            $scope.vehicleFuelTypes = res[6].data;
            $scope.vehicleMakeModels = res[7].data;
            $scope.vehicleMakeModelsUniq = [];
            
            $scope.inspectionVideos = res[8].data;

            $scope.remarks = $scope.caseDetails.comment;
            if($scope.caseDetails.remark)
                $scope.recommendation = $scope.caseDetails.remark;

            _.map(_.uniqBy(res[5].data, 'id'), function (item) {
              $scope.vehicleTypes.push(item);
            });

            _.map(_.uniqBy(res[7].data, 'make'), function (item) {
              $scope.vehicleMakeModelsUniq.push(item);
            });

            $scope.caseQuestions.forEach(function(caseQuestion, index){
                var item = getDefaultGroupOption(caseQuestion.group);
                if(item !== null)
                    caseQuestion.answer = ""+ item.id;
					caseQuestion.name = ""+ item.value;
            });

            $scope.caseDetails.inspectionTime = new Date($scope.caseDetails.inspectionTime);
            // $scope.casePhotos.sort(compare);
            var item = (_.sortBy($scope.casePhotos, function(o) { return o.id; }));

            $scope.casePhotos = item.reverse();
            console.log('reversed case photos==> ', $scope.casePhotos);

            var tmpLast = parseInt(($scope.casePhotos[0].fileName).split('.')[0]);
            $scope.casePhotos.lastCasePhotoUploadTime = tmpLast;
            $scope.casePhotos.forEach(function(item, index){
                var tags = getPhotoTagsByPhotoType(item.photoType);
                item.tags = tags;
                item.groups = getGroupsByPhotoTags(item.tags);
                item.zoom = "util/case-image/"+$scope.caseId+"/"+item.fileName;
                if(item.photoType == "chachis-number"){
                  $scope.vehicleChachis = item.qcAnswer;
                  $scope.vehicleChachisPhoto = "util/case-image/"+$scope.caseId+"/"+item.fileName;
                }

                if(item.photoType == "vin-plate-number"){
                  $scope.vehicleVin = item.qcAnswer;
                  $scope.vehicleVinPhoto = "util/case-image/"+$scope.caseId+"/"+item.fileName;
                }

                if(item.photoType == "odometer-rpm"){
                  $scope.odometerReading = item.qcAnswer;
                }



                // if(parseInt((item.fileName).split('.')[0]) > tmpLast){
                // $scope.casePhotos.lastCasePhotoUploadTime =
				// parseInt((item.fileName).split('.')[0]);
                // }
            });

            $scope.casePhotos.lastCasePhotoUploadTime = (_.maxBy($scope.casePhotos, 'snapTime')).snapTime;

            console.log('$scope.casePhotos--->',$scope.casePhotos);
            // $scope.buildCarousel($scope.casePhotos);

            var t1 = moment($scope.caseDetails.creationTime);
            var t2 = moment($scope.caseDetails.inspectionTime);
            $scope.caseDetails.ciTimeDiff = t2.diff(t1, 'minutes');

            var t2 = moment($scope.casePhotos.lastCasePhotoUploadTime);
            console.log('t2===============<', t2);
            $scope.caseDetails.cpTimeDiff = t2.diff(t1, 'minutes');

            $scope.setNewVehicleData();

        });

        $scope.caseVideo = undefined;
        $scope.caseVideoRequest = true;

        var vidReq = new XMLHttpRequest();
        vidReq.open('GET', 'case/inspection-video/'+$scope.caseId, true);
        vidReq.responseType = 'blob';

        vidReq.onload = function() {
           // Onload is triggered even on 404
           // so we need to check the status code
           if (this.status === 200) {
              var videoBlob = this.response;
              var vid = URL.createObjectURL(videoBlob); // IE10+
              // Video is now downloaded
              console.log('Video is now downloaded', vid);
              // and we can set it as source on the video element
              $scope.$apply(function() {
                  $scope.caseVideo = vid;
                  $scope.caseVideoRequest = false;
              });
              // $scope.caseVideo = vid;
              // $scope.caseVideoRequest = false;

           }
        }
        vidReq.onerror = function() {
          console.log('Video download Error');
        }

        vidReq.send();
    });
    }

    function compare(a,b) {
        if (a.id < b.id)
          return -1;
        if (a.id > b.id)
          return 1;
        return 0;
    }

    $scope.getGroupNameById = function(id){
         for(k=0;k<$scope.groups.length;k++){
             if($scope.groups[k].id === id){
                 return $scope.groups[k].value
             }
         }
         return '';
     }

    $scope.qwe = Lightbox.setImage;
    Lightbox.setImage = function(a){
        console.log($scope.jkc[a])
        $scope.activeGroup = $scope.jkc[a].groups[0];
        $scope.qwe(a);
        console.log('asdas')
        $timeout(function(){
        $('#q-data').html("Working");
            $('#q-data').html($compile('<table class=" table"> <tbody class="panel panel-primary">\n\
                <tr class="panel-heading clearfix"><th colspan=2><h2><b>Group: {{getGroupNameById(activeGroup)}}</b></h2></th></tr>\n\
                <tr ng-repeat="question in caseQuestions" ng-show="ifTagInActiveGroup(question.photoTag, activeGroup)"> \n\
                    <td> \n\
                        <h5> \n\
                            <b>{{question.value}}</b>\n\
                        </h5> \n\
                    </td> \n\
                    <td> \n\
                        <select ng-model="question.answer"  class="form-control">\n\
                            <option value="" disabled selected>Please select an option</option>\n\
                            <option ng-show="option.group === question.group" ng-repeat="option in caseOptions" value={{option.id}} ng-selected="question.answer === option.id">{{option.value}}</option>\n\
                        </select>\n\
                    </td>\n\
                </tr></tbody>\n\
            </table>')($scope));
            $timeout(function(){
                $('#q-data').height($('.modal-content').height()-8);
                console.log($('.modal-content').height()-8)
            },10);

        }, 10);

    }

    $scope.qwe2 = Lightbox.openModal;
    Lightbox.openModal = function(a,b,c){
        $scope.qwe2(a,b,c);
        $timeout(function(){
        $('#q-data').html("Working");
            $('#q-data').html($compile('<table class=" table"> <tbody class="panel panel-primary">\n\
                <tr class="panel-heading clearfix"><th colspan=2><h2><b>Group: {{getGroupNameById(activeGroup)}}</b></h2></th></tr>\n\
                <tr ng-repeat="question in caseQuestions" ng-show="ifTagInActiveGroup(question.photoTag, activeGroup)"> \n\
                    <td> \n\
                        <h5> \n\
                            <b>{{question.value}}</b>\n\
                        </h5> \n\
                    </td> \n\
                    <td> \n\
                        <select ng-model="question.answer"  class="form-control">\n\
                            <option value="" disabled selected>Please select an option</option>\n\
                            <option ng-show="option.group === question.group" ng-repeat="option in caseOptions" value={{option.id}} ng-selected="question.answer === option.id">{{option.value}}</option>\n\
                        </select>\n\
                    </td>\n\
                </tr></tbody>\n\
            </table>')($scope));
            $timeout(function(){
                $('#q-data').height($('.modal-content').height()-8);
                console.log($('.modal-content').height()-8)
            },100);

        }, 100);
    }

    $scope.editVehicleData = false;
    $scope.vehicleNumberEditEnable = false;
    $scope.vehicleNumberVerifyDisabled = true;
    $scope.verifyVahanMessage = "Please verify vehicle number!"

    $scope.enableVehicleNumberEdit = function(){
      $scope.vehicleNumberEditEnable = true;
      $scope.vahanVerified = true;
    }

    // $scope.vahanChasisNumber = true;
    // $scope.vahanVinPlateNumber = true;
    // $scope.vahanVerified = false;

    $scope.editVehicleDataEnable = function(){
      $scope.editVehicleData = !$scope.editVehicleData;
      $scope.setNewVehicleData();
      $scope.vahanYOM = false;
      $scope.vahanChasisNumber = false;
      $scope.vahanVinPlateNumber = false;
      $scope.vahanVerified = false;
    };

    $scope.submitNewVehicleData = function(){

      var data = {
        case_id: $scope.caseId,
        vehicle_number: $scope.newVehicleData.vehicle_number,
        vehicle_type: $scope.newVehicleData.type,
        vehicle_color: $scope.newVehicleData.color,
        make_model_id: $scope.newVehicleData.makeModel,
        fuel_type: $scope.newVehicleData.vehicle_fuel_type,
        yom: $scope.newVehicleData.yom,
        chachis_number : $scope.newVehicleData.chachis_number,
        vin_plate_number : $scope.newVehicleData.vin_plate_number,
        odometer_reading : $scope.newVehicleData.odometer_reading
      };
if(data.vin_plate_number==="" ||data.odometer_reading===""||data.odometer_reading=="0"){
    alert("Engine Number and Odometer reading cannot be empty or 0\n"); 

}else{
      console.log('data=================================*******',data);

      ajaxService.updateCaseVehicleDetails(data, 'Update Details. PleaseWait!...')
      .then(function(){
        alert('Vehicle Data Updated!');
        // $scope.caseDetails.vehicle.vehicleType.name = data.vehicle.type;
        // $scope.caseDetails.vehicle.make = data.vehicle.make;
        // $scope.caseDetails.vehicle.model = data.vehicle.model;
        // $scope.caseDetails.vehicleColor = data.vehicle.color;
        // $scope.caseDetails.vehicleFuelType.name =
		// data.vehicle.vehicle_fuel_type;
        // $scope.caseDetails.vehicleNumber = data.vehicle.vehicle_number;
        // $scope.caseDetails.vehicleYOM = data.vehicle.yom;
        // $scope.vehicleChachis = data.vehicle.chachis_number;
        // $scope.vehicleVin = data.vehicle.vin_plate_number;
        $scope.getCaseData();
        $scope.editVehicleDataEnable();

        // $scope.caseDetails.vehicle.vehicleType.name = data.vehicle_type;
        // $scope.caseDetails.vehicle.make = data.vehicle.make;
        // $scope.caseDetails.vehicle.model = data.vehicle.model;
        // $scope.caseDetails.vehicleColor = data.vehicle_color;
        // $scope.caseDetails.vehicleFuelType.name =
		// data.vehicle.vehicle_fuel_type;
        // $scope.caseDetails.vehicleNumber = data.vehicle_number;
        // $scope.caseDetails.vehicleYOM = data.vehicle.yom;
        // $scope.vehicleChachis = data.vehicle.chachis_number;
        // $scope.vehicleVin = data.vehicle.vin_plate_number;
      })
      .catch(function(e){
        alert('Error updating Vehicle Data.');
        console.error('Error updating Vehicle Data.',e);
      });
}
    };

    $scope.setNewVehicleData = function(){
      $scope.newVehicleData = {
        type:null,
        make:null,
        // model:null,
        makeModel:null,
        color:$scope.caseDetails.vehicleColor,
        vehicle_fuel_type:null,
        vehicle_number:$scope.caseDetails.vehicleNumber,
        yom:$scope.caseDetails.vehicleYOM,
        chachis_number:$scope.caseDetails.chassisNumber,
        vin_plate_number:$scope.caseDetails.engineNumber,
        odometer_reading : + $scope.caseDetails.odometerReading
      };

      for(i=0;i<$scope.vehicleTypes.length;i++){
        if($scope.vehicleTypes[i].name==$scope.caseDetails.vehicle.vehicleType.name){
          $scope.newVehicleData.type = $scope.vehicleTypes[i].id;
        }
      }
      for(i=0;i<$scope.vehicleFuelTypes.length;i++){
        if($scope.vehicleFuelTypes[i].name==$scope.caseDetails.vehicleFuelType.name){
          $scope.vehicle_fuel_type = $scope.vehicleFuelTypes[i].id;
        }
      }
      for(i=0;i<$scope.vehicleMakeModelsUniq.length;i++){
        if($scope.vehicleMakeModelsUniq[i].make==$scope.caseDetails.vehicle.make){
          $scope.newVehicleData.make = $scope.vehicleMakeModelsUniq[i].make;
        }
      }
      for(i=0;i<$scope.vehicleFuelTypes.length;i++){
        if($scope.vehicleFuelTypes[i].name==$scope.caseDetails.vehicleFuelType.name){
          $scope.newVehicleData.vehicle_fuel_type = $scope.vehicleFuelTypes[i].id;
        }
      }

      // $scope.vehicleMakeSelectedModels = [];
      //
      // for(i=0;i<$scope.vehicleMakeModels.length;i++){
      // if($scope.vehicleMakeModels[i].make==$scope.newVehicleData.make){
      // $scope.vehicleMakeSelectedModels.push($scope.vehicleMakeModels[i]);
      // }
      // }
      //
      // for(i=0;i<$scope.vehicleMakeSelectedModels.length;i++){
      // if($scope.vehicleMakeSelectedModels[i].model==$scope.caseDetails.vehicle.model){
      // $scope.newVehicleData.model =
		// $scope.vehicleMakeSelectedModels[i].id+"";
      // }
      // }
      $scope.setSelectedMakeModels();
      // $scope.getVahan/Data();
    }

    $scope.unsetMakeModels = function(){
      $scope.newVehicleData.make = "";
      $scope.newVehicleData.makeModel = "";
    }

    $scope.setSelectedMakeModels = function(){
      $scope.vehicleMakeSelectedModels = [];

      for(i=0;i<$scope.vehicleMakeModels.length;i++){
        if($scope.vehicleMakeModels[i].make==$scope.newVehicleData.make){
          $scope.vehicleMakeSelectedModels.push($scope.vehicleMakeModels[i]);
        }
      }

      for(i=0;i<$scope.vehicleMakeSelectedModels.length;i++){
        if($scope.vehicleMakeSelectedModels[i].model==$scope.caseDetails.vehicle.model){
          $scope.newVehicleData.makeModel = $scope.vehicleMakeSelectedModels[i].id+"";
        }
      }
    }

    $scope.checkType = function(item, index){
      // console.log('called check v t==>');
      // console.log('item ', item);
      // console.log('$scope.newVehicleData.type ',
		// $scope.newVehicleData.type);
      if(item.vehicleType.id == $scope.newVehicleData.type){
        return true;
      }
      else{
        return false;
      }
    }

    $scope.vahanData = {};

    $scope.getVahanData = function(){
      ajaxService.getVahanDetails({vehicle_number:$scope.newVehicleData.vehicle_number},'Verifying Vehicle Details!...')
      .then(function(res){
        console.log('Vahan Data', res);
        // $scope.newVehicleData.chachis_number = res.chasisNumber;
        // $scope.newVehicleData.vehicle_fuel_type = res.vehicleFuelType;
        // $scope.newVehicleData.yom = res.year;

        // $scope.vahanData.chachis_number = res.chasisNumber;
        // $scope.vahanData.vehicle_fuel_type = res.vehicleFuelType;
        // $scope.vahanData.yom = res.year;

        $scope.vahanData = res.data;
        console.log("**************",$scope.vahanData);
        $scope.verifyVahanMessage = ""
        $scope.vahanVerified = false;
      })
      .catch(function(e){
        console.log('Verifying vehicle details failed!', e);
        alert('Verifying vehicle details failed!');
      })
    }

    $scope.enableVerify = function(){
      if($scope.caseDetails.vehicleNumber.trim() != $scope.newVehicleData.vehicle_number.trim())
        $scope.vehicleNumberVerifyDisabled = false;
      else
        $scope.vehicleNumberVerifyDisabled = true;

      console.log('$scope.vehicleNumberVerifyDisabled===> ', $scope.vehicleNumberVerifyDisabled);
    }

    $scope.vahanVerify = function(){
      $scope.getVahanData();
    }

    $scope.zoomIn = function(id){
      // console.log('ZoomIn fired with==>',id);
      $("#"+id).elevateZoom({
        zoomWindowPosition: 10,
        zoomWindowOffetx: -15,
        scrollZoom : true,
        zoomWindowWidth : 500,
        zoomWindowHeight : 500
      });
    }

    $scope.zoomOut = function(){
      // console.log('ZoomOut fired');
    }

    $scope.getCaseData();
    
    $scope.isUploadMultiple = function(){
        
        if(!$scope.photoTypes)
            return false;
        
        for(var i=0; i< $scope.photoTypes.length; i++)
            if($scope.photoTypes[i].id === $scope.uploadFileType && $scope.photoTypes[i].multiple)
                return true;
        return false;
    };
    
    $scope.replacePhoto = function(){
        var file = document.getElementById('uploadFileIdentifier').files[0];
        
        if(!file){
            alert("Please choose a file!");
            return false;
        }
        
        var formData = new FormData();
        formData.append('case_file', file);
        formData.append('case_id', $scope.caseId);
        formData.append('comment', $scope.uploadFileComment);
        formData.append('photo_type', $scope.uploadFileType);

        ajaxService.replacePhoto(formData, "Uploading file. Please wait!").then(
                function(){
                    alert("File uploaded successfully!");
                }, function(err){

                    if(err.data.error_message)
                        alert(err.data.error_message);
                    else
                        alert("Unable to upload file!");
                    console.log(err);
                });
    };
    
    
    $scope.addPhoto = function(){
        var file = document.getElementById('uploadFileIdentifier').files[0];
        
        if(!file){
            alert("Please choose a file!");
            return false;
        }
        
        var formData = new FormData();
        formData.append('case_file', file);
        formData.append('case_id', $scope.caseId);
        formData.append('comment', $scope.uploadFileComment);
        formData.append('photo_type', $scope.uploadFileType);

        ajaxService.addPhoto(formData, "Uploading file. Please wait!").then(
                function(){
                    alert("File uploaded successfully!");
                }, function(err){

                    if(err.data.error_message)
                        alert(err.data.error_message);
                    else
                        alert("Unable to upload file!");
                    console.log(err);
                });
    };
    
    $scope.retagPhotos = function(data){        
      
      
       $("#qc-check-retag").modal('show');
       $("#qc-check-retag .modal-body")
               .html($compile("<retag-photos callback='retagPhotosCallback()' case-photos='casePhotos' case-id='caseId' photo-types='photoTypes'></retag-photos>")($scope));
    };
    
    $scope.retagPhotosCallback = function(){
        // $scope.$apply();
    };
    
    
    $scope.bulkUploadFiles = function(){
        var file = document.getElementById('bulkUploadFileIdentifier').files;
        
        
        
        
        if(!file){
            alert("Please choose a file!");
            return false;
        }
        
        var formData = new FormData();
        
        $.each($("input[type='file']")[0].files, function(i, file) {
            formData.append('photos', file);
        });
        
        formData.append('case_id', $scope.caseId);

        ajaxService.bulkUploadPhoto(formData, "Uploading file. Please wait!").then(
                function(){
                    alert("File uploaded successfully!");
                }, function(err){

                    if(err.data.error_message)
                        alert(err.data.error_message);
                    else
                        alert("Unable to upload file!");
                    console.log(err);
                });
    };
    
    $scope.deletePhoto = function(currentPhoto){
        ajaxService.deletePhoto({
            photo_id : currentPhoto.id
        }, "Deleting photo. Please wait!").then(function(){
            alert("Photo deleted successfully!");
            var index = $scope.casePhotos.indexOf(currentPhoto);
            $scope.casePhotos.splice(index, 1);  
        }, function(){
            alert("Unable to delete photo!");
        });
    }
    
    $scope.updateCustomerName = function(){
        ajaxService.updateCustomerName({
            case_id: $scope.caseId,
            customer_name : $scope.newCustName
        }, "Updating customer name. Please wait!").then(function(){
            alert("Updated successfully!");
            $scope.caseDetails.customerName = $scope.newCustName;  
        }, function(){
            alert("Unable to update name!");
        });
    }

}]);
