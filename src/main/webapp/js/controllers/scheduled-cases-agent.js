jaduApp.controller('ScheduledCasesAgentController',['$scope', '$timeout', '$location', '$compile', 'ajaxService', '$q','$mdDialog',  function($scope, $timeout, $location, $compile, ajaxService, $q, $mdDialog) {

  $scope.refreshDeamonActive = false;

  $scope.fetchScheduledCases = function(spinnerText){
    $q.all([
      ajaxService.getScheduledCasesAgent(null, spinnerText)
    ])
    .then(function(res){
      $scope.qcCases = res[0].data;
      console.log($scope.qcCases);
    });
  };

  angular.element(document).ready(function () {
    $scope.fetchScheduledCases("Fetching scheduled cases. Please wait!");
  });

}]);
