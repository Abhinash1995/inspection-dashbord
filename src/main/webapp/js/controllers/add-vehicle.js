jaduApp.controller('AddVehicleController',['$scope', '$timeout', '$location', '$compile', 'ajaxService', '$q', 'gmap',  function($scope, $timeout, $location, $compile, ajaxService, $q, gmap) {
  console.log('inside add veh controller');

  $scope.other_make = false;
  $scope.other_model = false;
  $scope.add_vehicle = false;
  $scope.vehicleTypes = [];
  $scope.vehicleSubTypes = [];
  $scope.vehicleFuelTypes = [];
  $scope.vehicleMakeModels = [];
  $scope.vehicleMakeModelsUniq = [];
  $scope.vehicle = {};

  $scope.getVehicleTypesData = function(){
    $scope.showProgress = true;

    $q.all([
      ajaxService.getVehicleTypes(null),
      ajaxService.getVehicleFuelTypes(null),
      ajaxService.getVehicleMakeModels(null)
    ])
    .then(function(res){
      console.log('res=== ', res);
      $scope.vehicleTypes = [];
      $scope.vehicleFuelTypes = res[1].data;
      $scope.vehicleMakeModels = res[2].data;
      $scope.vehicleMakeModelsUniq = [];

      _.map(_.uniqBy(res[0].data, 'id'), function (item) {
        $scope.vehicleTypes.push(item);
      });

      _.map(_.uniqBy($scope.vehicleMakeModels, 'make'), function (item) {
        $scope.vehicleMakeModelsUniq.push(item);
      });

      $scope.showProgress = false;

    })
    .catch(function(err){
      console.log('API Err! [make,model,fuel,type]', err)
    });
  }

  $scope.setSubType = function(){
    var item = null;
    // $scope.vehicle.type
    for(i=0;i<$scope.vehicleTypes.length;i++){
      type = $scope.vehicleTypes[i]
      if(type.id == $scope.vehicle.type){
        $scope.vehicleSubTypes = type.vehicleSubTypes;
        console.log(type);
      }
    }

    // if(item.subType.id == 'sedan'){
    //   $scope.vehicleSubType = {
    //     id: 'luxury',
    //     name: 'luxury'
    //   };
    // }
    // else
    // if(item.subType.id == 'tipper'){
    //   $scope.vehicleSubType = {
    //     id: 'truck',
    //     name: 'truck'
    //   };
    // }
    // else{
    //   $scope.vehicleSubType = item.subType;
    // }

  }

  $scope.setMake = function(){
    // $scope.vehicle.subtype = "";
    // $scope.vehicle.makeModel = "";
  }

  $scope.setSelectedMakeModels = function(){
    console.log('setSelectedMakeModel called with ',$scope.vehicle.make);
    if($scope.vehicle.make == -1){
      $scope.other_make = true;
      $scope.other_model = true;
      $scope.vehicle.model = -1;
    }
    else{
      $scope.other_make = false;
      $scope.other_model = true;
      $scope.vehicle.model = '';
    }
  }

  $scope.setVehicleMake = function(res){
    if($scope.vehicle.model == -1){
      $scope.other_model = true;
    }
    else{
      $scope.other_model = false;
    }
  }

  $scope.checkVehicleType = function(item, index){
    if(item.vehicleType.id == $scope.vehicle.type){
      // console.log('vt: ', item.id);
      return true;
    }
    else{
      return false;
    }
  }


  $scope.showAddVehicle = function(){
    $scope.add_vehicle = !$scope.add_vehicle;
  }

  $scope.getVehicleTypesData();

  $scope.submitNewVehicle = function(){
    var data = {};
    // console.log('vehicle ', $scope.vehicle);
    if($scope.vehicle.make == -1){
      data.vehicle_make = $scope.vehicle.other_make;
    }
    else{
      data.vehicle_make = $scope.vehicle.make;
    }

    data.vehicle_model = $scope.vehicle.other_model;
    data.vehicle_subtype = $scope.vehicle.sub_type;
    data.vehicle_type = $scope.vehicle.type;
    ajaxService.submitNewVehicle(data,'Please wait! Adding Vehicle.')
    .then(function(){
      $scope.getVehicleTypesData();
      alert('New Vehicle Added!');
      data = {};
      $scope.vehicle = {};
    })
    .catch(function(err){
      alert('Unable to add new vehicle!')
      console.log('New Vehicle Err ', err);
    })
    console.log('New Vehicle Data ', data);
  }

  $scope.toggleVehicle = function(id, enable){
    ajaxService.toggleVehicle({vehicle_id:id, enabled:enable}, 'Please Wait!. Updating Vehicle.')
    .then(function(){
      $scope.getVehicleTypesData();
      alert('Vehicle Updated');
    })
    .catch(function(err){
      alert('Unable to update vehicle!')
      console.log('Toggle Vehicle Err ', err);
    })
  }

}]);
