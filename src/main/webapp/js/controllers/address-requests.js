jaduApp.controller('AddressRequestsController',['$scope', '$timeout', '$location', '$compile', 'ajaxService', '$q',  function($scope, $timeout, $location, $compile, ajaxService, $q) {

    $scope.scheduleCase = function(id){
        $scope.currentCase = getCaseById(id);
        $("#modal-address-requests").modal('toggle');
        $("#yolo").html($compile("<schedule-case inspection-case=\"currentCase\" callback=\"callback()\"></schedule-case>")($scope));
    };

    $scope.callback = function(){
        $("#modal-address-requests").modal('hide');
    };

    angular.element(document).ready(function () {
        $q.all([
            ajaxService.getAddressRequests(null, "Fetching data. Please wait!")
        ]).then(function(data){
            $scope.qcCases = data[0];
        });
    });

    function getCaseById(id){
        for(var i=0; i< $scope.qcCases.length; i++){
            if($scope.qcCases[i].id === id)
                return $scope.qcCases[i];
        }

        return null;
    }
}]);
