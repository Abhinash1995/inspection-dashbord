jaduApp.controller('HomeController',['$scope', '$timeout', '$location', '$compile' , '$auth', '$q','ajaxService', function($scope, $timeout, $location, $compile, $auth, $q, ajaxService) {

  $scope.defaultState = null;
  $scope.defaultCompany = null; // 'IFFCO Tokio General Insurance Co. Ltd.';
  var spinnerText = 'Building Dashboard....';

  $scope.setTimeFilter = function(value){
    $scope.timeFilter = value;
    switch(value){
      case 'TODAY':
        $scope.todayClass = 'btn btn-success active';
        $scope.weekClass = 'btn btn-default';
        $scope.monthClass = 'btn btn-default';
        $scope.yearClass = 'btn btn-default';
        break;
      case 'CURRENT_WEEK':
        $scope.todayClass = 'btn btn-default';
        $scope.weekClass = 'btn btn-success active';
        $scope.monthClass = 'btn btn-default';
        $scope.yearClass = 'btn btn-default';
        break;
      case 'CURRENT_MONTH':
        $scope.todayClass = 'btn btn-default';
        $scope.weekClass = 'btn btn-default';
        $scope.monthClass = 'btn btn-success active';
        $scope.yearClass = 'btn btn-default';
        break;
      case 'CURRENT_YEAR':
        $scope.todayClass = 'btn btn-default';
        $scope.weekClass = 'btn btn-default';
        $scope.monthClass = 'btn btn-default';
        $scope.yearClass = 'btn btn-success active';
        break;
    }
    $scope.updateStats();
  }

  $scope.locationData = null;

  $scope.branchFilterDataUniqe = { // to be mapped after data fetch
    states:[],
    divisions:[],
    branches:[]
  };

  $scope.branchFilterData = { // to be updated by user activity
    states:[],
    divisions:[],
    branches:[]
  };

  $scope.branchFilter = {
    company:$scope.defaultCompany,
    state:$scope.defaultState,
    division:null,
    branch:null
  };
  // $scope.branchFilter.company = $scope.defaultCompany;
  // console.log('$scope.branchFilter.company****************',
	// $scope.branchFilter.company);

  $scope.setStateOptions = function(){
    // $scope.defaultState = '';
    $scope.branchFilterData.divisions = [];
    console.log('called with ',$scope.branchFilter.state);
    _.filter($scope.branchFilterDataUniqe.divisions, function(item) {
      if(item.state == $scope.branchFilter.state){
        $scope.branchFilterData.divisions.push(item.division);
      }
    });

    ajaxService.getBranchDivisionByCompany($scope.branchFilter.company,spinnerText)
    .then(function(res){
      // console.log(res);
      $scope.locationData = res;

      _.map(_.uniqBy(res, 'state'), function (item) {
        $scope.branchFilterData.states.push(item.state.trim());
        $scope.branchFilterDataUniqe.states.push(item);
      });

      var temp = [];
      _.map(_.uniqBy($scope.branchFilterData.states), function (item) {
        temp.push(item);
      });
      $scope.branchFilterData.states = temp;

      _.map(_.uniqBy(res, 'division'), function (item) {
        $scope.branchFilterDataUniqe.divisions.push(item);
      });

      _.map(_.uniqBy(res, 'branch'), function (item) {
        $scope.branchFilterDataUniqe.branches.push(item);
      });

      $scope.setDivisionOptions();
    })
    .catch(function(e){
      console.log('Error Fetching Locations ',e);
    })

  }

  $scope.setDivisionOptions = function(){
    // $scope.defaultState = '';
    $scope.branchFilterData.divisions = [];
    console.log('called with ',$scope.branchFilter.state);
    _.filter($scope.branchFilterDataUniqe.divisions, function(item) {
      if(item.state == $scope.branchFilter.state){
        $scope.branchFilterData.divisions.push(item.division);
      }
    });
  }

  $scope.setBranchOptions = function(){
    $scope.branchFilterData.branches = [];
    console.log('called with ',$scope.branchFilter.division);
    _.filter($scope.branchFilterDataUniqe.branches, function(item) {
      // console.log(item);
      if(item.division == $scope.branchFilter.division){
        $scope.branchFilterData.branches.push(item.branch);
      }
    });
  }

  $scope.setStateOptions();

  ajaxService.getInsuranceCompanies({},spinnerText)
  .then(function(res){
    console.log('res-->',res);
    $scope.companyData = res.data;
  })
  .catch(function(e){
    console.error('Get company names error',e);
  });



  var statsData = {
    state: $scope.defaultState,
    division: null,
    branch: null,
    duration: 'CURRENT_MONTH'
  };

  $scope.dashboardStats = {};

  $scope.availableFields = [];

  $scope.updateStats = function(){

    statsData = {
      state: $scope.branchFilter.state,
      division: $scope.branchFilter.division,
      branch: $scope.branchFilter.branch,
      duration: $scope.timeFilter,
      company: $scope.branchFilter.company
    };

    if(statsData.state == undefined){
      statsData.state = $scope.defaultState;
    }
    console.log('Stats data --> ', statsData);
    ajaxService.getDashboardStats(statsData, spinnerText)
    .then(function(res){
      console.log('Stats res==>',res);
      $scope.dashboardStats = res.data;

      $scope.dashboardStats.cancelledCasesCountPerc = isNaN((($scope.dashboardStats.cancelledCasesCount/$scope.dashboardStats.completedCasesCount)*100).toFixed(2))?0:(($scope.dashboardStats.cancelledCasesCount/$scope.dashboardStats.completedCasesCount)*100).toFixed(2);

      $scope.dashboardStats.commercialsCountPerc = isNaN((($scope.dashboardStats.commercialsCount/$scope.dashboardStats.completedCasesCount)*100).toFixed(2))?0:(($scope.dashboardStats.commercialsCount/$scope.dashboardStats.completedCasesCount)*100).toFixed(2);

      $scope.dashboardStats.completedCasesCountPerc = isNaN((($scope.dashboardStats.completedCasesCount/$scope.dashboardStats.completedCasesCount)*100).toFixed(2))?0:(($scope.dashboardStats.completedCasesCount/$scope.dashboardStats.completedCasesCount)*100).toFixed(2);

      $scope.dashboardStats.fourWheelersCountPerc = isNaN((($scope.dashboardStats.fourWheelersCount/$scope.dashboardStats.completedCasesCount)*100).toFixed(2))?0:(($scope.dashboardStats.fourWheelersCount/$scope.dashboardStats.completedCasesCount)*100).toFixed(2);

      $scope.dashboardStats.notRecommendedCountPerc = isNaN((($scope.dashboardStats.notRecommendedCount/$scope.dashboardStats.completedCasesCount)*100).toFixed(2))?0:(($scope.dashboardStats.notRecommendedCount/$scope.dashboardStats.completedCasesCount)*100).toFixed(2);

      $scope.dashboardStats.pendingcountPerc = isNaN((($scope.dashboardStats.pendingcount/$scope.dashboardStats.completedCasesCount)*100).toFixed(2))?0:(($scope.dashboardStats.pendingcount/$scope.dashboardStats.completedCasesCount)*100).toFixed(2);

      $scope.dashboardStats.recommendedCountPerc = isNaN((($scope.dashboardStats.recommendedCount/$scope.dashboardStats.completedCasesCount)*100).toFixed(2))?0:(($scope.dashboardStats.recommendedCount/$scope.dashboardStats.completedCasesCount)*100).toFixed(2);

      $scope.dashboardStats.twoWheelersCountPerc = isNaN((($scope.dashboardStats.twoWheelersCount/$scope.dashboardStats.completedCasesCount)*100).toFixed(2))?0:(($scope.dashboardStats.twoWheelersCount/$scope.dashboardStats.completedCasesCount)*100).toFixed(2);

    })
    .catch(function(e){
      console.log('Error Fetching Stats ',e);
    })
  }

  $scope.setTimeFilter('CURRENT_MONTH');

  // ===============================================================
  ajaxService.getDashboardTableFields({}, spinnerText)
  .then(function(res){
    console.log('Fields res==>',res);
    var tableFields = res.data;
    tableFields.forEach(function(field, index){
      $scope.availableFields.push({
        id: field.id,
        name: field.value,
        enabled:false
      });
    });
  })
  .catch(function(e){
    console.log('Error Fetching Stats ',e);
  })

  var paginationOptions = {
    pageNumber: 1,
    pageSize: 25,
    sort: null
  };

  var getData = {
    company:'',
    state:'',
    division:'',
    branch:'',
    from:0,
    to:25,
    // fields:[],
    fields:"",
    duration:'',
    totalFetched:25
  }

  $scope.gridOptions = {
    paginationPageSizes: [25, 50, 100, 200, 500],
    paginationPageSize: 25,
    enableColumnMenus: true,
    enableAutoFitColumns: true,
    useExternalPagination: true,
    onRegisterApi: function(gridApi) {
      $scope.gridApi = gridApi;
      gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
        console.log('paginationChanged CALLED');
        console.log('getData.fields.length',getData.fields.length);
        if(getData.fields.length > 0){
          paginationOptions.pageNumber = newPage;
          paginationOptions.pageSize = pageSize;

          getData.duration = $scope.timeFilter;

          getData.from = (newPage-1)*pageSize;
          getData.to = getData.from+pageSize;

          getData.state = $scope.branchFilter.state;
          getData.division = $scope.branchFilter.division;
          getData.branch = $scope.branchFilter.branch;
          getData.company = $scope.branchFilter.company;

          if(getData.to>getData.totalFetched){
            getData.totalFetched = getData.to;
          }
          console.log('Inside onRegister ',getData);
          // console.log('Pagination Request');
          // console.log('->newPage',newPage);
          // console.log('==>pageSize',pageSize);
          // console.log('->getData.from',getData.from);
          // console.log('->getData.to',getData.to);
          getPage();
        }
      });
    }
  };

  var getPage = function() {
    getData.duration = $scope.timeFilter;
    // console.log('~~~~~~~~~~~~~~~~~~REQUEST DATA: ',getData);
    ajaxService.getDashboardTableData(getData,"Loading Data.......")
    .then(function(res){
      console.log('TABLE==================> ',res.data);

      $scope.gridOptions.data = res.data.data;
      $scope.gridOptions.data.forEach(function(row,i){
        if('creation_time' in row){
          row['creation_time'] = moment.unix(row['creation_time']/1000).format("DD/MM/YYYY hh:mm a");
        }

        if('inspection_time' in row){
          row['inspection_time'] = moment.unix(row['inspection_time']/1000).format("DD/MM/YYYY hh:mm a");
        }
        
        if('qc_time' in row){
            row['qc_time'] = moment.unix(row['qc_time']/1000).format("DD/MM/YYYY hh:mm a");
          }

        for (var property in row) {
          if (row.hasOwnProperty(property)) {
              if(row[property] == null){
                row[property] = 'n/a';
              }
          }
        }
      });

      $scope.gridOptions.totalItems = res.data.count;
    })
    .catch(function(e){
      console.log('Error loading table data',e);
    })
  }

  // $scope.$watch('temp',function(oldVal, newVal){
  // console.log('$watch----------------------> ');
  // console.log('oldVal ',oldVal);
  // console.log('newVal ',newVal);
  // console.log('$watch----------------------> ');
  // })

  $scope.toggleField = function($index){
    $scope.temp = getData.fields;

    var fieldName = $scope.availableFields[$index].name;
    var fieldId = $scope.availableFields[$index].id;
    var enabled = $scope.availableFields[$index].enabled;
    // console.log('toggleField called with field: ',field);
    // console.log('toggleField called with enabled: ',enabled);
    if(enabled){
      $scope.gridOptions.columnDefs.push({
        name: fieldName,
        // field: field.replace(/ /g,'_').toLowerCase(),
        field:fieldId,
        minWidth: 130
      });
      console.log('==============> ',getData.fields);
      // getData.fields.push(field.replace(/ /g,'_').toLowerCase());
      // getData.fields.push(fieldId);
      if(getData.fields.length == 0){
        getData.fields = ""+fieldId;
      }
      else{
        getData.fields = getData.fields+","+fieldId;
      }
      getData.company = $scope.branchFilter.company;
      console.log('Aftre push==============> ',getData.fields);

    }
    else{
      // console.log('Hitting else');
      // console.log('$scope.gridOptions',$scope.gridOptions);
      // var index = null;
      // var bc = function(){
        // console.log('Hitting splice');
        // console.log('$scope.gridOptions.columnDefs.length
		// :',$scope.gridOptions.columnDefs.length);
        for(k=0;k<$scope.gridOptions.columnDefs.length;k++){
          // console.log('$scope.gridOptions.columnDefs[k].name == field');
          // console.log('$scope.gridOptions.columnDefs[k].name:',$scope.gridOptions.columnDefs[k].name);
          // console.log('field:',field);
          // console.log($scope.gridOptions.columnDefs[k].name == field);
          if($scope.gridOptions.columnDefs[k].name == fieldName){
            // return k;
            $scope.gridOptions.columnDefs.splice(k,1);
          }
        }
      // };
      // index = bc();
      // $scope.gridOptions.columnDefs.splice(index,1);
      var temp = getData.fields.split(',');
      var removeIndex;
      for(x=0;x<temp.length;x++){
        if(temp[x] == fieldId){
          removeIndex = x;
        }
      }

      temp.slice(removeIndex,1);
      console.log('AFTER REMOVE from temp-->',temp);
      getData.fields = '';
      getData.fields = temp.join();
      console.log('AFTER REMOVE getdata.fields',getData.fields);

      // bc1 = function(){
        // for(k=0;k<getData.fields.length;k++){
        // if(getData.fields[k] == fieldId){
        // // return k;
        // getData.fields.splice(k,1);
        // }
        // }
      // };
      // index = bc1();
    }
    getPage();
  }

  $scope.generateExcel = function(){
    var dataObj = {
      from:0,
      to:100000,
      duration:getData.duration,
      fields:getData.fields
    }
    console.log('excel req object=========>  ', dataObj);
    ajaxService.getDashboardTableData(dataObj,"Generating Excel Data...")
    .then(function(res){
      console.log(' getDashboardTableData =========>  ', res);

      res.data.data.forEach(function(row,i){
        if('creation_time' in row){
          row['creation_time'] = moment.unix(row['creation_time']/1000).format("DD/MM/YYYY hh:mm a");
        }

        if('inspection_time' in row){
          row['inspection_time'] = moment.unix(row['inspection_time']/1000).format("DD/MM/YYYY hh:mm a");
        }
        if('qc_time' in row){
            row['qc_time'] = moment.unix(row['qc_time']/1000).format("DD/MM/YYYY hh:mm a");
          }

        for (var property in row) {
          if (row.hasOwnProperty(property)) {
              if(row[property] == null){
                row[property] = 'n/a';
              }
          }
        }
      });

      var csv = Papa.unparse({data: res.data.data},{
                                delimiter: ",",
                                newline: "\r\n"
                              });

      var link = document.createElement("a");
      blob = new Blob([csv], { type: 'text/csv' });
    	var csvUrl = window.webkitURL.createObjectURL(blob);
    	$(link).attr({
    		'download': 'MIS_Report.csv',
    		'href': csvUrl
    	});
      link.click();

      console.log('EXCEL DATA==> ',res.data);

    })
    .catch(function(e){
      alert('Error loading excel data');
      console.log('Error loading excel data',e);
    })
  }

}]);
