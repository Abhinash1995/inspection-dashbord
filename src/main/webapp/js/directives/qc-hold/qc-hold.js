angular.module('jaduApp').directive('qcHold', ['$http', '$location', '$timeout', '$compile', '$window', 'ajaxService', function($http, $location, $timeout, $compile, $window, ajaxService) {

	return {
		replace: true,
		restrict: 'EA',
		scope: {
			photoTypes: "=",
			caseId: "=",
			callback: "&"
		},
		templateUrl: "js/directives/qc-hold/qc-hold.html",
		controller: function($scope, $element) {
			
		},
		link: function(scope, element, attrs) {
			
			scope.selection = [];

			scope.togglePhotoType = function(photoType) {
				var idx = scope.selection.indexOf(photoType);
				// Is currently selected
				if (idx > -1) {
					scope.selection.splice(idx, 1);
				}
				else {
					scope.selection.push(photoType);
				}
			};

			/*scope.confirmHoldQc = function() {

				scope.callback({
				});
			};*/
			
			scope.confirmHoldQc = function(){
                ajaxService.reUploadPhoto({
				                    case_id : scope.caseId,
				                    parts : scope.selection.join(', ')
               				 		},"Submitting Qc. Please wait!")
               .then(function(){
                   scope.callback();
                }, function(err){
                    console.log(err);
                });
            };
		}
	};
}]);














/*

angular.module('jaduApp').directive('qcHold', ['$http', '$location', '$timeout', '$compile', '$window', 'ajaxService', function($http, $location, $timeout, $compile, $window, ajaxService) {

	return {
		replace: true,
		restrict: 'EA',
		scope: {
			photoTypes: "=",
			caseId: "=",
			callback: "&"
		},
		templateUrl: "js/directives/qc-hold/qc-hold.html",
		controller: function($scope, $element) {
			
		},
		link: function(scope, element, attrs) {
			
			scope.selection = [];

			scope.togglePhotoType = function(photoType) {
				var idx = scope.selection.indexOf(photoType);
				// Is currently selected
				if (idx > -1) {
					scope.selection.splice(idx, 1);
				}
				else {
					scope.selection.push(photoType);
				}
			};
			console.log('abcd', scope.selection)

			scope.confirmHoldQc = function() {

				scope.callback({
				});
			

			scope.confirmHoldQc = function(){
                ajaxService.reUploadPhoto({
				                    caseId : scope.caseId,
				                    comment : scope.selection
               				 		},"Submitting Qc. Please wait!")
               .then(function(){
                   scope.callback();
                }, function(err){
                    console.log(err);
                });
            };
		}
	};
}]);*/