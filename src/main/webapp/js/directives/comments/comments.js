angular
<<<<<<< HEAD
	.module('jaduApp')
	.directive(
		'caseComments',
		[
			'$http',
			'$location',
			'$timeout',
			'$compile',
			'$window',
			'ajaxService',
			function($http, $location, $timeout, $compile, $window,
				ajaxService) {
				return {
					replace: true,
					restrict: 'EA',
					scope: {
						inspectionCase: "="
					},
					templateUrl: "js/directives/comments/comments.html",
					controller: function($scope, $element) {
					},
					link: function(scope, element, attrs) {
						console.log(scope.inspectionCase);
						scope.getComments = function() {
							ajaxService.getDefaultComments(null, "Fetching comments. Please wait!").
								then(function(result) {
									var data = result.data;
									scope.defaultComments = data;
								});
								
							ajaxService
								.getComments(
									{
										case_id: scope.inspectionCase.id
									},
									"Fetching comments. Please wait!")
								.then(
									function(result) {
										var data = result.data;
										scope.comments = data;
									},
									function(err) {
										console.log(err);
										alert("Unable to fetch comments. Please try again later.");
									});
						};

						scope.addComments = function() {
							ajaxService
								.addComment(
									{
										case_id : scope.inspectionCase.id,
										comment: scope.comment,
										commentId : scope.commentId
									},
									"Adding comment. Please wait!")
								.then(
									function(data) {
										alert("Comment added");
										scope.comment = null;
										scope.getComments()
									},
									function(err) {
										console.log(err);
										alert("Unable to add comment. Please try again later.");
									});
						};

						scope.getOTP = function() {
							ajaxService
								.getUnexpiredOTP(
									{
										username: scope.inspectionCase.customerPhoneNumber
									},
									"Fetching data. Please wait!")
								.then(
									function(result) {
										var data = result.data;
										scope.otps = data;
									},
									function(err) {
										console.log(err);
										alert(err.data.error_message);
									});
						}

						scope.getComments();
					}
				};
			}]);
=======
		.module('jaduApp')
		.directive(
				'caseComments',
				[
						'$http',
						'$location',
						'$timeout',
						'$compile',
						'$window',
						'ajaxService',
						function($http, $location, $timeout, $compile, $window,
								ajaxService) {
							return {
								replace : true,
								restrict : 'EA',
								scope : {
									inspectionCase : "="
								},
								templateUrl : "js/directives/comments/comments.html",
								controller : function($scope, $element) {

								},
								link : function(scope, element, attrs) {
									console.log(scope.inspectionCase);
									scope.getComments = function() {
										ajaxService
												.getComments(
														{
															case_id : scope.inspectionCase.id
														},
														"Fetching comments. Please wait!")
												.then(
														function(result) {
															var data = result.data;
															scope.comments = data;
														},
														function(err) {
															console.log(err);
															alert("Unable to fetch comments. Please try again later.");
														});
									};

									scope.addComments = function() {
										ajaxService
												.addComment(
														{
															case_id : scope.inspectionCase.id,
															comment : scope.comment
														},
														"Adding comment. Please wait!")
												.then(
														function(data) {
															alert("Comment added");
															scope.comment = null;
															scope.getComments()
														},
														function(err) {
															console.log(err);
															alert("Unable to add comment. Please try again later.");
														});
									};

									scope.getOTP = function() {
										ajaxService
												.getUnexpiredOTP(
														{
															username : scope.inspectionCase.customerPhoneNumber
														},
														"Fetching data. Please wait!")
												.then(
														function(result) {
															var data = result.data;
															scope.otps = data;
														},
														function(err) {
															console.log(err);
															alert(err.data.error_message);
														});
									}

									scope.getComments();
								}
							};
						} ]);
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
