
/* global ModalWindow, angular, atqUTIL, ATQConfirmDialog */

angular.module('jaduApp').directive('reopenCase', ['$http', '$location', '$timeout','$compile', '$window','ajaxService', function ($http, $location, $timeout, $compile, $window, ajaxService) {
    return {
        replace: true,
        restrict: 'EA',
        scope: {
            inspectionCase : "=",
            callback: "&"
        },
        templateUrl: "js/directives/reopen-case-qc/reopen.html",
        controller: function ($scope, $element) {

        },
        link: function(scope, element, attrs){
            scope.reopen = function(){
                ajaxService.reopenCase({
                    case_id : scope.inspectionCase.id,
                    reason : scope.reopenReason
                }).then(function(){
                    alert("Inspection case with ref. no.: " + scope.inspectionCase.id + " has been reopened successfully!" )
                    scope.callback();
                }, function(err){
                    console.log(err);
                    alert("Unable to reopen case. Please try again later.");
                });
            };
        }
    };
}]);
