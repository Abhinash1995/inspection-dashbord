
/* global ModalWindow, angular, atqUTIL, ATQConfirmDialog */

angular.module('jaduApp').directive('scheduleCase', ['$http', '$location', '$timeout','$compile', '$window','ajaxService', 'GoogleMaps', function ($http, $location, $timeout, $compile, $window, ajaxService, GoogleMaps) {
    return {
        replace: true,
        restrict: 'EA',
        scope: {
            inspectionCase : "=",
            callback: "&"
        },
        templateUrl: "js/directives/schedule-case/schedule-case.html",
        controller: function ($scope, $element) {
            // $scope.agentFilter = null;
            $scope.agentList = [];
            //fetch list of agents from api
            ajaxService.getNearbyAgents({
              lat:34,
              lng:34,
              range:10
            },'Fetching nearby Agents')
            .then(function(res){
              console.log('Nearby agents',res);
              $scope.agentList = res;
              console.log('scope.agentList ', scope.agentList);
            })
            .catch(function(e){
              console.log('Error fetching nearby agents',e);
            })

            $scope.agentAssignedId = null;
            $scope.assignAgent = function(id){
              $scope.agentAssignedId = id;
              //update the input with agent contact
            }
            console.log('controller fired');
        },
        link: function(scope, element, attrs, controller){
            $timeout(function(){
                init()
            }, 10);
            //init();


            function init(){
              console.log('link fired',controller.agentList);

                if(scope.inspectionCase){
                    //var timestamp = moment.unix(scope.inspectionCase.inspectionTime/1000);
                    scope.inspectionCase.inspectionTimeModel = null;
                    scope.inspectionCase.inspectionTime = null;
                }
                scope.test = "works";
                scope.map = null;

                GoogleMaps.getCurrentLocation(function(){
                    console.log("In here 1")
                    scope.map = GoogleMaps.addMap(null, null, "map");
                    GoogleMaps.addInputField(document.getElementById('pac-input'));
                    console.log('GOT MAP INSTANCE-->', scope.map);

                }, function(){
                    console.log("In here 2")
                    scope.map = GoogleMaps.addMap(null, null, "map");
                    GoogleMaps.addInputField(document.getElementById('pac-input'), function(){
                    });

                });
            }



            scope.scheduleCase = function(){
                var latlng = GoogleMaps.getLatLong();
                //console.log(latlng.lat(), latlng.lng() )
                scope.callback();

                var data = {
                    latitude : typeof latlng.lat  === "function" ? latlng.lat() :latlng.lat,
                    longitude : typeof latlng.lng  === "function" ? latlng.lng() : latlng.lng,
                    inspection_time : scope.inspectionCase.inspectionTime,
                    ic: scope.inspectionCase.id
                };

                console.log(data)

                ajaxService.schdeuleCase(data, "Secheduling case. Please wait!").then(function(data){
                    $location.url('/scheduled-cases');
                }, function(){
                    alert("Unable to schedule case!");
                });
            };

        }
    };
}]);
