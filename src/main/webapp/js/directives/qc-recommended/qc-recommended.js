angular.module('jaduApp').directive('qcRecommended', ['$http', '$location', '$timeout', '$compile', '$window', 'ajaxService', function($http, $location, $timeout, $compile, $window, ajaxService) {
	return {
		replace: true,
		restrict: 'EA',
		scope: {
			uniqueQuestion: "=",
			caseQuestions: "=",
			caseId: "=",
			callback: "&"
		},
		templateUrl: "js/directives/qc-recommended/qc-recommended.html",
		controller: function($scope, $element) {

		},
		link: function(scope, element, attrs) {
			
			/*scope.getAnswersCount = function(answerId) {
				scope.count = 0;
				if (scope.caseQuestions) {
					for (var j = 0; j < scope.caseQuestions.length; j++) {
						var question = scope.caseQuestions[j];
						if (angular.equals(question.answer, answerId)) {
							count++;
						}
					}

					return scope.count;
				}
			};*/
			scope.confirmRecommendQc = function() {
				scope.callback();
			};
		}
	};

}]);