angular.module('jaduApp')
  .directive('noSpaces', function () {
    return {
        restrict: 'AE',
        scope: {
            noSpaces: '='
        },
        link: function (scope) {
          // this will match spaces, tabs, line feeds etc
          // you can change this regex as you want
          var regex = /\s/g;

          scope.$watch('noSpaces', function (newValue, oldValue) {
              if (newValue != oldValue && regex.test(newValue)) {
                scope.noSpaces = newValue.replace(regex, '');
              }
          });
        }
    };
  });
