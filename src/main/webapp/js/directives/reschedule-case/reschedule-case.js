
/* global ModalWindow, angular, atqUTIL, ATQConfirmDialog */

angular.module('jaduApp').directive('rescheduleCase', ['$http', '$location', '$timeout','$compile', '$window','ajaxService', function ($http, $location, $timeout, $compile, $window, ajaxService) {
    return {
        replace: true,
        restrict: 'EA',
        scope: {
            inspectionCase : "=",
            callback: "&"
        },
        templateUrl: "js/directives/reschedule-case/reschedule-case.html",
        controller: function ($scope, $element) {

        },
        link: function(scope, element, attrs){
            scope.rescheduleReasons = [
                {id : "Customer not available"},
                {id : "Vehicle not available"},
                {id : "Inspector not available"}
            ];

            scope.cancelReasons = [
                {id : "Customer not interested"},
                {id : "Inpector not avilable"},
                {id : "Inspection already done"},
                {id : "Agent asked to cancel"}
            ];

            if(scope.inspectionCase){
              console.log('========>here');
              var timestamp = moment.unix(scope.inspectionCase.inspectionTime/1000);
              scope.inspectionCase.inspectionTimeModel = timestamp;
              scope.inspectionCase.inspectionTime = ( timestamp.format("YYYY/MM/DD HH:mm:ss") );
            }

            scope.rescheduleCase = function(){
              scope.callback();
              // $("#modal-2").modal('close');
              var dataObj = {
                case_id:scope.inspectionCase.id,
                new_inspection_time:moment.unix(scope.inspectionCase.inspectionTimeModel/1000).format("YYYY/MM/DD HH:mm:ss"),
                reason:scope.inspectionCase.rescheduleReason
              };

              ajaxService.rescheduleCase(dataObj,'Please Wait...')
              .then(function(res){
                alert('Case Rescheduled!');
<<<<<<< HEAD
                $scope.callback('refresh');
=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
                // scope.fetchScheduledCases("Updating Case. Please wait!");
              })
              .catch(function(e){
                console.error('Case Rescheduling Error',e);
              })
            };
        }
    };
}]);
