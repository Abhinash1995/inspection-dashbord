var customerApp = angular.module('customerApp', ["ngRoute","ngMaterial", "ngResource", "ngCookies", "moment-picker","ngAria"
,"ngMessages"]);

customerApp.config(['momentPickerProvider', function (momentPickerProvider) {
    momentPickerProvider.options({
        /* Picker properties */
        locale:        'en',
        format:        'YYYY/MM/DD HH:mm',
        minView:       'decade',
        maxView:       'hour',
        startView:     'month',
        autoclose:     true,
        today:         false,
        keyboard:      false,

        /* Extra: Views properties */
        leftArrow:     '&larr;',
        rightArrow:    '&rarr;',
        yearsFormat:   'YYYY',
        monthsFormat:  'MMM',
        daysFormat:    'D',
        hoursFormat:   'HH:[00]',
        minutesFormat: moment.localeData().longDateFormat('LT').replace(/[aA]/, ''),
        secondsFormat: 'ss',
        minutesStep:   15,
        secondsStep:   60,
        hoursStart:8,
        hoursEnd:20,
    });
}]);

customerApp.service('gmap', function(){
  var self = this;
  var map = null;
  var marker = null;
  var cityCircle = null;
  var radius = null;
  var myLatlng = {
    lat:null,
    lng:null
  };
  var formatted_address = "";
  var geocoder = new google.maps.Geocoder;

  navigator.geolocation.getCurrentPosition(function(pos){
    myLatlng.lat = parseFloat(pos.coords.latitude);
    myLatlng.lng = parseFloat(pos.coords.longitude);
  },
  function(e){
    console.log('Unable to fetch Location', e)
  },
  {
    timeout:1000,
    enableHighAccuracy:true
  });

  this.initMap = function(element, lat, lng){
    if(lat != null)
      myLatlng.lat = lat;
    if(lng != null)
      myLatlng.lng = lng;

    map = new google.maps.Map(element, {
      center: myLatlng,
      zoom: 15,
      mapTypeControl: true,
    });

    marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      draggable:true,
      animation: google.maps.Animation.DROP
    });

    return map;
  }

  this.addInputField = function(element, map){
    var searchBox = new google.maps.places.SearchBox(element);

    marker.addListener('dragend', function(evt){
      myLatlng.lat = evt.latLng.lat();
      myLatlng.lng = evt.latLng.lng();

      reverseGeocode(myLatlng, element);

    });

    reverseGeocode(myLatlng, element);

    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();
      console.log(places);
      if (places.length == 0) {
        return;
      }

      var bounds = new google.maps.LatLngBounds();

      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }

        formatted_address = place.formatted_address;

        myLatlng.lat = place.geometry.location.lat();
        myLatlng.lng = place.geometry.location.lng();

        self.addCirlce(map, myLatlng.lat, myLatlng.lng, radius);
        map.panTo(place.geometry.location);
        map.setZoom(9);
        marker.setPosition(place.geometry.location);

        if (place.geometry.viewport) {
          bounds.union(place.geometry.viewport);
        }
        else {
          bounds.extend(place.geometry.location);
        }
      });

      map.fitBounds(bounds);
    });
  }

  this.addCirlce = function(map, lat, lng, r){
    if(r != null){
      radius = r;
    }
    else
    if(r==null){
      r = radius;
    }
    if(cityCircle !=null){
      cityCircle.setMap(null);
    }
    cityCircle = new google.maps.Circle({
      strokeColor: '#69f0ae',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#ffff00',
      fillOpacity: 0.25,
      map: map,
      center: {lat: lat, lng: lng},
      radius: r*1000
    });
  }

  this.getCurrentPosition = function(){
    return myLatlng;
  }

  this.getFormattedAddress = function(){
    return formatted_address;
  }

  function reverseGeocode(myLatlng, element){
    geocoder.geocode({'location': myLatlng}, function(results, status) {
      if (status === 'OK') {
        if (results[0]) {
          element.value = results[0].formatted_address;
          formatted_address = results[0].formatted_address;
        }
        else {
          console.log('No results found for reverse geocode');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
    });
  }

});

customerApp.controller('CustomerAddressController',['$scope', '$timeout', '$location', '$compile', '$routeParams', '$location', 'gmap', function($scope, $timeout, $location, $compile, $routeParams, $location, gmap){

  var currentUrl = $location.absUrl();
  var tmp = currentUrl.split('/');

  $scope.case_id = tmp[tmp.length-1];
  $scope.screen = 'homeScreen';
  $scope.customer = {
    address : undefined,
    location : undefined,
    datetime : undefined
  }

  $scope.enterApp = function(){
    $scope.screen = 'address'
  }

  $scope.initializeGoogleMap = function(lat, lng){

    var mapElement = document.getElementById('customer-address-map');
    var mapInputElement = document.getElementById('customer-address-input');

    if(lat==null || lng==null){
      navigator.geolocation.getCurrentPosition(function(pos){
        lat = parseFloat(pos.coords.latitude);
        lng = parseFloat(pos.coords.longitude);

        $scope.map = gmap.initMap(mapElement, lat, lng);
        gmap.addInputField(mapInputElement, $scope.map);

      },
      function(e){
        console.log('Unable to fetch Location', e)
      },
      {
        timeout: 1000,
        enableHighAccuracy: true
      });
    }
    else{
      $scope.map = gmap.initMap(mapElement, lat, lng);
      gmap.addInputField(mapInputElement, $scope.map);
    }
  }

  $scope.screenToAddress = function(){
    $scope.screen = 'address';
  }

  $scope.next = function(){
    $scope.customer.location = gmap.getCurrentPosition();
    $scope.customer.address = gmap.getFormattedAddress();

    $scope.screen = 'datetime';
  }

  $scope.submit = function(){
    $scope.loading = true;

    var data = {
      latitude: $scope.customer.location.lat,
      longitude: $scope.customer.location.lng,
      inspection_time: $scope.customer.datetime,
      ic: $scope.case_id
    }
    console.log('Submit Data ', data);
    $.ajax({
      type: "POST",
      url: "../util/submit-address",
      data: data,
      success: function(){
        $scope.loading = false;
        $scope.screen = 'thankyou';
      },
      error: function(err ){
        $scope.loading = false;
        alert('Error! '+ err.responseJSON.error_message);

        if(err.responseJSON.error_message == "Case has already been updated!")
          $scope.screen = 'thankyou';

        $scope.$apply();
      }
    });
  }

  angular.element(document).ready(function () {
    $scope.initializeGoogleMap(null, null);
  });

}]);
