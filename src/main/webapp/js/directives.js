/*
 * Angular Time Difference
 * Calculate the hh mm and ss between two times
 *
 *
 *
 */

/* global angular */

jaduApp.directive('ilnTimeDifference', [
        function () {
        return {
            restrict: 'E',
            template:
                '<div id="iln-time-diff">'+
                '<span id="iln-time-diff-hour">{{ hour }}</span>'+
                '<span id="iln-time-diff-minute">{{ minute }}</span>'+
                '<span id="iln-time-diff-second">{{ second }}</span>'+
                '</div>',
            link:   function ( scope, elm, attrs ) {

                var startTime   = attrs.startTime;
                var endTime     = new Date();
                var initialTime = endTime - startTime;
                console.log(initialTime)

                scope.convertTime = function( _milsec ){
                    var _sec = _milsec / 1000;

                    scope.hour = Math.floor(((_sec % 31536000) % 86400) / 3600);
                    scope.minute = Math.floor((((_sec % 31536000) % 86400) % 3600) / 60);
                    scope.second = Math.floor(((_sec % 31536000) % 86400) % 3600) % 60;
                };
                scope.convertTime( initialTime );
            }
        };
    }]);





    angular.module('jaduApp').directive('ngRightClick', function($parse) {
      return function(scope, element, attrs) {
        var fn = $parse(attrs.ngRightClick);
        element.bind('contextmenu', function(event) {
          scope.$apply(function() {
            event.preventDefault();
            fn(scope, {
              $event: event
            });
          });
        });
      };
    });
