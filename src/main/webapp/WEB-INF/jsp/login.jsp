<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="WIMWIsure - Back Office">
<meta name="keywords" content="WimwiSure, QC, ADMIN, DASHBOARD, wimwisure">
<title>WIMWIsure - Back Office</title>
<!-- Site favicon -->
<link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />
<!-- /site favicon -->

<!-- Entypo font stylesheet -->
<link href="css/entypo.css" rel="stylesheet">
<!-- /entypo font stylesheet -->

<!-- Font awesome stylesheet -->
<link href="css/font-awesome.min.css" rel="stylesheet">
<!-- /font awesome stylesheet -->

<!-- CSS3 Animate It Plugin Stylesheet -->
<link href="css/plugins/css3-animate-it-plugin/animations.css" rel="stylesheet">
<!-- /css3 animate it plugin stylesheet -->

<!-- Bootstrap stylesheet min version -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- /bootstrap stylesheet min version -->

<!-- Mouldifi core stylesheet -->
<link href="css/mouldifi-core.css" rel="stylesheet">
<!-- /mouldifi core stylesheet -->

<link href="css/mouldifi-forms.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
<![endif]-->
<script src="js/jquery.min.js"></script>
<style type="text/css">
    .alert-progress{
        display: none;
    }
    
    .alert-error{
        display: none;
    }
</style>
<script>
    $(document).ready(function(){
        
        $("#login").click(function(){
            var username = $("#username").val();
            var password = $("#password").val();
            
            $(".alert-progress").css("display", "block");
            $(".alert-progress").html("Logging in. Please wait!");
            
            $(".alert-error").css("display", "none");
            
            $.post(
                    "oauth/token", 
                    {
                        client_id : "jadu-android-app",
                        grant_type : "password",
                        username : username,
                        password : password
                    }, 
                    function(data) {
                        $(".alert-progress").css("display", "none");
                        localStorage.setItem("access_token", data.access_token);
                        localStorage.setItem("refresh_token", data.refresh_token);
                        window.location = "./#";
            }).fail(function(a, b, c) {
                $(".alert-progress").css("display", "none");
                $(".alert-error").css("display", "block");
                $(".alert-error").html(a.responseJSON.error_description);
            });
            return false;
        });
    });
    
</script>
</head>
<body class="login-page">
	<div class="login-pag-inner">
		<div class="animatedParent animateOnce z-index-50">
			<div class="login-container animated growIn slower">
                                <br/><br/>
				<div class="login-content">
                                    <div class="login-branding" style="padding: 0px;">
                                        <a href="http://wimwisure.com" target="_blank"><img src="images/logo.png" alt="WIMWIsure" title="WIMWIsure"></a>
                                    </div>
                                    <br/>
					<h3><strong>Welcome to WIMWIsure - Back Office</strong></h3>
					<form method="post" action="#">                        
						<div class="form-group">
                                                    <input name="username" id="username" type="text" placeholder="Username" class="form-control">
						</div>                        
						<div class="form-group">
                                                    <input name="password" id="password" type="password" placeholder="Password" class="form-control">
						</div>
						<div class="form-group">
							 <div class="checkbox checkbox-replace">
								<input type="checkbox" id="remeber">
								<label for="remeber">Remember me</label>
							  </div>
						 </div>
						<div class="form-group">
							<button id="login" type='submit'  class="btn btn-primary btn-block">Login</button>
						</div>
						<div class="alert alert-success alert-progress" role="alert">
                                                    
                                                </div>
                                                <div class="alert alert-danger alert-error" role="alert">
                                                    
                                                </div>
					</form>
				</div>
			</div>
		</div>
	</div>
    
    <div style="position: fixed; bottom: 0px; height: 40px; padding: 10px; width: 100%; background-color: black; text-align: center;">
        We use cookies to improve your experience. By your continued use of this site you accept such use.
    </div>
    
<script src="js/plugins/css3-animate-it-plugin/css3-animate-it.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
