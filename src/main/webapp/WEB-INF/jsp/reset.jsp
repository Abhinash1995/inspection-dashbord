<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="WIMWIsure - Back Office">
<meta name="keywords"
	content="WimwiSure, QC, ADMIN, DASHBOARD, wimwisure">
<title>WIMWIsure - Back Office</title>
<!-- Site favicon -->
<link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />
<!-- /site favicon -->

<!-- Entypo font stylesheet -->
<link href="css/entypo.css" rel="stylesheet">
<!-- /entypo font stylesheet -->

<!-- Font awesome stylesheet -->
<link href="css/font-awesome.min.css" rel="stylesheet">
<!-- /font awesome stylesheet -->

<!-- CSS3 Animate It Plugin Stylesheet -->
<link href="css/plugins/css3-animate-it-plugin/animations.css"
	rel="stylesheet">
<!-- /css3 animate it plugin stylesheet -->

<!-- Bootstrap stylesheet min version -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- /bootstrap stylesheet min version -->

<!-- Mouldifi core stylesheet -->
<link href="css/mouldifi-core.css" rel="stylesheet">
<!-- /mouldifi core stylesheet -->

<link href="css/mouldifi-forms.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
<![endif]-->
<script src="js/jquery.min.js"></script>
<style type="text/css">
.alert-progress {
	display: none;
}

.alert-error {
	display: none;
}
</style>
<script>
	$(document)
			.ready(
					function() {

						var accessToken = localStorage.getItem('access_token');

						if (!accessToken || accessToken === '') {
							alert("Please login first to change the password!");
						}

						$("#reset-password")
								.click(
										function() {
											var password = $("#password").val();
											var confirm_password = $(
													"#password2").val();
											$(".alert-progress").css("display",
													"block");
											$(".alert-progress").html(
													"Logging in. Please wait!");

											$(".alert-error").css("display",
													"none");

											$
													.ajax({
														url : "/user/web/change-password",
														type : 'post',
														headers : {
															Authorization : "Bearer "
																	+ localStorage
																			.getItem('access_token')
														},
														data : {

															new_password : password,
															confirm_password : confirm_password
														},
														success : function(data) {
															$(".alert-progress")
																	.css(
																			"display",
																			"block");
															$(".alert-error")
																	.css(
																			"display",
																			"nonde");
															$(".alert-progress")
																	.html(
																			"Password has been changed successfully.");
															window.localStorage
																	.removeItem("access_token");
															window.localStorage
																	.removeItem("refresh_token");
															window.localStorage
																	.removeItem("userProfile");
															window.localStorage
																	.removeItem("userConfiguration");
															window.location = "./#";

														},
														error : function() {
															alert("Unable to change password. Please contact administrator!");
														}
													});
											return false;
										});
					});
</script>
</head>
<body class="login-page">
	<div class="login-pag-inner">
		<div class="animatedParent animateOnce z-index-50">
			<div class="login-container animated growIn slower">
				<br /> <br />
				<div class="login-content">
					<div class="login-branding" style="padding: 0px;">
						<a href="http://wimwisure.com" target="_blank"><img
							src="images/logo.png" alt="WIMWIsure" title="WIMWIsure"></a>
					</div>
					<br />
					<h3>
						<strong>Welcome to WIMWIsure - Reset Password Portal</strong>
					</h3>
					<form method="post" action="#">
						<div class="form-group">
							<input name="password" id="password" type="password"
								placeholder="New Password" class="form-control"> <input
								name="password2" id="password2" type="password"
								placeholder="Confirm Password" class="form-control">

						</div>
						<div class="form-group">
							<button id="reset-password" type='submit'
								class="btn btn-primary btn-block">Change Password</button>
						</div>
						<div class="alert alert-success alert-progress" role="alert">

						</div>
						<div class="alert alert-danger alert-error" role="alert"></div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div
		style="position: fixed; bottom: 0px; height: 40px; padding: 10px; width: 100%; background-color: black; text-align: center;">
		We use cookies to improve your experience. By your continued use of
		this site you accept such use.</div>

	<script src="js/plugins/css3-animate-it-plugin/css3-animate-it.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
