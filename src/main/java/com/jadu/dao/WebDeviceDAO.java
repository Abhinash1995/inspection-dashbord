package com.jadu.dao;

import com.jadu.model.Device;
import com.jadu.model.WebDevice;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class WebDeviceDAO  extends DeviceDAO{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    @Override
    public Device get(
            String deviceId,
            String username
    ){
        return (Device)this.sessionFactory
                .getCurrentSession()
                .createCriteria(WebDevice.class)
                .add(Restrictions.eq("deviceId", deviceId))
                .add(Restrictions.eq("username", username))
                .uniqueResult();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void delete(String username){
        Query q = this.sessionFactory.getCurrentSession().createQuery("delete WebDevice where username = :username");
        q.setString("username", username);
        q.executeUpdate();
    }
}
