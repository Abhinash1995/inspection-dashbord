package com.jadu.dao;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

public class DefaultCommentDao {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getAll(){
        String hql = "FROM DefaultComment ORDER BY id ASC";
        return this.sessionFactory.getCurrentSession().createQuery(hql).list();
    }
	

}
