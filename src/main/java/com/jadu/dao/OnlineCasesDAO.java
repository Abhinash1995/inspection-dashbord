package com.jadu.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.OnlineCases;

public class OnlineCasesDAO {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(OnlineCases onlineCases) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(onlineCases);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public OnlineCases findByCaseId(Long caseId) {
		String hql = "SELECT ic FROM OnlineCases ic  WHERE ic.inspectionCaseId = ?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setLong(0, caseId);
		List<OnlineCases> onlineCases = query.list();
		if (onlineCases != null && !onlineCases.isEmpty())
			return onlineCases.get(0);
		else
			return null;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<OnlineCases> findByCreatedTime(Date thresholdTime) {
		String hql = "SELECT ic FROM OnlineCases ic  WHERE ic.createdDate >= ? AND ic.isStatusUpdated=0 ";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setDate(0, thresholdTime);
		List<OnlineCases> onlineCases = query.list();
		return onlineCases;

	}

}
