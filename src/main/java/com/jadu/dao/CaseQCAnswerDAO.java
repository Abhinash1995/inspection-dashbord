package com.jadu.dao;

import com.jadu.model.CaseQCAnswer;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class CaseQCAnswerDAO {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void save( CaseQCAnswer answer){
        this.sessionFactory.getCurrentSession().saveOrUpdate(answer);
    }
    
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getQCAnswers( long caseId){
        String hql = "select new com.jadu.dto.CaseQCAnswerDTO( c.id, c.question.id, c.option.id) from CaseQCAnswer c join c.inspectionCase ic where ic.id=:id";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setLong("id", caseId)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public int deleteAnswersForCase(long caseId){
        String hql = "delete from CaseQCAnswer c  where c.inspectionCase.id=:caseId";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setLong("caseId", caseId)
                .executeUpdate();
    }
}
