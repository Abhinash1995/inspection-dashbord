package com.jadu.dao;

import com.jadu.model.CaseAttemptAllocation;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class CaseAttemptAllocationDAO {
    
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void save( CaseAttemptAllocation caseAttemptAllocation){
        this.sessionFactory.getCurrentSession().saveOrUpdate(caseAttemptAllocation);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List get(long caseId){
        String hql = "SELECT new com.jadu.dto.CaseAttemptAllocationDTO(c.id, c.inspector.username, c.attemptTime, c.attemptExpiryTime) from CaseAttemptAllocation c join c.inspectionCase ic where c.id=:id";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setLong("id", caseId)
                .list();
    }
}
