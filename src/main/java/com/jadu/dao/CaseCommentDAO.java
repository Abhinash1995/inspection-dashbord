package com.jadu.dao;

import com.jadu.dto.CaseCommentDTO;
import com.jadu.model.CaseComment;
import com.jadu.model.InspectionCase;
import com.jadu.model.User;

import java.util.Collection;
import java.util.Date;
<<<<<<< HEAD
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
<<<<<<< HEAD
=======
import java.util.List;
import org.hibernate.SessionFactory;
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
=======
>>>>>>> inspection-dashbord
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class CaseCommentDAO {
	private SessionFactory sessionFactory;
<<<<<<< HEAD
<<<<<<< HEAD
	
	@Autowired
	DefaultCommentDao defaultCommentDao;
=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
=======
	
	@Autowired
	DefaultCommentDao defaultCommentDao;
>>>>>>> inspection-dashbord

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
<<<<<<< HEAD
	public void add(InspectionCase ic, String comment, Long commentId, User user) {
		CaseComment cc = new CaseComment();
<<<<<<< HEAD
		cc.setComment(comment);
		if(null != commentId) {
			cc.setDefaultComment(defaultCommentDao.get(commentId));	
		}
//		cc.setCommentId(commentId);
=======
	public void add(InspectionCase ic, String comment, User user) {
		CaseComment cc = new CaseComment();
		cc.setComment(comment);
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
=======
>>>>>>> inspection-dashbord
		cc.setInspectionCase(ic);
		cc.setCommentTime(new Date());
		cc.setUser(user);
		
		String dropdownComment = "";
		if(null != commentId) {
			dropdownComment = defaultCommentDao.get(commentId).getComment();
		}
		
		if(null != commentId && null != comment) {
			cc.setComment(dropdownComment + " - " + comment);
			this.sessionFactory.getCurrentSession().saveOrUpdate(cc);
		}else if(null != commentId && null == comment){
			cc.setComment(dropdownComment);
			this.sessionFactory.getCurrentSession().saveOrUpdate(cc);
		}else if(null == commentId && null != comment){
			cc.setComment(comment);
			this.sessionFactory.getCurrentSession().saveOrUpdate(cc);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<CaseCommentDTO> get(long caseId) {
<<<<<<< HEAD
<<<<<<< HEAD
		String hql = "SELECT new com.jadu.dto.CaseCommentDTO(c.id, c.comment, c.commentTime, c.user.firstName, c.user.lastName) from CaseComment c join c.inspectionCase ic  where ic.id=:id and c.comment is not null";
		List<CaseCommentDTO> comments =  this.sessionFactory.getCurrentSession().createQuery(hql).setLong("id", caseId).list();
		
		String dropDown = "SELECT new com.jadu.dto.CaseCommentDTO(c.id, c.defaultComment.comment, c.commentTime, c.user.firstName, c.user.lastName) from CaseComment c join c.inspectionCase ic  where ic.id=:id";
		List<CaseCommentDTO> dropDownComments =  this.sessionFactory.getCurrentSession().createQuery(dropDown).setLong("id", caseId).list();
		comments.addAll(dropDownComments);
=======
		String hql = "SELECT new com.jadu.dto.CaseCommentDTO(c.id, c.comment, c.commentTime, c.user.firstName, c.user.lastName) from CaseComment c join c.inspectionCase ic  where ic.id=:id and c.comment is not null";
		List<CaseCommentDTO> comments =  this.sessionFactory.getCurrentSession().createQuery(hql).setLong("id", caseId).list();
		
//		String dropDown = "SELECT new com.jadu.dto.CaseCommentDTO(c.id, c.defaultComment.comment, c.commentTime, c.user.firstName, c.user.lastName) from CaseComment c join c.inspectionCase ic  where ic.id=:id";
//		List<CaseCommentDTO> dropDownComments =  this.sessionFactory.getCurrentSession().createQuery(dropDown).setLong("id", caseId).list();
//		comments.addAll(dropDownComments);
		
>>>>>>> inspection-dashbord
		return comments;
=======
		String hql = "SELECT new com.jadu.dto.CaseCommentDTO(c.id, c.comment, c.commentTime, c.user.firstName, c.user.lastName) from CaseComment c join c.inspectionCase ic  where ic.id=:id";

		return this.sessionFactory.getCurrentSession().createQuery(hql).setLong("id", caseId).list();
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List count(long caseId) {
		String hql = "SELECT FROM CaseComment c join c.inspectionCase ic  where ic.id=:id";

		return this.sessionFactory.getCurrentSession().createQuery(hql).setLong("id", caseId).list();
	}

<<<<<<< HEAD
	
=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Collection<CaseCommentDTO> getCommentsByCommentTimeAndCaseId(long id, Date from, Date to) {
		String hql = "SELECT new com.jadu.dto.CaseCommentDTO(c.id, c.comment, c.commentTime, c.user.firstName, c.user.lastName, ic.id) from CaseComment c join c.inspectionCase ic  where ic.id=:id and (c.commentTime>=:from and c.commentTime<=:to)";
		return this.sessionFactory.getCurrentSession().createQuery(hql).setLong("id", id).setDate("from", from)
				.setDate("to", to).list();
	}
}
