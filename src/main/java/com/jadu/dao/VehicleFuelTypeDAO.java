package com.jadu.dao;

import com.jadu.model.VehicleFuelType;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class VehicleFuelTypeDAO {
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getAll(){
        return this.sessionFactory
                .getCurrentSession()
                .createCriteria(VehicleFuelType.class)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public VehicleFuelType get(String id){
        return  (VehicleFuelType) this.sessionFactory
                .getCurrentSession()
                .get(VehicleFuelType.class, id);
    }
}
