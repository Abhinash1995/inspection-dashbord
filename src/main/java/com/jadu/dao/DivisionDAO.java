package com.jadu.dao;

import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class DivisionDAO {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getAll(int state){
        String hql = "from Division where state=:state ";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setParameter("state", state)
                .list();
    }
}
