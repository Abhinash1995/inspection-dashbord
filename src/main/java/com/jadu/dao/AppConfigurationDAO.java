package com.jadu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.AppConfiguration;
import com.jadu.service.AppConfigService;

public class AppConfigurationDAO {
	private SessionFactory sessionFactory;

	@Autowired
	private AppConfigService appConfigService;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(AppConfiguration appConfiguration) {
		this.sessionFactory.getCurrentSession().persist(appConfiguration);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(AppConfiguration appConfiguration) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(appConfiguration);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getConfigurationsByTag(String tag) {
		String hql = "FROM AppConfiguration c WHERE c.tag=:tag";
		return this.sessionFactory.getCurrentSession().createQuery(hql).setString("tag", tag).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public String getConfigurationValueByTag(String tag) {
		String hql = "SELECT c.value FROM AppConfiguration c WHERE c.tag=:tag";

		return (String) this.sessionFactory.getCurrentSession().createQuery(hql).setString("tag", tag).uniqueResult();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public String getConfigurationValueByTag(String tag, String defaultValue) {
		String hql = "SELECT c.value FROM AppConfiguration c WHERE c.tag=:tag";

		Object result = this.sessionFactory.getCurrentSession().createQuery(hql).setString("tag", tag).uniqueResult();

		return result != null ? (String) result : defaultValue;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public boolean getConfigurationBooleanValueByTag(String tag) {
		String hql = "SELECT c.value FROM AppConfiguration c WHERE c.tag=:tag";

		return Boolean.parseBoolean(
				(String) this.sessionFactory.getCurrentSession().createQuery(hql).setString("tag", tag).uniqueResult());
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public boolean getConfigurationBooleanValueByTag(String tag, Boolean defaultValue) {
		String hql = "SELECT c.value FROM AppConfiguration c WHERE c.tag=:tag";

		Object result = this.sessionFactory.getCurrentSession().createQuery(hql).setString("tag", tag).uniqueResult();

		return result != null ? Boolean.parseBoolean((String) result) : defaultValue;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public int getConfigurationIntegerValueByTag(String tag) {
		String hql = "SELECT c.value FROM AppConfiguration c WHERE c.tag=:tag";

		return Integer.parseInt(
				(String) this.sessionFactory.getCurrentSession().createQuery(hql).setString("tag", tag).uniqueResult());
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public int getConfigurationIntegerValueByTag(String tag, Integer defaultValue) {
		String hql = "SELECT c.value FROM AppConfiguration c WHERE c.tag=:tag";

		Object result = this.sessionFactory.getCurrentSession().createQuery(hql).setString("tag", tag).uniqueResult();

		return result != null ? Integer.parseInt((String) result) : defaultValue;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public AppConfiguration findByTag(String tag) {
		String hql = "FROM AppConfiguration c WHERE c.tag=:tag";

		return (AppConfiguration) this.sessionFactory.getCurrentSession().createQuery(hql).setString("tag", tag)
				.uniqueResult();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<AppConfiguration> findAll() {
		String hql = "FROM AppConfiguration";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}
}
