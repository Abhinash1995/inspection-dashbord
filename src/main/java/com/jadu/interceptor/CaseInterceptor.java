/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.interceptor;

import com.jadu.dao.CompanyBranchDivisionDAO;
import com.jadu.model.CompanyBranchDivision;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author gkumar
 */
public class CaseInterceptor extends HandlerInterceptorAdapter{
    
    @Autowired
    CompanyBranchDivisionDAO companyBranchDivisionDAO; 
    
    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception {
        
        
        String value = (request.getHeader("Authorization"));
        
        String[] array = value.split(" +");
        
        if(value == null || "".equals(value) || array.length < 2){
            response.setStatus( 401 );
            throw new Exception("INVALID KEY");
        }
        
//        String key = array[1];
//        
//        CompanyBranchDivision cbd = companyBranchDivisionDAO.getByApiKey(key);
//        
//        if(cbd == null){
//            response.setStatus( 401 );
//            response.setContentType("application/json");
//            throw new Exception("INVALID KEY");
//        }
//        
//        request.setAttribute("branch_id", cbd.getId());
        
        return true;
    }
}
