package com.jadu.front.controller;

import com.jadu.api.ApiSwitch;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
<<<<<<< HEAD
import javax.swing.text.StyledEditorKit.BoldAction;
=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.jadu.dao.AgentCommentDAO;
import com.jadu.dao.AgentDAOImpl;
import com.jadu.dao.CaseCommentDAO;
import com.jadu.dao.CaseDAO;
import com.jadu.dao.CaseLogDAO;
import com.jadu.dao.CasePhotoDAO;
import com.jadu.dao.CaseQCAnswerDAO;
import com.jadu.dao.CaseQCQuestionDAO;
import com.jadu.dao.CaseQCQuestionOptionDAO;
import com.jadu.dao.CaseUserAllocationDAO;
<<<<<<< HEAD
import com.jadu.dao.DefaultCommentDao;
=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
import com.jadu.dao.InspectionTypeDAO;
import com.jadu.dao.InspectorDAO;
import com.jadu.dao.InsuranceCompanyDAO;
import com.jadu.dao.OnlineCasesDAO;
import com.jadu.dao.PhotoTypeDAO;
import com.jadu.dao.PurposeOfInspectionDAO;
import com.jadu.dao.ReUploadPhotoDao;
import com.jadu.dao.UserDAOImpl;
import com.jadu.dao.VehicleDAO;
import com.jadu.dao.VehicleFuelTypeDAO;
import com.jadu.dao.VehicleSubTypeDAO;
import com.jadu.dao.VehicleTypeDAO;
import com.jadu.dto.AgentCommentDTO;
import com.jadu.dto.AgentsDTO;
import com.jadu.dto.AllCases;
import com.jadu.dto.QCQuestionDTO;
import com.jadu.dto.ScheduledCasesDTO;
import com.jadu.dto.factory.CaseQCQuestionOptionFactory;
import com.jadu.dto.factory.QCQuestionsFactory;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.helpers.BoundingBox;
import com.jadu.helpers.TimeHelper;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.Admin;
import com.jadu.model.Agent;
import com.jadu.model.CaseComment;
import com.jadu.model.CasePhoto;
import com.jadu.model.CaseQCAnswer;
import com.jadu.model.CaseQCQuestion;
import com.jadu.model.CaseQCQuestionOption;
import com.jadu.model.CaseType;
import com.jadu.model.CaseUserAllocation;
import com.jadu.model.CompanyBranchDivision;
import com.jadu.model.CompanyBranchDivisionUser;
import com.jadu.model.CompanyUser;
import com.jadu.model.GeoLocation;
import com.jadu.model.InspectionCase;
import com.jadu.model.Inspector;
import com.jadu.model.OnlineCases;
import com.jadu.model.PurposeOfInspection;
import com.jadu.model.QC;
import com.jadu.model.ReUploadPhoto;
import com.jadu.model.Vehicle;
import com.jadu.model.builder.CaseUserAllocationBuilder;
import com.jadu.model.builder.InspectionCaseBuilder;
import com.jadu.pdf.create.GenerateReport;
import com.jadu.push.notification.AndroidPushNotificationsService;
import com.jadu.service.AppConfigService;
import com.jadu.service.CallbackService;
import com.jadu.service.CaseService;
import com.jadu.service.ConnectCustomerToFlow;
import com.jadu.service.CreateUserService;
import com.jadu.service.GoogleLocationService;
import com.jadu.service.MailService;
import com.jadu.service.ReconcileService;
import com.jadu.service.RedisService;
import com.jadu.service.S3DirectUploadService;
import com.jadu.service.S3Service;
import com.jadu.service.SmsService;
import com.jadu.service.URLShortnerService;
import com.jadu.service.UtilService;
import java.util.Arrays;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/qc")
public class QCController {

	@Autowired
	UserDAOImpl userDAO;

	@Autowired
	AgentDAOImpl agentDAO;

	@Autowired
	PurposeOfInspectionDAO poiDAO;

	@Autowired
	InsuranceCompanyDAO insuranceCompanyDAO;

	@Autowired
	CaseDAO caseDAO;

	@Autowired
	CaseQCQuestionDAO caseQCQuestionDAO;

	@Autowired
	CaseQCQuestionOptionDAO caseQCQuestionOptionDAO;

	@Autowired
	CaseQCAnswerDAO caseQCAnswerDAO;

	@Autowired
	GenerateReport generateReportService;

	@Autowired
	SmsService smsService;

	@Autowired
	AndroidPushNotificationsService androidPushNotificationsService;

	@Autowired
	URLShortnerService urlShortnerService;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	CaseLogDAO caseLogDAO;

	@Autowired
	S3Service s3Service;

	@Autowired
	Environment env;

	@Autowired
	MailService mailService;

	@Autowired
	CasePhotoDAO casePhotoDAO;

	@Autowired
	VehicleDAO vehicleDAO;

	@Autowired
	VehicleFuelTypeDAO vehicleFuelTypeDAO;

	@Autowired
	VehicleTypeDAO vehicleTypeDAO;

	@Autowired
	S3DirectUploadService s3DirectUploadService;

	@Autowired
	CaseService caseService;

	@Autowired
	InspectorDAO inspectorDAO;

	@Autowired
	InspectionTypeDAO inspectionTypeDAO;

	@Autowired
	CreateUserService createUserService;

	@Autowired
	CaseCommentDAO caseCommentDAO;

	@Autowired
	VehicleSubTypeDAO vehicleSubTypeDAO;

	@Autowired
	PhotoTypeDAO photoTypeDAO;

	@Autowired
	ConnectCustomerToFlow connectCustomerToFlow;

	@Autowired
	CaseUserAllocationDAO caseUserAllocationDAO;

	@Autowired
	private RedisService redisService;

	@Autowired
	private UtilService utilService;

	@Autowired
	private AgentCommentDAO agentCommentDAO;

	@Autowired
	private ReconcileService reconcileService;

	@Autowired
	private CallbackService callbackService;
<<<<<<< HEAD

	@Autowired
	private OnlineCasesDAO onlineCasesDAO;

	@Autowired
	DefaultCommentDao defaultCommentDao;
	
	@Autowired
	ReUploadPhotoDao reUploadPhotoDao;

	@Autowired
	private GoogleLocationService googleLocationService;
=======
        
        @Autowired
        private OnlineCasesDAO onlineCasesDAO;
        
        @Autowired
        private GoogleLocationService googleLocationService;
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb

	private static final Logger LOGGER = LoggerFactory.getLogger(QCController.class);

	@RequestMapping(value = "/cases/inspection", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String submitInspectionFile(@RequestParam(value = "case_id", required = true) int caseId,
			@RequestParam(value = "case_file", required = true) MultipartFile caseFile) throws Exception {
		JSONObject result = new JSONObject();

		InspectionCase c = (InspectionCase) caseDAO.getCaseById(caseId);

		if (c == null)
			throw new Exception("Unable to find case with given id. ");

		if (c.getCurrentStage() > 3)
			throw new Exception("Case has already been submitted to QC!");

		if (c.getCurrentStage() < 3)
			throw new Exception("Case has not been scheduled yet!");

		String extension = FilenameUtils.getExtension(caseFile.getOriginalFilename());

		if (!extension.equals("zip"))
			throw new Exception("Invalid zip file!");

		s3Service.uploadMultipartFileDirect(caseFile, "cases/" + c.getId() + "/" + c.getId() + ".zip");

		if (s3Service.exists("cases/" + c.getId() + "/" + c.getId() + ".zip")) {

			caseService.uploadZipToS3(caseId, "cases/" + c.getId() + "/" + c.getId() + ".zip", false);
			c.setLastUpdatedTime(new Date());
			c.setCurrentStage(4);
			caseDAO.save(c);
<<<<<<< HEAD
			caseService.addComments(c, "File uploaded for Manual QC ");
=======

>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
			result.put("success", true);
		} else {
			throw new Exception("Inspection file not uploaded to server!");
		}

		return result.toString();

	}

	@RequestMapping(value = "/get-inspection-types")
	@ResponseBody
	public List getInspectionTypes() throws JSONException, Exception {
		return inspectionTypeDAO.getAll();
	}

	@RequestMapping(value = "/move-to-qc")
	@ResponseBody
	public void moveToQC(@RequestParam(value = "case_id", required = true) long caseId,
			@RequestParam(value = "vehicle_type", required = true) String type) throws JSONException, Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(((User) authentication.getPrincipal()).getUsername());
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);
		Integer currentStage = ic.getCurrentStage();
		String vehicleType = ic.getVehicleType() != null ? ic.getVehicleType().getId() : null;
		if (ic.getCurrentStage() != 3)
			return;
		ic.setCurrentStage(4);
		ic.setInspectionSubmitTime(new Date());
		ic.setVehicleType(vehicleTypeDAO.get(type));
		ic.setLastUpdatedTime(new Date());
		caseDAO.save(ic);
<<<<<<< HEAD
<<<<<<< HEAD
		//adding comments
=======
		// adding comments
>>>>>>> inspection-dashbord
		caseService.addComments(ic, "Moved case from stage: " + currentStage + " to stage: " + ic.getCurrentStage()
				+ " and vehicleType from: " + vehicleType + " to :" + type);
=======
		try {
			caseCommentDAO.add(ic, "Moved case from stage: " + currentStage + " to stage: " + ic.getCurrentStage()
					+ " and vehicleType from: " + vehicleType + " to :" + type, user);
			if (appConfigService.getBooleanProperty("IS_TO_UPDATE_COMMENT_IN_CACHE", true))
				redisService.updateCaseComment(ic, "Moved case from stage: " + currentStage + " to stage: "
						+ ic.getCurrentStage() + " and vehicleType from: " + vehicleType + " to :" + type, user);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception raised while adding comment, caseId={}, vehicle_type={},user={}, error={}", caseId,
					type, user.getPhoneNumber(), e.getMessage());
		}
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
		if (appConfigService.getBooleanProperty("IS_TO_REMOVE_FROM_SCHEDULED_CACHED_CASES", true)) {
			try {
				redisService.removeFromScheduledCases(user, ic);
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error(
						"Exception raised while removing case from scheduled case, caseId={}, vehicle_type={},user={}, error={}",
						caseId, type, user.getPhoneNumber(), e.getMessage());
			}
		}
	}

	@RequestMapping(value = "/vehicle", method = RequestMethod.POST)
	@ResponseBody
	public void addVehicle(@RequestParam(value = "vehicle_make", required = true) String make,
			@RequestParam(value = "vehicle_model", required = true) String model,
			@RequestParam(value = "vehicle_type", required = true) String type,
			@RequestParam(value = "vehicle_subtype", required = true) String subtype) throws JSONException, Exception {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(((User) authentication.getPrincipal()).getUsername());

		Vehicle vehicle = new Vehicle();
		vehicle.setMake(make);
		vehicle.setModel(model);
		vehicle.setSubType(vehicleSubTypeDAO.get(subtype));
		vehicle.setVehicleType(vehicleTypeDAO.get(type));
		vehicle.setCreatedBy(user.getUsername());
		vehicleDAO.save(vehicle);
	}

	@RequestMapping(value = "/vehicle/toggle", method = RequestMethod.POST)
	@ResponseBody
	public void addVehicle(@RequestParam(value = "vehicle_id", required = true) int vehicle_id,
			@RequestParam(value = "enabled", required = true) boolean enabled) throws JSONException, Exception {

		Vehicle vehicle = vehicleDAO.getVehicleById(vehicle_id);

		vehicle.setEnabled(enabled);

		vehicleDAO.save(vehicle);
	}

	@RequestMapping(value = "/get-inspectors-in-radius")
	@ResponseBody
	public List getInspectorInRadius(@RequestParam(value = "latitude", required = true) Double latitude,
			@RequestParam(value = "longitude", required = true) Double longitude,
			@RequestParam(value = "radius", required = true) Double radius) throws JSONException, Exception {
		List<GeoLocation> result = BoundingBox.getBoundingBox(latitude, longitude, radius);
		return inspectorDAO.getInspectorBoundingBox(result.get(0), result.get(1));
	}

	@RequestMapping(value = "/assign-case-to-inspector")
	@ResponseBody
	public void assignCaseToInspector(@RequestParam(value = "case_id", required = true) long caseId,
			@RequestParam(value = "username", required = true) String username) throws JSONException, Exception {
		Inspector inspector = inspectorDAO.getInspectorByUsername(username);

		if (inspector == null)
			throw new Exception("Inspector doesn't exists with given username");

		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Inspection case doesn't exists with given id!");

		if (ic.getCurrentStage() != 1 || ic.getCurrentStage() != 2)
			throw new Exception("Invalid stage!");

		ic.addCaseUserAllocation(new CaseUserAllocationBuilder(ic, inspector, 3).build());

		ic.setCurrentStage(3);
		ic.setLastUpdatedTime(new Date());
		caseDAO.save(ic);
	}

	@RequestMapping(value = "/assign-case-to-inspector-by-phone")
	@ResponseBody
	public void assignCaseToInspectorByPhone(@RequestParam(value = "case_id", required = true) long caseId,
			@RequestParam(value = "phone_number", required = true) String phoneNumber) throws JSONException, Exception {
		Inspector inspector = (Inspector) inspectorDAO.getInspectorByPhoneNumber(phoneNumber);

		if (inspector == null)
			throw new Exception("Inspector doesn't exists with given username");

		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Inspection case doesn't exists with given id!");

		if (ic.getCurrentStage() != 1 || ic.getCurrentStage() != 2)
			throw new Exception("Invalid stage!");

		ic.addCaseUserAllocation(new CaseUserAllocationBuilder(ic, inspector, 3).build());
		ic.setLastUpdatedTime(new Date());
		ic.setCurrentStage(3);

		caseDAO.save(ic);
	}

	@RequestMapping(value = "/manual-qc", method = RequestMethod.POST)
	@ResponseBody
	public void manualQC(@RequestParam(value = "case_id", required = true) long caseId,
			@RequestParam(value = "zip_file", required = true) String zipFile) throws JSONException, Exception {
		zipFile = zipFile.replace("\\", "/");
		caseService.uploadZipToS3(caseId, zipFile, false);
	}

	@RequestMapping(value = "/get-bucket-objects", method = RequestMethod.GET)
	@ResponseBody
	public Object getBucketObjects() {
		return s3DirectUploadService.getObjectslistFromFolder();
	}

	@RequestMapping(value = "/get-configurations", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getConfigurations() throws JSONException {
		JSONObject result = new JSONObject();
		result.put("ENABLE_INSPECTOR", appConfigService.getProperty("ENABLE_INSPECTOR"));
		return result.toString();
	}

	@RequestMapping(value = "/get-user-details", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getUserDetails(@RequestParam(value = "username", required = true) String username)
			throws JSONException {
		return userDAO.getUserByUsername(username);
	}

	@RequestMapping(value = "/get-all-agents", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getAllAgents() throws JSONException {
		if (appConfigService.getBooleanProperty("IS_TO_FETCH_QC_ALL_AGENTS_FROM_CACHE", true)) {
			List<AgentsDTO> allAgents = redisService.getAllAgentDetails("ALL_AGENTS_DETAILS");
			if (allAgents == null) {
				allAgents = redisService.reconcileAllAgents("ALL_AGENTS_DETAILS");
			}
			return allAgents;
		}
		return agentDAO.getAgentPersonalDetails();
	}

	@RequestMapping(value = "/update-vehicle-number", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void updateVehicleNumber(@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "vehicle_number", required = true) String vehicleNumber)
			throws JSONException, Exception {
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);
<<<<<<< HEAD
		String existingVehicleNumber = ic.getVehicleNumber();
=======

>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
		if (ic == null)
			throw new Exception("Unable to get case details!");

		ic.setVehicleNumber(vehicleNumber);
<<<<<<< HEAD
		caseDAO.save(ic);

		// adding comments on vehicle number update
		if (!existingVehicleNumber.equalsIgnoreCase(vehicleNumber))
<<<<<<< HEAD
			caseService.addComments(ic, "Vehicle Number Updated ");
=======

		caseDAO.save(ic);
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
=======
			caseService.addComments(ic,
					"Vehicle Number Updated from  '" + existingVehicleNumber + "'  to  '" + vehicleNumber + "'");
>>>>>>> inspection-dashbord
	}

	@RequestMapping(value = "/update-customer-phone-number", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void updateCustomerPhoneNumber(@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "customer_phone_number", required = true) String customerPhoneNumber)
			throws JSONException, Exception {
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);
<<<<<<< HEAD
		String existingNumber = ic.getCustomerPhoneNumber();
=======

>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
		if (ic == null)
			throw new Exception("Unable to get case details!");

		if (ic.getInspectionType().equals("ASSIGN_TO_CUSTOMER")) {
			com.jadu.model.User customer = userDAO.getUserByPhoneOrEmail(customerPhoneNumber);
<<<<<<< HEAD
=======

>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
			if (customer == null)
				customer = createUserService.createCustomer("", customerPhoneNumber);

			ic.setCustomerPhoneNumber(customerPhoneNumber);
			ic.setLastUpdatedTime(new Date());
			caseDAO.save(ic);
			caseUserAllocationDAO.update(ic, 3, customer);

			String url = urlShortnerService.shortenUrl("customer-inspection/", ic.getId(),
					ic.getInsuranceCompany().getName());
			smsService.sendSMSAsync(customerPhoneNumber,
					"We have an insurance inspection request for " + ic.getVehicleNumber()
							+ ". Please download the app " + url
							+ " to complete the inspection. Call 7290049100 for support");
			connectCustomerToFlow.callCustomerAfterCaseCreate(customerPhoneNumber);
		} else {
			ic.setCustomerPhoneNumber(customerPhoneNumber);
			ic.setLastUpdatedTime(new Date());
			caseDAO.save(ic);
			smsService.sendSMS(ic.getCustomerPhoneNumber(),
					"We have received an inspection request for Vehicle Number - " + ic.getVehicleNumber()
							+ " from Insurance Company. Our agent will contact you shortly.");
		}
<<<<<<< HEAD
		// adding comments on customer phone number update
		if (!existingNumber.equalsIgnoreCase(customerPhoneNumber)) {
			caseService.addComments(ic,
					"Customer Phone Number Updated from  '" + existingNumber + "'  to  '" + customerPhoneNumber + "'");
		}

=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
	}

	@RequestMapping(value = "/update-inspector-phone-number", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void updateInspectorPhoneNumber(@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "phone_number", required = true) String phoneNumber) throws JSONException, Exception {
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);
<<<<<<< HEAD
		String existingNumber = ic.getRequestorPhoneNumber();
=======

>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
		if (ic == null)
			throw new Exception("Unable to get case details!");

		Agent agent = agentDAO.getUniqueAgentByPhoneNumber(phoneNumber);

		if (agent == null)
			throw new Exception("No agent exists with given phone number!");

		if (ic.getInspectionType().equals("SELF_INSPECT")) {
			ic.setRequestorPhoneNumber(phoneNumber);
			ic.setLastUpdatedTime(new Date());
			caseDAO.save(ic);
			caseUserAllocationDAO.update(ic, 3, agent);
		} else {
			ic.setRequestorPhoneNumber(phoneNumber);
			ic.setLastUpdatedTime(new Date());
			caseDAO.save(ic);
		}

<<<<<<< HEAD
		// adding comments on inspector phone number update
		if (!existingNumber.equalsIgnoreCase(phoneNumber)) {
			caseService.addComments(ic, "Agent Phone Number updated from " + existingNumber + " to " + phoneNumber);
		}

=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
	}

	@RequestMapping(value = "/update-case-vehicle-details", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void updateCaseVehicleDetails(@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "vehicle_number", required = true) String vehicleNumber,
			@RequestParam(value = "vehicle_type", required = true) String vehicleType,
			@RequestParam(value = "vehicle_color", required = true) String vehicleColor,
			@RequestParam(value = "make_model_id", required = true) int makeModelId,
			@RequestParam(value = "fuel_type", required = true) String fuelType,
			@RequestParam(value = "yom", required = true) int YOM,
			@RequestParam(value = "chachis_number", required = false, defaultValue = "") String chachis_number,
			@RequestParam(value = "vin_plate_number", required = false, defaultValue = "") String vin_plate_number,
			@RequestParam(value = "odometer_reading", required = false, defaultValue = "") String odometer_reading)
			throws JSONException, Exception {
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Unable to get case details!");

		Vehicle vehicle = vehicleDAO.getVehicleById(makeModelId);

		ic.setVehicleFuelType(vehicleFuelTypeDAO.get(fuelType));
		ic.setVehicleYOM(YOM);
		ic.setVehicleColor(vehicleColor);
		ic.setVehicle(vehicle);
		ic.setVehicleNumber(vehicleNumber);
		ic.setVehicleType(vehicleTypeDAO.get(vehicleType));

		if (!chachis_number.equals("")) {
			casePhotoDAO.updateQCAnswer(caseId, "chachis-number", chachis_number);
			ic.setChassisNumber(chachis_number);
		}

		if (!vin_plate_number.equals("")) {
			casePhotoDAO.updateQCAnswer(caseId, "vin-plate-number", vin_plate_number);
			ic.setEngineNumber(vin_plate_number);
		}

		if (!odometer_reading.equals("")) {
			casePhotoDAO.updateQCAnswer(caseId, "odometer-rpm", odometer_reading);
			ic.setOdometerReading(odometer_reading);
		}
		ic.setLastUpdatedTime(new Date());
		caseDAO.save(ic);
<<<<<<< HEAD
		caseService.addComments(ic, "Vehicle Details updated ");
=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
	}

	@RequestMapping(value = "/cases/get-qc-questions", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<QCQuestionDTO> getQCQuestions(@RequestParam(value = "case_id", required = true) Long caseId)
			throws Exception {
		LOGGER.info("/qc/cases/get-qc-questions request recieved for case={}", caseId);
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Unable to get case details!");

		List<QCQuestionDTO> qcQuestionDtoList = ic.getVehicleType() != null
				? caseQCQuestionDAO.getQuestions(ic.getVehicleType().getId())
				: new ArrayList<QCQuestionDTO>();
		LOGGER.info("/qc/cases/get-qc-questions request finished for case={}", caseId);

		return qcQuestionDtoList;

	}

	@RequestMapping(value = "/cases/get-qc-options", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getQCOptions() throws Exception {

		return caseQCQuestionOptionDAO.getAll();
	}

	@RequestMapping(value = "/cases/case-details", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getCaseDetails(@RequestParam(value = "case_id", required = true) Long caseId) throws Exception {
		LOGGER.info("/qc/cases/case-details request recieved for case={}", caseId);
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);
<<<<<<< HEAD
		ic.setCount((short) ((long) caseDAO.getCompletedCaseByVehicleNumber(ic.getVehicleNumber())));

		if (ic == null)
			throw new Exception("Unable to get case details!");
		LOGGER.info("/qc/cases/case-details request finished for case={}", caseId);
<<<<<<< HEAD
		// logging in comments
//		caseService.addComments(ic, "QC opened ");
=======
>>>>>>> inspection-dashbord
		return ic;
	}

	@RequestMapping(value = "/get-default-comments")
	@ResponseBody
	public List getDefaultComments() throws JSONException, Exception {
		return defaultCommentDao.getAll();
=======
		ic.setCount(  (short) ((long) caseDAO.getCompletedCaseByVehicleNumber(ic.getVehicleNumber())));
		
		if (ic == null)
			throw new Exception("Unable to get case details!");
		LOGGER.info("/qc/cases/case-details request finished for case={}", caseId);

		return ic;
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
	}

	@RequestMapping(value = "/log-qc-open-comments", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void logComments(@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "logQcOpen", required = true) boolean logQcOpen) throws Exception {
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);
		if (logQcOpen)
			caseService.addComments(ic, "QC opened ");
	}

	@RequestMapping(value = "/cases/comments", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getComments(@RequestParam(value = "case_id", required = true) Long caseId) {

		return caseCommentDAO.get(caseId);
	}

<<<<<<< HEAD
	@RequestMapping(value = "/log-qc-open-comments", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public boolean logComments(@RequestParam(value = "case_id", required = true) Long caseId) throws Exception {
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);
		caseService.addComments(ic, "QC opened ");
		return true;
	}

	@RequestMapping(value = "/cases/comments", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void addComment(@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "comment", required = false) String comment,
			@RequestParam(value = "commentId", required = false) Long commentId) throws Exception {
=======
	@RequestMapping(value = "/cases/comments", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void addComment(@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "comment", required = true) String comment) throws Exception {
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
		InspectionCase id = (InspectionCase) caseDAO.getCaseById(caseId);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());

<<<<<<< HEAD
		caseCommentDAO.add(id, comment, commentId, user);
=======
		caseCommentDAO.add(id, comment, user);
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
		if (appConfigService.getBooleanProperty("IS_TO_UPDATE_COMMENT_IN_CACHE", true))
			redisService.updateCaseComment(id, comment, user);
	}

	@RequestMapping(value = "/cases/retag-photos", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void retagPhotos(@RequestParam(value = "case_id", required = true) Long caseId,
<<<<<<< HEAD
			@RequestParam(value = "payload", required = true) String payload) throws JSONException, Exception {
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);
=======
			@RequestParam(value = "payload", required = true) String payload) throws JSONException {

>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
		JSONArray payloadArr = new JSONArray(payload);

		for (int i = 0; i < payloadArr.length(); i++) {
			long photoId = payloadArr.getJSONObject(i).getInt("id");
			String photoType = payloadArr.getJSONObject(i).getString("photoType");

			CasePhoto photo = (CasePhoto) casePhotoDAO.get(photoId);
			photo.setPhotoType(photoTypeDAO.get(photoType));
			casePhotoDAO.save(photo);
		}
<<<<<<< HEAD
		caseService.addComments(ic, "Photos Retaged ");
=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
	}

	@RequestMapping(value = "/cases/bulk-upload", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void bulkUpload(@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "photos", required = true) List<MultipartFile> vehiclePhotos)
			throws JSONException, Exception {
		LOGGER.info("/qc/cases/bulk-upload request recieved for case={}", caseId);

		String rootFolder = "cases/" + caseId;
		InspectionCase c = (InspectionCase) caseDAO.getCaseById(caseId);

		List<CasePhoto> casePhotos = new ArrayList<>();

		for (MultipartFile vehiclePhoto : vehiclePhotos) {

			String newFileName = TimeHelper.getCurrentTimestampString() + "."
					+ FilenameUtils.getExtension(vehiclePhoto.getOriginalFilename());
			s3Service.uploadMultipartImage(vehiclePhoto, rootFolder + "/" + newFileName);
			CasePhoto cp = new CasePhoto();
			cp.setInspectionCase(c);
			cp.setAnswer("");
			cp.setQcAnswer("");
			cp.setLatitude(c.getInspectionLatitude());
			cp.setLongitude(c.getInspectionLongitude());
			cp.setSnapTime(new Date());
			cp.setFileName(newFileName);
			cp.setPhotoType(photoTypeDAO.get("others"));
			casePhotos.add(cp);
		}

		caseDAO.saveCasePhotos(c, casePhotos, "");
<<<<<<< HEAD
		// logging in comments
		caseService.addComments(c, "Bulk photo Uploaded ");
=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
		LOGGER.info("/qc/cases/bulk-upload request finished for case={}", caseId);

	}

	@RequestMapping(value = "/cases/address-requested-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List addressRequestedCases() {

		return caseDAO.getAllCasesByStage(1);
	}

	@RequestMapping(value = "/cases/get-case-photos", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getCasePhotos(@RequestParam(value = "case_id", required = true) Long caseId) {
		return casePhotoDAO.getCasePhotosDTOByCaseId(caseId);
	}

	@RequestMapping(value = "/cases/requested-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List requestedCases() {

		return caseDAO.getAllCasesByStage(2);
	}

	@RequestMapping(value = "/cases/qc-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List qcCases() {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		if (user instanceof Admin) {
			companyBranchDivisionUsers = ((Admin) user).getCompanyBranchDivisionUser();
		}

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user).getCompanyBranchDivisionUser();
		}

		if (user instanceof QC) {
			companyBranchDivisionUsers = ((QC) user).getCompanyBranchDivisionUser();
		}

		return caseDAO.getQCCasesDTO(companyBranchDivisionUsers);
	}

	@RequestMapping(value = "/cases/scheduled-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<ScheduledCasesDTO> scheduledCases() {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		if (user instanceof Admin) {
			companyBranchDivisionUsers = ((Admin) user).getCompanyBranchDivisionUser();
		}

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user).getCompanyBranchDivisionUser();
		}

		if (user instanceof QC) {
			companyBranchDivisionUsers = ((QC) user).getCompanyBranchDivisionUser();
		}

		if (appConfigService.getBooleanProperty("IS_TO_FETCH_SCHEDULED_CASES_FROM_CACHE", true)) {
			List<ScheduledCasesDTO> scheduledCases = redisService
					.getScheduledCases("SCHEDULED_CASES_FOR_USER_" + user.getUsername());
			if (scheduledCases == null || scheduledCases.isEmpty()) {
				scheduledCases = caseDAO.getScheduledCasesDTO(companyBranchDivisionUsers);
				redisService.cacheScheduledCases("SCHEDULED_CASES_FOR_USER_" + user.getUsername(), scheduledCases);
				return scheduledCases;
			}
			return scheduledCases;
		}
		return caseDAO.getScheduledCasesDTO(companyBranchDivisionUsers);
	}

	@RequestMapping(value = "/cases/offine-scheduled-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<ScheduledCasesDTO> offlineScheduledCases() {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		LOGGER.info("/qc/cases/scheduled-cases request recieved for user={}", user.getPhoneNumber());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		if (user instanceof Admin) {
			companyBranchDivisionUsers = ((Admin) user).getCompanyBranchDivisionUser();
		}

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user).getCompanyBranchDivisionUser();
		}

		if (user instanceof QC) {
			companyBranchDivisionUsers = ((QC) user).getCompanyBranchDivisionUser();
		}
		List<ScheduledCasesDTO> scheduledCases = null;
		if (appConfigService.getBooleanProperty("IS_TO_FETCH_SCHEDULED_CASES_FROM_CACHE", false)) {
			scheduledCases = redisService.getScheduledCases("SCHEDULED_CASES_FOR_USER_" + user.getUsername());
			if (scheduledCases == null || scheduledCases.isEmpty()) {
				scheduledCases = caseDAO.getOfflineScheduledCasesDTO(companyBranchDivisionUsers);
				redisService.cacheScheduledCases("SCHEDULED_CASES_FOR_USER_" + user.getUsername(), scheduledCases);
				LOGGER.info("/qc/cases/cheduled-cases request finished for user={}", user.getPhoneNumber());
				return scheduledCases;
			}
			return scheduledCases;
		}
		scheduledCases = caseDAO.getOfflineScheduledCasesDTO(companyBranchDivisionUsers);
		/*
		 * for (ScheduledCasesDTO scheduledCasesDTO : scheduledCases) {
		 * List<CaseCommentDTO> caseCommentDTO =
		 * caseCommentDAO.get(scheduledCasesDTO.getId());
		 * scheduledCasesDTO.setComments(caseCommentDTO); }
		 */
		LOGGER.info("/qc/cases/scheduled-cases request finished for user={}", user.getPhoneNumber());
		return scheduledCases;

	}

	@RequestMapping(value = "/cases/online-scheduled-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<ScheduledCasesDTO> onlineSheduledCases() {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		LOGGER.info("/qc/cases/scheduled-cases request recieved for user={}", user.getPhoneNumber());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		if (user instanceof Admin) {
			companyBranchDivisionUsers = ((Admin) user).getCompanyBranchDivisionUser();
		}

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user).getCompanyBranchDivisionUser();
		}

		if (user instanceof QC) {
			companyBranchDivisionUsers = ((QC) user).getCompanyBranchDivisionUser();
		}
		List<ScheduledCasesDTO> scheduledCases = null;
		if (appConfigService.getBooleanProperty("IS_TO_FETCH_ONLINE_SCHEDULED_CASES_FROM_CACHE", false)) {
			scheduledCases = redisService.getScheduledCases("ONLINE_SCHEDULED_CASES_FOR_USER_" + user.getUsername());
			if (scheduledCases == null || scheduledCases.isEmpty()) {
				scheduledCases = caseDAO.getOnlineScheduledCasesDTO(companyBranchDivisionUsers);
				redisService.cacheScheduledCases("ONLINE_SCHEDULED_CASES_FOR_USER_" + user.getUsername(),
						scheduledCases);
				LOGGER.info("/qc/cases/scheduled-cases request finished for user={}", user.getPhoneNumber());
				return scheduledCases;
			}
			return scheduledCases;
		}
		scheduledCases = caseDAO.getOnlineScheduledCasesDTO(companyBranchDivisionUsers);
		LOGGER.info("/qc/cases/online-scheduled-cases request finished for user={}", user.getPhoneNumber());
		return scheduledCases;

	}

	@RequestMapping(value = "/cases/completed-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<AllCases> completedCases() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		/*
		 * if (user instanceof Admin) { companyBranchDivisionUsers = ((Admin)
		 * user).getCompanyBranchDivisionUser(); }
		 */

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user).getCompanyBranchDivisionUser();
			return caseDAO.getTodaysCompletedCases(5, companyBranchDivisionUsers);

		}
		/*
		 * if (user instanceof QC) { companyBranchDivisionUsers = ((QC)
		 * user).getCompanyBranchDivisionUser();
		 * 
		 * }
		 */
		return new ArrayList<AllCases>();

	}

	@RequestMapping(value = "/cases/closed-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<AllCases> closedCases() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		if (user instanceof Admin) {
			companyBranchDivisionUsers = ((Admin) user).getCompanyBranchDivisionUser();
		}

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user).getCompanyBranchDivisionUser();
		}
		if (user instanceof QC) {
			companyBranchDivisionUsers = ((QC) user).getCompanyBranchDivisionUser();
		}
		if (appConfigService.getBooleanProperty("IS_TO_FETCH_FROM_CACHE", true)) {
			List<AllCases> closedCase = redisService.getAllCases("CLOSED_CASES_FOR_USER_" + user.getUsername());
			if (closedCase == null || closedCase.isEmpty()) {
				List<AllCases> closedCases = caseDAO.getAllCasesByStageDTO(-1, companyBranchDivisionUsers);
				redisService.cacheAllCases("CLOSED_CASES_FOR_USER_" + user.getUsername(), closedCases);
				return closedCases;
			}
			return closedCase;
		}
		return caseDAO.getAllCasesByStageDTO(-1, companyBranchDivisionUsers);
	}

	@RequestMapping(value = "/cases/all-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<AllCases> allCases() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		if (user instanceof Admin) {
			companyBranchDivisionUsers = ((Admin) user).getCompanyBranchDivisionUser();
		}

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user).getCompanyBranchDivisionUser();
		}

		if (user instanceof QC) {
			companyBranchDivisionUsers = ((QC) user).getCompanyBranchDivisionUser();
		}

		if (appConfigService.getBooleanProperty("IS_TO_FETCH_FROM_CACHE", true)) {
			List<AllCases> allCase = redisService.getAllCases("ALL_CASES_FOR_USER_" + user.getUsername());
			if (allCase == null || allCase.isEmpty()) {
				List<AllCases> allCases = caseDAO.getAllDTO(companyBranchDivisionUsers);
				redisService.cacheAllCases("ALL_CASES_FOR_USER_" + user.getUsername(), allCases);
				return allCases;
			}
			return allCase;
		}
		return caseDAO.getAllDTO(companyBranchDivisionUsers);

	}

	@RequestMapping(value = "/cases/qc-answers", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getQCAnswers(@RequestParam(value = "case_id", required = true) Long caseId) {

		return caseQCAnswerDAO.getQCAnswers(caseId);
	}

	@RequestMapping(value = "/cases/update-vehicle-data", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List updateVehicleData(@RequestParam(value = "case_id", required = true) Long caseId) {

		return caseQCAnswerDAO.getQCAnswers(caseId);
	}

	@RequestMapping(value = "/create-case", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<Object> createCase(
			@RequestParam(value = "purpose_of_inspection", required = true) String purposeOfInspection,
			@RequestParam(value = "customer_name", required = true) String customerName,
			@RequestParam(value = "customer_phone_number", required = true) String customerPhoneNumber,
			@RequestParam(value = "inspection_type", required = true) String inspectionType,
			@RequestParam(value = "vehicle_number", required = true) String vehicleNumber,
			@RequestParam(value = "agent_id", required = true) String agentId,
			@RequestParam(value = "inspection_time", required = false) @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss") Date date,
			@RequestParam(value = "latitude", required = false, defaultValue = "0.0") Double latitude,
			@RequestParam(value = "longitude", required = false, defaultValue = "0.0") Double longitude,
<<<<<<< HEAD
			@RequestParam(value = "inspection_number", required = false) String inspectionNumber)

=======
                        @RequestParam(value = "inspection_number", required = false) String inspectionNumber
        )
                
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
			throws Exception {
		LOGGER.info(
				"/create-case request recieved with data purpose_of_inspection={}, customer_name={}, customer_phone_number={}, inspection_type={}, vehicle_number={}, agent_id={}, inspection_time={}, latitude={}, longitude={}",
				purposeOfInspection, customerName, customerPhoneNumber, inspectionType, vehicleNumber, agentId, date,
				latitude, longitude);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(((User) authentication.getPrincipal()).getUsername());
		PurposeOfInspection poi = poiDAO.getPurposeOfInspectionById(purposeOfInspection);
		Agent refAgent = agentDAO.getAgentByUsername(agentId);
		if (date == null)
			date = new Date();
		if (refAgent == null) {
			LOGGER.info("No agent found with agentId={} for user={}", agentId, user.getUsername());
			throw new Exception("Referred agent account doesn't exists");
		}

		if (poi == null) {
			LOGGER.info("Invalid purpose of inspection!={} for user={}", purposeOfInspection, user.getUsername());
			throw new Exception("Invalid purpose of inspection!");
		}

		try {
			InspectionCaseBuilder icBuilder = new InspectionCaseBuilder(customerName, customerPhoneNumber,
					refAgent.getFirstName() + " " + refAgent.getLastName(), refAgent.getEmail(),
					refAgent.getPhoneNumber(), vehicleNumber, poi,
					refAgent.getAgentDetails().getCompanyBranchDivision(), CaseType.OFFLINE)
							.setInsuranceCompany(
									insuranceCompanyDAO.getInsuranceCompanyById(
											refAgent.getAgentDetails().getCompanyBranchDivision().getCompany()),
									refAgent.getAgentDetails().getCompanyBranchDivision())
							.setInspectionTime(date, latitude, longitude).addCaseToUserForStage(user, 1).setStage(1);

			switch (inspectionType) {
			case "SELF_INSPECT": {
				icBuilder.addCaseToUserForStage(refAgent, 3).setInspectionType("SELF_INSPECT").setStage(3);
				break;
			}
			case "ASSIGN_TO_CUSTOMER": {
				com.jadu.model.User customer = userDAO.getUserByPhoneOrEmail(customerPhoneNumber);

				if (customer == null)
					customer = createUserService.createCustomer("", customerPhoneNumber);

				icBuilder.addCaseToUserForStage(customer, 3).setInspectionTime(new Date(), latitude, longitude)
						.setInspectionType("ASSIGN_TO_CUSTOMER").setStage(3);
				break;
			}
			case "ASSIGN_TO_INSPECTOR": {
				break;
			}
			default:
				throw new Exception("Invalid inspection type!");
			}

			InspectionCase ic = icBuilder.build();

			ic.setDownloadKey(UtilHelper.getRandomString(64) + UtilHelper.getCurrentTimestampAsString() + "_");
			ic.setInspectionStage(0);// inspection not started
			ic.setEncryptionKey(UtilHelper.getRandomStringUpperCase(16));
			ic.setLastUpdatedTime(new Date());
			caseDAO.save(ic);
			caseLogDAO.addCreateCase(user, ic);
			LOGGER.info("case={} has been created, for user={}", ic, user.getUsername());
<<<<<<< HEAD
<<<<<<< HEAD
=======
			caseService.addComments(ic, "Case created for user " + user.getUsername());
>>>>>>> inspection-dashbord

			if (inspectionNumber != null && inspectionNumber != ""
					&& refAgent.getAgentDetails().getCompanyBranchDivision().getBranch().equals("Central Office")) {
				OnlineCases oc = new OnlineCases();
				oc.setInspectionCaseId(ic.getId());
				oc.setInspectionNumber(inspectionNumber);
				oc.setVehicleNumber(vehicleNumber);
				onlineCasesDAO.saveOrUpdate(oc);
			}
=======
                        
                        
                        if(inspectionNumber != null && inspectionNumber !="" && refAgent.getAgentDetails().getCompanyBranchDivision().getBranch().equals("Central Office")){
                            OnlineCases oc = new OnlineCases();
                            oc.setInspectionCaseId(ic.getId());
                            oc.setInspectionNumber(inspectionNumber);
                            oc.setVehicleNumber(vehicleNumber);
                            onlineCasesDAO.saveOrUpdate(oc);
                        }
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb

			if (null != inspectionType) {
				switch (inspectionType) {
				case "SELF_INSPECT":
					break;
				case "ASSIGN_TO_INSPECTOR":
					smsService.sendSMS(ic.getCustomerPhoneNumber(),
							"Hi, Please provide location & schedule for inspection of " + ic.getVehicleNumber()
									+ " for insurance policy." + " Visit "
<<<<<<< HEAD
									+ urlShortnerService.shortenCaseUrl(ic.getId()) + ". Please call "
									+ appConfigService.getProperty("SUPPORT_CONTACT_NUMBER") + " for any assistance.");
=======
									+ urlShortnerService.shortenCaseUrl(ic.getId())
									+ ". Please call " + appConfigService.getProperty("SUPPORT_CONTACT_NUMBER") + " for any assistance.");
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
					break;
				case "ASSIGN_TO_CUSTOMER": {
					String url = urlShortnerService.shortenUrl("customer-inspection/", ic.getId(),
							ic.getInsuranceCompany().getName());
					Map<String, String> dataMap = new HashMap<>();
					dataMap.put(UtilHelper.MSG_COMPANY_NAME_KEY, ic.getInsuranceCompany().getName());
					dataMap.put(UtilHelper.MSG_VEHICLE_NUMBER, ic.getVehicleNumber());
					dataMap.put(UtilHelper.MSG_URL, url);
					dataMap.put(UtilHelper.MSG_SUPPORT_CONTACT_NUMBER_KEY, UtilHelper.MSG_SUPPORT_CONTACT_NUMBER_VALUE);
<<<<<<< HEAD

					String msg = smsService.getMessageForSms(dataMap);
					smsService.sendSMSAsync(customerPhoneNumber, msg);
					connectCustomerToFlow.callCustomerAfterCaseCreate(customerPhoneNumber);
				}
=======
					
					String msg = smsService.getMessageForSms(dataMap);
					smsService.sendSMSAsync(customerPhoneNumber, msg);
					connectCustomerToFlow.callCustomerAfterCaseCreate(customerPhoneNumber);
					}
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
				}
			}

			if (appConfigService.getBooleanProperty("IS_TO_PUSH_CREATE_CASE_IN_CACHE", true)) {
				try {
					redisService.cacheCaseAgainstUser(user, ic, 0);
				} catch (Exception e) {
					e.printStackTrace();
					LOGGER.error("Exception raised while caching case={} for user={}, error={}", ic.getId(),
							user.getPhoneNumber(), e.getMessage());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception raised while creating case for user={}, error={}", user.getUsername(),
					e.getMessage());
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
		LOGGER.info(
				"/create-case request finished with data purpose_of_inspection={}, customer_name={}, customer_phone_number={}, inspection_type={}, vehicle_number={}, agent_id={}, inspection_time={}, latitude={}, longitude={}",
				purposeOfInspection, customerName, customerPhoneNumber, inspectionType, vehicleNumber, agentId, date,
				latitude, longitude);
		return new ResponseEntity<>(HttpStatus.OK);
	}

<<<<<<< HEAD
=======
	
	
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
	@RequestMapping(value = "/resend-sms", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void resendSms(@RequestParam(value = "case_id", required = true) Long caseId) throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		if (user != null) {
			InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);
			if (ic == null)
				throw new Exception("Case not found!");

			if (ic.getCurrentStage() != 3) {
				caseService.addComments(ic, "SMS Sent");
				return;
			}

			switch (ic.getInspectionType()) {
			case "ASSIGN_TO_CUSTOMER": {
				smsService.sendSMS(ic);
<<<<<<< HEAD
				caseService.addComments(ic, "SMS Sent");
=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
				break;
			}
			}

		} else {
			throw new Exception("Authentication failed, Invalid user!");
		}
	}

	@RequestMapping(value = "/cases/allot-user-manual", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void allotUserManual(@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "username", required = true) String username) throws Exception {
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Case not found!");

		com.jadu.model.User user = userDAO.getUserByUsername(username);

		ic.setCurrentStage(3);
		CaseUserAllocation allocate2 = new CaseUserAllocation();
		allocate2.setAllocationTime(UtilHelper.getDate());
		allocate2.setInspectionCase(ic);
		allocate2.setStage(3);
		allocate2.setUser(user);
		ic.addCaseUserAllocation(allocate2);
		ic.setLastUpdatedTime(new Date());
		caseDAO.save(ic);
	}

	@RequestMapping(value = "/update-inspection-time", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void updateInspectionTime(@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "new_inspection_time", required = true) @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss") Date date,
			@RequestParam(value = "reason", required = true) String reason) throws Exception {

		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Unable to find case with given details. Either case doesn't exists or is updated!");

		caseDAO.updateCaseInspectionTime(caseId, date);
<<<<<<< HEAD
		// add comment
		caseService.addComments(ic, "Case rescheduled - " + reason);
	}

	@RequestMapping(value = "/cases/submit-status-to-iffco", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String submitQCReport(@RequestParam(value = "case_id", required = true) Long caseId) throws Exception {
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);
		if (ic.getCurrentStage() == 5 && ic.getCaseType() == CaseType.ONLINE) {
			String status = callbackService.pushOnlineCaseStatus(ic, null);
			JSONObject result = new JSONObject();
			result.put("status", status);
			return result.toString();
		} else {
			throw new Exception("Invalid case");
		}
	}
=======
	}
        
        @RequestMapping(value = "/cases/submit-status-to-iffco", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String submitQCReport(@RequestParam(value = "case_id", required = true) Long caseId) throws Exception {
            InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);
            if (ic.getCurrentStage() == 5 && ic.getCaseType() == CaseType.ONLINE) {
                String status = callbackService.pushOnlineCaseStatus(ic, null);
                JSONObject result =  new JSONObject();
                result.put("status", status);
                return result.toString();
            }else{
                throw new Exception("Invalid case");
            }
        }
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb

	@RequestMapping(value = "/cases/submit-qc-report", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void submitQCReport(@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "recommendation", required = true) String recommendation,
			@RequestParam(value = "answers", required = true) String answers,
			@RequestParam(value = "remarks", required = true) String remarks) throws Exception {
		LOGGER.info("/case/submit-qc-report request recieved for case={},recommendation={},answers={},remarks={}",
				caseId, recommendation, answers, remarks);
<<<<<<< HEAD

		List<String> requiredPhotos = Arrays
				.asList(new String[] { "chachis-number", "number-plate", "client-signature" });

		if (!casePhotoDAO.ifPhotoTypeEXists(caseId, requiredPhotos))
			throw new Exception("Required photos does not exisits!");

=======
                
                
                List<String> requiredPhotos = Arrays.asList(new String[] { "chachis-number", "number-plate", "client-signature" });

                if(!casePhotoDAO.ifPhotoTypeEXists(caseId, requiredPhotos))
                    throw new Exception("Required photos does not exisits!");
                
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(((User) authentication.getPrincipal()).getUsername());

		JSONArray answersArray = new JSONArray(answers);
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);

		List<Agent> refAgent = agentDAO.getAgentByPhoneNumber(ic.getRequestorPhoneNumber());

		if (refAgent.isEmpty()) {
			LOGGER.info("Requestor agent details={} not found! for case={}", ic.getRequestorPhoneNumber(), caseId);
			throw new Exception("Requestor agent details not found!");
		}
<<<<<<< HEAD

		if (ic.getCompanyBranchDivision().getCompany().equals("Policybachat")
				&& (remarks.equals("recommended") || remarks.equals("not-recommended"))) {
			throw new Exception("Policybachat cases should be only reffered to underwriter!");
		}
=======
                
                if(ic.getCompanyBranchDivision().getCompany().equals("Policybachat") && (remarks.equals("recommended") || remarks.equals("not-recommended"))){
                    throw new Exception("Policybachat cases should be only reffered to underwriter!");
                }
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb

		if (ic.getCurrentStage() != 4) {
			LOGGER.info("Case={} is not at QC stage!", caseId);
			throw new Exception("Case is not at QC stage!");
		}

		CompanyBranchDivision cbd = refAgent.get(0).getAgentDetails().getCompanyBranchDivision();
		
		LOGGER.info("Company Name",cbd.getCompany());
		QCQuestionsFactory qcQuestionsFactory = new QCQuestionsFactory(caseQCQuestionDAO.getAll());
		CaseQCQuestionOptionFactory qcOptionsFactory = new CaseQCQuestionOptionFactory(
				caseQCQuestionOptionDAO.getAllObjects());

		for (int i = 0; i < answersArray.length(); i++) {
			JSONObject item = answersArray.getJSONObject(i);
			CaseQCQuestion questionId = qcQuestionsFactory.get(item.getInt("id"));
			CaseQCQuestionOption answer = null;
			if ("2-wheeler".equals(ic.getVehicleType().getId())
					&& appConfigService.getBooleanProperty("IS_TWO_SET_DEFAULT_NOT_FIITED_IN_CASE_OF_TWO_WHEELER", true)
					&& appConfigService.getProperty("QUESTION_IDS_TO_SET_NOT_FITTED", "58,65,68,70")
							.contains(String.valueOf(questionId.getId()))) {
				String vehicleSubTypes = ic.getVehicle().getSubType().getId();
				if ("scooter".equalsIgnoreCase(vehicleSubTypes)
						&& appConfigService.getProperty("QUESTION_IDS_FOR_SCOOTER_TO_SET_NOT_FITTED", "58,68,70")
								.contains(String.valueOf(questionId.getId()))) {
					answer = qcOptionsFactory.get(7);
				} else if ("bike".equalsIgnoreCase(vehicleSubTypes)
						&& appConfigService.getProperty("QUESTION_IDS_FOR_SCOOTER_TO_SET_NOT_FITTED", "65,68")
								.contains(String.valueOf(questionId.getId()))) {
					answer = qcOptionsFactory.get(7);
				} else {
					answer = qcOptionsFactory.get(item.getInt("answer"));
				}

			} else {
				answer = qcOptionsFactory.get(item.getInt("answer"));
			}
			CaseQCAnswer qcAnswer = new CaseQCAnswer();
			qcAnswer.setInspectionCase(ic);
			qcAnswer.setOption(answer);
			qcAnswer.setQuestion(questionId);
			caseQCAnswerDAO.save(qcAnswer);
		}

		ic.setRemark(recommendation);

		if (!recommendation.equals("hold")) {
			ic.setCurrentStage(5);

		} else {
			ic.setCurrentStage(4);
		}

		ic.setComment(remarks);

		ic.setQcEmail(user.getEmail());
		ic.setQcName(user.getFirstName() + " " + user.getLastName());
		ic.setQcPhone(user.getPhoneNumber());
		ic.setQcTime(new Date());
		ic.setLastUpdatedTime(new Date());
		caseDAO.save(ic);
		try {
			if (!recommendation.equals("hold")) {

				String outputFile1 = "report.pdf";
				generateReportService.createPdf((InspectionCase) caseDAO.getCaseById(ic.getId()), ic.getId() + ".pdf",
						outputFile1, true);

				if (appConfigService.getBooleanProperty("IS_TO_PUSH_SUBMIT_QC_COMPLETED_CASE_IN_CACHE", false)) {
					try {
						// redisService.updateCompletedCaseInCache(user, ic);
						redisService.removeFromScheduledCases(user, ic);
					} catch (Exception e) {
						e.printStackTrace();
						LOGGER.info(
								"Exception raised while pushing submitted case in cache for user={}, case={}, error={}",
								user.getPhoneNumber(), caseId, e.getMessage());
					}
				}
				String mailIds[] = cbd.getEmail().split(",");
				for (String mail : mailIds) {
					mailService.sendGenaratedReportMail(mail, ic,
							s3Service.readFile("cases/" + ic.getId() + "/" + outputFile1));
				}
				mailService.sendGenaratedReportMail(ic.getRequestorEmail(), ic,
						s3Service.readFile("cases/" + ic.getId() + "/" + outputFile1));

				smsService.sendSMSAsync(ic.getRequestorPhoneNumber(),
						"Your case request for " + ic.getVehicleNumber() + " is " + recommendation.toUpperCase()
								+ ". Download the report from your mobile app or check email.");
<<<<<<< HEAD

=======
				
				
				
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
			} else {
				if (ic.getInspectionType().equals("ASSIGN_TO_CUSTOMER")) {
					smsService.sendSMSAsync(ic.getCustomerPhoneNumber(),
							"Your Case Id - " + ic.getId() + " for Vehicle Number - " + ic.getVehicleNumber()
									+ " is on HOLD. Photos Required : " + ic.getComment() + ". Call "
									+ appConfigService.getProperty("SUPPORT_CONTACT_NUMBER") + " for support.");
<<<<<<< HEAD

=======
					
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
					com.jadu.model.User customer = userDAO.getUserByPhoneOrEmail(ic.getCustomerPhoneNumber());
					caseService.sendCaseHoldNotificationsAsync(customer, ic);
					connectCustomerToFlow.callCustomerAfterCaseHold(ic.getCustomerPhoneNumber());
				} else {
					smsService.sendSMSAsync(ic.getRequestorPhoneNumber(),
							"Your Case Id - " + ic.getId() + " for Vehicle Number - " + ic.getVehicleNumber()
									+ " is on HOLD. Photos Required : " + ic.getComment() + ". Call "
									+ appConfigService.getProperty("SUPPORT_CONTACT_NUMBER") + " for support.");
					com.jadu.model.User agent = userDAO.getUserByPhoneOrEmail(ic.getRequestorPhoneNumber());
<<<<<<< HEAD
					caseService.sendCaseHoldNotificationsAsync(agent, ic);
=======
					caseService.sendCaseHoldNotificationsAsync(agent , ic);
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
					connectCustomerToFlow.callAgentAfterCaseHold(ic.getRequestorPhoneNumber());
				}
			}
//			if (!recommendation.equals("hold")) {
//                            if (ic.getCaseType() == CaseType.ONLINE && ic.getRemark() != null) {
//                                    String status = callbackService.pushOnlineCaseStatus(ic, null);
//                                    LOGGER.info("Callback has been executed for case={}, with response={}", ic.getId(), status);
//                            }
//			}
<<<<<<< HEAD
<<<<<<< HEAD

=======
>>>>>>> inspection-dashbord
			OnlineCases oc = onlineCasesDAO.findByCaseId(ic.getId());

			for (ApiSwitch apiSwitch : ApiSwitch.values()) {
				if (apiSwitch.qualify(ic.getCompanyBranchDivision(), ic, oc)) {
					apiSwitch.getApi().call(ic, oc, onlineCasesDAO, appConfigService, googleLocationService,
							casePhotoDAO.getCasePhotosByCaseId(ic.getId()), s3Service);
				}
			}
=======
                        
                        OnlineCases oc = onlineCasesDAO.findByCaseId(ic.getId());
                        
                        for (ApiSwitch apiSwitch : ApiSwitch.values()) {
                            if(apiSwitch.qualify(ic.getCompanyBranchDivision(), ic, oc)){
                                apiSwitch.getApi().call(ic, oc, onlineCasesDAO, appConfigService, googleLocationService, casePhotoDAO.getCasePhotosByCaseId(ic.getId()), s3Service);
                            }
                        }
                        
                        
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info("Exception raised while submitting case={} for user={}, error={}", caseId,
					user.getPhoneNumber(), e.getMessage());
			throw new Exception("Something went wrong while generating and sending report, error: " + e.getMessage());
		}
		LOGGER.info("/case/submit-qc-report request finished for case={},recommendation={},answers={},remarks={}",
				caseId, recommendation, answers, remarks);
<<<<<<< HEAD
		// logging in comments
		caseService.addComments(ic, "QC submitted with Recommendation " + recommendation + " and remarks " + remarks);
<<<<<<< HEAD
=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
=======
>>>>>>> inspection-dashbord
	}

	@RequestMapping(value = "/cases/update-qc-report", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void updateQCReport(@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "recommendation", required = true) String recommendation,
			@RequestParam(value = "remarks", required = true) String remarks) throws Exception {
		LOGGER.info("/cases/submit-qc-report request recieved with parameters case_id={},recommendation={},remarks={}",
				caseId, recommendation, remarks);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(((User) authentication.getPrincipal()).getUsername());

		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);

		List<Agent> refAgent = agentDAO.getAgentByPhoneNumber(ic.getRequestorPhoneNumber());
		try {
			if (refAgent == null)
				throw new Exception("Requestor agent details not found!");

			CompanyBranchDivision cbd = refAgent.get(0).getAgentDetails().getCompanyBranchDivision();

			ic.setRemark(recommendation);
			ic.setComment(remarks);

			caseDAO.save(ic);

<<<<<<< HEAD
<<<<<<< HEAD
			//logging comments
			caseService.addComments(ic, "Remark: " + recommendation + " , comment: " + remarks);
			
=======
			caseCommentDAO.add(ic, "Remark: " + recommendation + " , comment: " + remarks, user);
			if (appConfigService.getBooleanProperty("IS_TO_UPDATE_COMMENT_IN_CACHE", true))
				redisService.updateCaseComment(ic, "Remark: " + recommendation + " , comment: " + remarks, user);

>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
=======
			// logging comments
			caseService.addComments(ic, "Remark: " + recommendation + " , comment: " + remarks);

>>>>>>> inspection-dashbord
			if (!recommendation.equals("hold")) {

				String outputFile1 = "report.pdf";

				generateReportService.createPdf((InspectionCase) caseDAO.getCaseById(ic.getId()), ic.getId() + ".pdf",
						outputFile1, true);
				String mailIds[] = cbd.getEmail().split(",");
				for (String mail : mailIds) {
					mailService.sendGenaratedReportMail(mail, ic,
							s3Service.readFile("cases/" + ic.getId() + "/" + outputFile1));
				}
				mailService.sendGenaratedReportMail(ic.getRequestorEmail(), ic,
						s3Service.readFile("cases/" + ic.getId() + "/" + outputFile1));

				smsService.sendSMSAsync(ic.getRequestorPhoneNumber(),
						"Your case request for " + ic.getVehicleNumber() + " is " + recommendation.toUpperCase()
								+ ". Download the report from your mobile app or check email.");
//            List<String> list = cbd.getEmails();
//            //list.add(ic.getRequestorEmail());
//
//            mailService.sendGenaratedReportMail(
//                    list, 
//                    ic, 
//                    s3Service.readFile("cases/" + ic.getId() + "/" + outputFile1));
//            
//            smsService.sendSMSAsync(ic.getRequestorPhoneNumber(), "Your case request for " + ic.getVehicleNumber() + " is " + recommendation.toUpperCase() + ". Download the report from your mobile app or check email.");
			} else {
				if (ic.getInspectionType().equals("ASSIGN_TO_CUSTOMER")) {
					smsService.sendSMSAsync(ic.getCustomerPhoneNumber(),
							"Your Case Id - " + ic.getId() + " for Vehicle Number - " + ic.getVehicleNumber()
									+ " is on HOLD. Call " + appConfigService.getProperty("SUPPORT_CONTACT_NUMBER")
									+ " for support");
				} else {
					smsService.sendSMSAsync(ic.getRequestorPhoneNumber(),
							"Your Case Id - " + ic.getId() + " for Vehicle Number - " + ic.getVehicleNumber()
									+ " is on HOLD. Call " + appConfigService.getProperty("SUPPORT_CONTACT_NUMBER")
									+ " for support");
				}
			}
<<<<<<< HEAD

			OnlineCases oc = onlineCasesDAO.findByCaseId(ic.getId());

			for (ApiSwitch apiSwitch : ApiSwitch.values()) {
				if (apiSwitch.qualify(ic.getCompanyBranchDivision(), ic, oc)) {
					apiSwitch.getApi().call(ic, oc, onlineCasesDAO, appConfigService, googleLocationService,
							casePhotoDAO.getCasePhotosByCaseId(ic.getId()), s3Service);
				}
			}
=======
                        
                        OnlineCases oc = onlineCasesDAO.findByCaseId(ic.getId());
                        
                        for (ApiSwitch apiSwitch : ApiSwitch.values()) {
                            if(apiSwitch.qualify(ic.getCompanyBranchDivision(), ic, oc)){
                                apiSwitch.getApi().call(ic, oc, onlineCasesDAO, appConfigService, googleLocationService, casePhotoDAO.getCasePhotosByCaseId(ic.getId()), s3Service);
                            }
                        }
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception raised while submitting case={} for user={}, error={}", caseId,
					user.getPhoneNumber(), e.getMessage());
		}
	}

	@RequestMapping(value = "/close-case", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void submitReport(@RequestParam(value = "case_id", required = true) int caseId,
			@RequestParam(value = "reason", required = true) String reason) throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(((User) authentication.getPrincipal()).getUsername());
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Case not found");

		if (ic.getCurrentStage() == -1)
			throw new Exception("Case has already been closed!");

		ic.setCurrentStage(-1);
		ic.setInspectionStage(2);// inspection finished
		ic.setCloseTime(UtilHelper.getDate());
		ic.setCloseReason(reason);
		ic.setLastUpdatedTime(new Date());
		caseDAO.save(ic);
		caseService.addComments(ic, "Case closed - " + reason);
		if (CaseType.ONLINE.equals(ic.getCaseType())) {
			String status = callbackService.pushOnlineCaseStatus(ic, null);
			LOGGER.info("Callback has been executed for case={}, with response={}", ic.getId(), status);
		}
		if (appConfigService.getBooleanProperty("IS_TO_PUSH_IN_CACHE", true)) {
			redisService.updateClosedCases(user, ic);
		}
<<<<<<< HEAD
<<<<<<< HEAD
		caseService.addComments(ic, "Case closed - " + reason);
=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
=======
>>>>>>> inspection-dashbord
	}

	@RequestMapping(value = "/case/reopen-qc", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void reopenForQC(@RequestParam(value = "case_id", required = true) int caseId,
			@RequestParam(value = "reason", required = true) String reason) throws Exception {
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Case not found");
		ic.setCurrentStage(4);
		ic.setInspectionStage(1);// inspection started
		ic.setQcReopenTime(new Date());
		ic.setQcReopenReason(reason);
		ic.setLastUpdatedTime(new Date());
		caseDAO.save(ic);
		caseQCAnswerDAO.deleteAnswersForCase(ic.getId());
<<<<<<< HEAD
=======
		caseService.addComments(ic, "Case  Re-opened for QC - " + reason);
>>>>>>> inspection-dashbord
	}

	@RequestMapping(value = "/case/reopen", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void reopen(@RequestParam(value = "case_id", required = true) int caseId,
			@RequestParam(value = "reason", required = true) String reason) throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(((User) authentication.getPrincipal()).getUsername());
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Case not found");

		ic.setCurrentStage(3);
		ic.setReopenReason(reason);
		ic.setInspectionStage(0);// inspection not started
		ic.setReopenTime(new Date());
		ic.setLastUpdatedTime(new Date());
		caseDAO.save(ic);

		caseQCAnswerDAO.deleteAnswersForCase(ic.getId());
<<<<<<< HEAD
<<<<<<< HEAD
		caseService.addComments(ic, "Case Re-opened for Inspection " + reason);
=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb

=======
		caseService.addComments(ic, "Case Re-opened From QC for Inspection " + reason);
>>>>>>> inspection-dashbord
		String ts = String.valueOf(System.currentTimeMillis());

		if (s3Service.exists("cases/" + ic.getId() + "/" + ic.getId() + ".zip")) {
			s3Service.copyFile("cases/" + ic.getId() + "/" + ic.getId() + ".zip",
					"cases/" + ic.getId() + "/" + ts + ".zip");
		}

		if (s3Service.exists("cases/" + ic.getId() + "/" + ic.getId() + ".json")) {
			s3Service.copyFile("cases/" + ic.getId() + "/" + ic.getId() + ".json",
					"cases/" + ic.getId() + "/" + ts + ".json");
		}
		if (appConfigService.getBooleanProperty("IS_TO_PUSH_REOPEN_CASE_IN_CACHE", true)) {
			try {
				List<CaseComment> commentCounts = caseCommentDAO.count(caseId);
				redisService.cacheCaseAgainstUser(user, ic, commentCounts != null ? commentCounts.size() : 0);
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error("Exception raised while caching case={} for user={}, error={}", ic.getId(),
						user.getPhoneNumber(), e.getMessage());
			}
		}
	}

	@RequestMapping(value = "/replace-photo", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void replacePhoto(@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "case_file", required = true) MultipartFile caseFile,
			@RequestParam(value = "comment", required = true) String comment,
			@RequestParam(value = "photo_type", required = true) String photoType) throws Exception {

		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Unable to find case with given details. Either case doesn't exists or is updated!");

		/*
		 * Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		 * 
		 * String currTime = String.valueOf(timestamp.getTime());
		 * 
		 * String newFileName = currTime + "_" + caseFile.getOriginalFilename();
		 */
		String newFileName = TimeHelper.getCurrentTimestampString() + "."
				+ FilenameUtils.getExtension(caseFile.getOriginalFilename());

		s3Service.uploadMultipartFile(caseFile, "cases/" + ic.getId() + "/" + newFileName);

		List<Long> casePhotos = casePhotoDAO.getCasePhotosByPhotoType(caseId, photoType);

		if (casePhotos.isEmpty())
			throw new Exception("No photo to replace!");

		CasePhoto cp = (CasePhoto) casePhotoDAO.get(casePhotos.get(0));

		cp.setFileName(newFileName);

		casePhotoDAO.save(cp);
<<<<<<< HEAD
		caseService.addComments(ic, "Photos replaced - " + comment);
=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb

	}

	@RequestMapping(value = "/add-photo", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void addPhoto(@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "case_file", required = true) MultipartFile caseFile,
			@RequestParam(value = "comment", required = true) String comment,
			@RequestParam(value = "photo_type", required = true) String photoType) throws Exception {

		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Unable to find case with given details. Either case doesn't exists or is updated!");

		// Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		String newFileName = TimeHelper.getCurrentTimestampString() + "."
				+ FilenameUtils.getExtension(caseFile.getOriginalFilename());
		/*
		 * String currTime = String.valueOf(timestamp.getTime()); String newFileName =
		 * currTime + "_" + caseFile.getOriginalFilename();
		 */
		s3Service.uploadMultipartFile(caseFile, "cases/" + ic.getId() + "/" + newFileName);

		List<Long> casePhotos = casePhotoDAO.getCasePhotosByPhotoType(caseId, photoType);

		if (casePhotos.isEmpty())
			throw new Exception("No photo to replace!");

		CasePhoto cp = new CasePhoto();
		cp.setAnswer("");
		cp.setFileName(newFileName);
		cp.setInspectionCase(ic);
		cp.setLatitude(0.0);
		cp.setLongitude(0.0);
		cp.setPhotoType(photoTypeDAO.get(photoType));
		cp.setSnapTime(new Date());
		cp.setQcAnswer("");

		casePhotoDAO.save(cp);

	}

	@RequestMapping(value = "/cases/download-uploaded-zip/{case_id}", method = RequestMethod.GET, produces = "application/zip")
	@ResponseBody
	public void downloadZipPhotos(@PathVariable long case_id, HttpServletResponse response) throws Exception {

		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(case_id);

		if (ic == null)
			throw new Exception("No case with given ID exists for user!");

		InputStream zipfile = s3Service.readFileStream("cases/" + ic.getId() + "/" + ic.getId() + ".zip");

		if (zipfile == null)
			throw new Exception("Unable to zip all files! Please try again later.");

		try {
			response.setHeader("Content-Disposition", "attachment; filename=\"" + ic.getId() + ".zip" + "\"");
			IOUtils.copy(zipfile, response.getOutputStream());
			response.flushBuffer();
		} catch (IOException ex) {
			throw new RuntimeException("IOError writing file to output stream");
		}
	}

	@RequestMapping(value = "/cases/delete-case-photo", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void deletePhoto(@RequestParam(value = "photo_id", required = true) Long photoId) throws Exception {

		casePhotoDAO.deletePhoto(photoId);
	}

	@RequestMapping(value = "/cases/update-customer-name", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void updateCustomerName(@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "customer_name", required = true) String customerName) throws Exception {
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);

		if (ic.getCurrentStage() == 5)
			throw new Exception("Case is already completed/closed");
		ic.setLastUpdatedTime(new Date());
		ic.setCustomerName(customerName);
		caseDAO.save(ic);
	}

	@RequestMapping(value = "/send-report", method = RequestMethod.GET)
	public void sendReport(@RequestParam("case_id") long caseId) throws Exception {
		LOGGER.info("/case/send-report request recieved for case={}", caseId);
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);
		List<Agent> refAgent = agentDAO.getAgentByPhoneNumber(ic.getRequestorPhoneNumber());
		CompanyBranchDivision cbd = refAgent.get(0).getAgentDetails().getCompanyBranchDivision();
		String outputFile1 = "report.pdf";
		String mailIds[] = cbd.getEmail().split(",");
		for (String mail : mailIds) {
			mailService.sendGenaratedReportMail(mail, ic,
					s3Service.readFile("cases/" + ic.getId() + "/" + outputFile1));
		}
		mailService.sendGenaratedReportMail(ic.getRequestorEmail(), ic,
				s3Service.readFile("cases/" + ic.getId() + "/" + outputFile1));

		smsService.sendSMSAsync(ic.getRequestorPhoneNumber(), "Your case request for " + ic.getVehicleNumber() + " is "
				+ ic.getRemark().toUpperCase() + ". Download the report from your mobile app or check email.");
		caseService.addComments(ic, "Mail Reported ");
		LOGGER.info("/case/send-report request finished for case={}", caseId);

	}

	@ExceptionHandler(Exception.class)
	public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
		ErrorHandler.handleError(ex, response);
		ex.printStackTrace();
	}

	@RequestMapping(value = "/case/search", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public List<AllCases> searchCase(@RequestParam(value = "case_id", required = false) String caseId,
			@RequestParam(value = "vehicle_id", required = false) String vehicle_id,
			@RequestParam(value = "customer_phone", required = false) String customer_phone,
			@RequestParam(value = "requester_phone", required = false) String requester_phone) throws Exception {
		LOGGER.info(
				"/case/search request recieved, parameter case_id={},vehicle_id={},customer_phone={},requester_phone={}",
				caseId, vehicle_id, customer_phone, requester_phone);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = utilService.getUser(authentication);
		// Set<CompanyBranchDivisionUser> companyBranchDivisionUsers =
		// utilService.getCompanyBranchDivisionUser(user);
		List<InspectionCase> casesList = new ArrayList<InspectionCase>();
		if (caseId != null) {
			casesList.add((InspectionCase) caseDAO.getCaseById(Integer.valueOf(caseId)));
		} else if (vehicle_id != null) {
			casesList.addAll(caseDAO.getCaseByVehicleNumber(vehicle_id));
		} else if (customer_phone != null) {
			casesList.addAll(caseDAO.getCaseByCustomerPhoneNumber(vehicle_id));
		} else if (requester_phone != null) {
			casesList.addAll(caseDAO.getCaseByAgentPhoneNumber(vehicle_id));
		} else {
			LOGGER.info("No valid input found");
			throw new Exception("No valid input found");

		}
		if (casesList == null || casesList.isEmpty())
			throw new Exception("Case not found");
		List<AllCases> allCasesList = new ArrayList<AllCases>();
		for (InspectionCase ic : casesList) {
			List commentList = caseCommentDAO.count(ic.getId());
			int countComments = commentList != null && !commentList.isEmpty() ? commentList.size() : 0;
			AllCases allCases = new AllCases(ic.getId(), ic.getVehicleNumber(), ic.getCustomerName(),
					ic.getCustomerPhoneNumber(), ic.getInspectionTime(), ic.getInspectionSubmitTime(),
					ic.getCurrentStage(), ic.getInspectionType(),
					ic.getInsuranceCompany() != null ? ic.getInsuranceCompany().getName() : null,
					ic.getCompanyBranchDivision().getBranch(),
					ic.getInsuranceCompany() != null ? ic.getInsuranceCompany().getLogoUrl() : null,
					ic.getRequestorPhoneNumber(), ic.getRemark(), ic.getDownloadKey(), countComments, ic.getComment());
			allCasesList.add(allCases);
		}
		LOGGER.info(
				"/case/search request finished, parameter case_id={},vehicle_id={},customer_phone={},requester_phone={}",
				caseId, vehicle_id, customer_phone, requester_phone);
		return allCasesList;
	}

	@RequestMapping(value = "/agent/comments", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public AgentCommentDTO getAgentComments(@RequestParam(value = "phonenumber", required = true) String phonenumber,
			@RequestParam(value = "from", required = false) Long from,
			@RequestParam(value = "to", required = false) Long to) {
		AgentCommentDTO agentCommentDTO = new AgentCommentDTO();
		agentCommentDTO.setCasesCount(caseDAO.getCaseCount(phonenumber));
		if (from == null || to == null) {
			agentCommentDTO.setCaseCommentDTO(agentCommentDAO.get(phonenumber));
		} else {
			agentCommentDTO.setCaseCommentDTO(agentCommentDAO.get(phonenumber, new Date(from), new Date(to)));
		}
		return agentCommentDTO;
	}

	@RequestMapping(value = "/add/agent/comments", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void addComment(@RequestParam(value = "phonenumber", required = true) String phonenumber,
			@RequestParam(value = "comment", required = true) String comment) throws Exception {
		LOGGER.info("/agent/comments request recieved for agent={}, comment={}", phonenumber, comment);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		agentCommentDAO.add(phonenumber, comment, user);
		LOGGER.info("/agent/comments request finished for agent={}, comment={}", phonenumber, comment);

	}

	@RequestMapping(value = "/case/reminder", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity remindCaseInspection(@RequestParam(value = "from", required = false) long from,
			@RequestParam(value = "to", required = false) long to,
			@RequestParam(value = "self_inspection", required = false) boolean self_inspection,
			@RequestParam(value = "case_type", required = false) String case_type,
			@RequestParam(value = "case_stage", required = true, defaultValue = "3") int caseStage) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		if (user != null) {
			LOGGER.info("qc/case/reminder job started at={} by user={}", new Date(), user.getUsername());
			CaseType caseType = null;
			if ("offline".equalsIgnoreCase(case_type))
				caseType = CaseType.OFFLINE;
			else if ("online".equalsIgnoreCase(case_type))
				caseType = CaseType.ONLINE;
			// appConfigService.updateTag("IS_TO_RUN_SYSTEM_AUTOMATED_CALL", "TRUE");
			appConfigService.updateTag("IS_TO_RUN_SYSTEM_AUTOMATED_CALL_FOR_" + caseType, "TRUE");
			reconcileService.remindCaseInspection(from, to, self_inspection, user, caseType, caseStage);
			LOGGER.info("qc/case/reminder job finished at ", new Date());
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

	}

	@RequestMapping(value = "/case/reminder/hold", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity remindHoldCases(@RequestParam(value = "from", required = false) long from,
			@RequestParam(value = "to", required = false) long to,
			@RequestParam(value = "self_inspection", required = false) boolean self_inspection,
			@RequestParam(value = "case_stage", required = true, defaultValue = "4") int caseStage) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		if (user != null) {
			LOGGER.info("case/reminder/hold job started at={} by user={}", new Date(), user.getUsername());
			appConfigService.updateTag("IS_TO_RUN_SYSTEM_AUTOMATED_CALL_FOR_HOLD_CASES", "TRUE");
			reconcileService.remindCaseInspection(from, to, self_inspection, user, null, caseStage);
			LOGGER.info("/case/reminder/hold job finished at ", new Date());
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

	}

	@RequestMapping(value = "/stop/call/hold/cases", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity stopCallToHoldCases(@RequestParam(value = "case_type", required = false) String case_type,
			@RequestParam(value = "case_stage", required = true, defaultValue = "4") int caseStage) {
		LOGGER.info("/stop/call/hold/cases job being stoped at ={}", new Date());
		appConfigService.updateTag("IS_TO_RUN_SYSTEM_AUTOMATED_CALL_FOR_HOLD_CASES", "FALSE");
		LOGGER.info("/stop/call/hold/cases job has been stopped at ", new Date());
		return new ResponseEntity<>(appConfigService.getBooleanProperty("IS_TO_RUN_SYSTEM_AUTOMATED_CALL"),
				HttpStatus.OK);

	}

	@RequestMapping(value = "/stop/call-reminder", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity stopCallReminder(@RequestParam(value = "case_type", required = false) String case_type) {
		LOGGER.info("reconcile/case/reminder job being stoped at ={}", new Date());
		// appConfigService.updateTag("IS_TO_RUN_SYSTEM_AUTOMATED_CALL", "FALSE");
		CaseType caseType = null;
		if ("offline".equalsIgnoreCase(case_type))
			caseType = CaseType.OFFLINE;
		else
			caseType = CaseType.ONLINE;
		LOGGER.info("reconcile/case/reminder job has been stopped at ", new Date());
		appConfigService.updateTag("IS_TO_RUN_SYSTEM_AUTOMATED_CALL_FOR_" + caseType, "FALSE");
		return new ResponseEntity<>(appConfigService.getBooleanProperty("IS_TO_RUN_SYSTEM_AUTOMATED_CALL"),
				HttpStatus.OK);

	}
	
	@RequestMapping(value = "/cases/re-upload-photo", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void reUploadPhotos(@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "parts", required = true) String parts) throws Exception {
		reUploadPhotoDao.add(caseId, parts);
	}
	
	@RequestMapping(value = "/cases/pending-photo-for-qc", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getPendingPhotoByCaseId(@RequestParam(value = "case_id", required = true) Long caseId)  throws Exception {
		ReUploadPhoto pendingPhoto = reUploadPhotoDao.get(caseId);
		
		if (pendingPhoto == null)
			throw new Exception("No Photos are on hold for Qc!");
		LOGGER.info("/qc/cases/pending-photo-for-qc for case={}", caseId);
		return pendingPhoto;
	}


}
