package com.jadu.front.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author gauta
 */
@RestController
@Scope("session")
@PropertySource("/WEB-INF/config.properties")
public class RootFrontController {

	@Autowired
	Environment env;

	@RequestMapping(value = "/")
	public ModelAndView getIndex(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView model = new ModelAndView("WEB-INF/jsp/index");
		return model;
	}

	@RequestMapping(value = "/login")
	public ModelAndView getLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView model = new ModelAndView("WEB-INF/jsp/login");
		return model;
	}

	@RequestMapping(value = "/reset-password")
	public ModelAndView resetPassword(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView model = new ModelAndView("WEB-INF/jsp/reset");
		return model;
	}

	@RequestMapping(value = "/demo-video")
	public ModelAndView showDemoVideo(HttpServletRequest request, HttpServletResponse response) throws Exception {

		return new ModelAndView("redirect:https://youtu.be/1VlbgIXoVME");
	}
}
