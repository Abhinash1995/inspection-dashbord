/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.front.controller;

import com.ibm.watson.developer_cloud.visual_recognition.v3.model.ErrorInfo;
import com.jadu.model.ErrorResponse;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;
 
@ControllerAdvice
public class ExceptionControllerAdvice {
 
	/*@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage("Please contact your administrator");
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
	}
        
        @ExceptionHandler(NoHandlerFoundException.class)
        public ResponseEntity<ErrorResponse> notFoundHanlder(NoHandlerFoundException ex) {
            ErrorResponse error = new ErrorResponse();
            error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            error.setMessage("Please contact your administrator");
            return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
        }

        @ExceptionHandler(AccessDeniedException.class)
        public ResponseEntity<ErrorResponse> handleAuthorizationException(AccessDeniedException ex) {
            ErrorResponse error = new ErrorResponse();
            error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            error.setMessage("Please contact your administrator");
            return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
        }*/
        
    //@RequestMapping(produces = "application/json")
    //@ExceptionHandler(Throwable.class)
    public ResponseEntity<?> handleException(Throwable ex) {
        ErrorResponse error = new ErrorResponse();
        //error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setMessage("Please contact your administrator");
        return new ResponseEntity<>(error, HttpStatus.OK);
    }
}