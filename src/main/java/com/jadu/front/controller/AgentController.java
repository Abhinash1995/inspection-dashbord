package com.jadu.front.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.jadu.dao.AgentDAOImpl;
import com.jadu.dao.CaseDAO;
import com.jadu.dao.CaseLogDAO;
import com.jadu.dao.CasePhotoDAO;
import com.jadu.dao.ConfigurationDAO;
import com.jadu.dao.InspectorDAO;
import com.jadu.dao.InsuranceCompanyDAO;
import com.jadu.dao.PhotoTypeDAO;
import com.jadu.dao.PurposeOfInspectionDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.dao.VehicleDAO;
import com.jadu.dao.VehicleFuelTypeDAO;
import com.jadu.dao.VehicleTypeDAO;
import com.jadu.dto.CaseHistoryDTO;
import com.jadu.dto.CaseHistoryDTO2;
import com.jadu.dto.RestWrapperDTO;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.Agent;
import com.jadu.model.CaseType;
import com.jadu.model.CompanyBranchDivision;
import com.jadu.model.InspectionCase;
import com.jadu.model.PurposeOfInspection;
import com.jadu.model.builder.InspectionCaseBuilder;
import com.jadu.push.notification.AndroidPushNotificationsService;
import com.jadu.service.AppConfigService;
import com.jadu.service.CaseService;
import com.jadu.service.ConnectCustomerToFlow;
import com.jadu.service.CreateUserService;
import com.jadu.service.RedisService;
import com.jadu.service.S3DirectUploadService;
import com.jadu.service.S3Service;
import com.jadu.service.SmsService;
import com.jadu.service.URLShortnerService;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/agent")
@Scope("session")
public class AgentController {

	@Autowired
	AgentDAOImpl agentDAO;

	@Autowired
	PurposeOfInspectionDAO poiDAO;

	@Autowired
	VehicleDAO vehicleDAO;

	@Autowired
	CaseDAO caseDAO;

	@Autowired
	InsuranceCompanyDAO insuranceCompanyDAO;

	@Autowired
	PhotoTypeDAO photoTypeDAO;

	@Autowired
	CasePhotoDAO casePhotoDAO;

	@Autowired
	VehicleFuelTypeDAO vehicleFuelTypeDAO;

	@Autowired
	UserDAOImpl userDAO;

	@Autowired
	ConfigurationDAO configurationDAO;

	@Autowired
	InspectorDAO inspectorDAO;

	@Autowired
	AndroidPushNotificationsService androidPushNotificationsService;

	@Autowired
	URLShortnerService urlShortnerService;

	@Autowired
	S3Service s3Service;

	@Autowired
	SmsService smsService;

	@Autowired
	CaseLogDAO caseLogDAO;

	@Autowired
	VehicleTypeDAO vehicleTypeDAO;

	@Autowired
	S3DirectUploadService s3DirectUploadService;

	@Autowired
	CaseService caseService;

	@Autowired
	CreateUserService createUserService;

	@Autowired
	ConnectCustomerToFlow connectCustomerToFlow;

	@Autowired
	private RedisService redisService;

	@Autowired
	private AppConfigService appConfigService;

	private static final Logger LOGGER = LoggerFactory.getLogger(AgentController.class);

	@RequestMapping(value = "/profile-details", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object checkAgentDetails() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return userDAO.getUserByUsername(((User) authentication.getPrincipal()).getUsername());
	}

	@RequestMapping(value = "/get-configurations", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getConfigurations() throws JSONException {
		JSONObject result = new JSONObject();
		result.put("AGENT_SELF_INSPECT", configurationDAO.getConfigurationBooleanValueByTag("AGENT_SELF_INSPECT"));
		return result.toString();
	}

	@RequestMapping(value = "/cases/scheduled-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List scheduledCases() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		return caseDAO.getAllCasesSubsetByStage(3, user.getUsername());
	}

	@RequestMapping(value = "/cases/scheduled-cases/v2", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getScheduledCases() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		return new RestWrapperDTO(caseDAO.getScheduledCasesByUsername(user.getUsername()));
	}
        
	@RequestMapping(value = "/cases/all-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List allCases(@RequestParam(value = "limit", required = false, defaultValue = "-1") Integer limit) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());
		if (appConfigService.getBooleanProperty("IS_TO_FETCH_AGENT_CASE_FROM_CACHE", true)) {
			List<CaseHistoryDTO> allCase = redisService.getAllAgentCases("ALL_CASES_FOR_USER_" + user.getUsername());
			if (allCase == null || allCase.isEmpty()) {
				List<CaseHistoryDTO> allCases = caseDAO.getAllCasesByUsername(user.getUsername(), limit);
				redisService.cacheAllAgentCases("ALL_CASES_FOR_AGENT_" + user.getUsername(), allCases);
				return allCases;
			}
			return allCase;
		}
		return caseDAO.getAllCasesByUsername(user.getUsername(), limit);
	}
        
        @RequestMapping(value = "/cases/case-history", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List caseHistory(@RequestParam(value = "limit", required = false, defaultValue = "-1") Integer limit) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());
		if (appConfigService.getBooleanProperty("IS_TO_FETCH_AGENT_CASE_FROM_CACHE", true)) {
			List<CaseHistoryDTO> allCase = redisService.getAllAgentCases("ALL_CASES_FOR_USER_" + user.getUsername());
			if (allCase == null || allCase.isEmpty()) {
				List<CaseHistoryDTO> allCases = caseDAO.getAllCasesByUsername(user.getUsername(), limit);
				redisService.cacheAllAgentCases("ALL_CASES_FOR_AGENT_" + user.getUsername(), allCases);
				return allCases;
			}
			return allCase;
		}
		return caseDAO.getCaseHistory(user.getUsername(), limit);
	}

	@RequestMapping(value = "/cases/all-cases/v2", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getAllCases() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());
		if (appConfigService.getBooleanProperty("IS_TO_FETCH_WRAPPED_AGENT_CASE_FROM_CACHE", true)) {
			List<CaseHistoryDTO2> allCase = redisService
					.getAgentAllCases("WRAPPED_ALL_CASES_FOR_AGENT_" + user.getUsername());
			if (allCase == null || allCase.isEmpty()) {
				allCase = caseDAO.getAllCasesByUsername2(user.getUsername());
				RestWrapperDTO allCases = new RestWrapperDTO(allCase);
				redisService.cacheAgentAllCases("WRAPPED_ALL_CASES_FOR_AGENT_" + user.getUsername(), allCase);
				return allCases;
			}
			return new RestWrapperDTO(allCase);

		}
		return new RestWrapperDTO(caseDAO.getAllCasesByUsername2(user.getUsername()));
	}

	@RequestMapping(value = "/cases/get-case-details", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getCaseDetails(@RequestParam(value = "case_id", required = true) Long caseId) throws Exception {
		LOGGER.info("/agent/cases/get-case-details request recieved at={} for case={}", new Date(), caseId);
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);
		if (ic == null)
			throw new Exception("Unable to get case details!");
		LOGGER.info("/agent/cases/get-case-details request finished at={} for case={}", new Date(), caseId);
		return ic;
	}

	@RequestMapping(value = "/cases/download-zip-photos", method = RequestMethod.GET, produces = "application/zip")
	@ResponseBody
	public void downloadZipPhotos(@RequestParam(value = "case_id", required = true) Long caseId,
			HttpServletResponse response) throws Exception {
		LOGGER.info("/agent/cases/download-zip-photos request recieved at={} for case={}", new Date(), caseId);

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		InspectionCase ic = (InspectionCase) caseDAO.getInspectionCase(caseId, user.getUsername());

		if (ic == null)
			throw new Exception("No case with given ID exists for user!");

		List<String> casePhotos = casePhotoDAO.getCasePhotosByCaseId(ic.getId());

		List<byte[]> files = new ArrayList<>();

		for (String casePhoto : casePhotos) {
			files.add(s3Service.readFile("cases/" + caseId + "/" + casePhoto));
		}

		files.add(s3Service.readFile("cases/" + caseId + "/" + "report.pdf"));
		casePhotos.add("report.pdf");

		File zipfile = UtilHelper.zipBytes(files, casePhotos, "export.zip");

		if (zipfile == null)
			throw new Exception("Unable to zip all files! Please try again later.");

		try {
			InputStream is = new FileInputStream(zipfile);
			IOUtils.copy(is, response.getOutputStream());
			response.flushBuffer();
		} catch (IOException ex) {
			throw new RuntimeException("IOError writing file to output stream");
		}
		LOGGER.info("/agent/cases/download-zip-photos request finished at={} for case={}", new Date(), caseId);

	}

	@RequestMapping(value = "/cases/download-report", method = RequestMethod.GET, produces = "application/pdf")
	@ResponseBody
	public HttpEntity<byte[]> downloadReport(@RequestParam(value = "case_id", required = true) Long caseId,
			HttpServletResponse response) throws Exception {
		LOGGER.info("/agent/cases/download-report request recieved at={} for case={}", new Date(), caseId);

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		InspectionCase ic = (InspectionCase) caseDAO.getInspectionCase(caseId, user.getUsername());

		if (ic == null)
			throw new Exception("No case with given ID exists for user!");

		if (ic.getCurrentStage() < 5)
			throw new Exception("Inspection report has not been generated yet!");

		byte[] document = s3Service.readFile("cases/" + caseId + "/" + "report.pdf");

		HttpHeaders header = new HttpHeaders();
		header.setContentType(new MediaType("application", "pdf"));
		header.set("Content-Disposition", "inline; filename=Report.pdf");
		header.setContentLength(document.length);
		LOGGER.info("/agent/cases/download-report request finished at={} for case={}", new Date(), caseId);
		return new HttpEntity<>(document, header);
	}

	@RequestMapping(value = "/cases/my-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getCasesList() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		return caseDAO.getMyCases(user.getUsername(), user.getEmail());
	}

	@RequestMapping(value = "/create-case", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<InspectionCase> createCase(
			@RequestParam(value = "purpose_of_inspection", required = true) String purposeOfInspection,
			@RequestParam(value = "customer_name", required = true) String customerName,
			@RequestParam(value = "customer_phone_number", required = true) String customerPhoneNumber,
			@RequestParam(value = "self_inspect", required = true) boolean selfInspect,
			@RequestParam(value = "inspection_time", required = false) @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss") Date date,
			@RequestParam(value = "latitude", required = false) Double latitude,
			@RequestParam(value = "longitude", required = false) Double longitude,
			@RequestParam(value = "vehicle_number", required = true) String vehicleNumber)
			throws JSONException, Exception {
		LOGGER.info(
				"/create-case request recieved with data purpose_of_inspection={}, customer_name={}, customer_phone_number={}, self_inspect={}, vehicle_number={}, inspection_time={}, latitude={}, longitude={}",
				purposeOfInspection, customerName, customerPhoneNumber, selfInspect, vehicleNumber, date, latitude,
				longitude);

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Agent agent = agentDAO.getAgentByUsername(((User) authentication.getPrincipal()).getUsername());
		CompanyBranchDivision companyBranchDivision = agent.getAgentDetails().getCompanyBranchDivision();
		InspectionCase ic = null;
		if (date == null)
			date = new Date();

		if (companyBranchDivision == null) {
			LOGGER.info("Invalid company for agent={}. Please contact administrator!", agent.getPhoneNumber());
			throw new Exception("Invalid company. Please contact administrator!");
		}

		PurposeOfInspection poi = poiDAO.getPurposeOfInspectionById(purposeOfInspection);

		if (poi == null) {
			LOGGER.info("Invalid purpose of inspection={} for user={}", poi, agent.getPhoneNumber());
			throw new Exception("Invalid purpose of inspection!");
		}
		try {
			InspectionCaseBuilder icBuilder = new InspectionCaseBuilder(customerName, customerPhoneNumber,
					agent.getFirstName() + " " + agent.getLastName(), agent.getEmail(), agent.getPhoneNumber(),
					vehicleNumber, poi, agent.getAgentDetails().getCompanyBranchDivision(), CaseType.OFFLINE)
							.setInsuranceCompany(
									insuranceCompanyDAO.getInsuranceCompanyById(companyBranchDivision.getCompany()),
									companyBranchDivision)
							.setStage(1).addCaseToUserForStage(agent, 1);

			if (selfInspect) {
				icBuilder.addCaseToUserForStage(agent, 3).setInspectionType("SELF_INSPECT")
						.setInspectionTime(new Date(), latitude, longitude).setStage(3);
			} else {
				com.jadu.model.User user = userDAO.getUserByPhoneOrEmail(customerPhoneNumber);

				if (user == null)
					user = createUserService.createCustomer(customerName, customerPhoneNumber);

				icBuilder.addCaseToUserForStage(user, 3).setInspectionType("ASSIGN_TO_CUSTOMER")
						.setInspectionTime(new Date(), latitude, longitude).setStage(3);
			}

			ic = icBuilder.build();

			ic.setEncryptionKey(UtilHelper.getRandomStringUpperCase(16));

			ic.setDownloadKey(UtilHelper.getRandomString(64) + UtilHelper.getCurrentTimestampAsString() + "_");
			ic.setInspectionStage(0);
			ic.setLastUpdatedTime(new Date());
			caseDAO.save(ic);

			caseLogDAO.addCreateCase(agent, ic);

			caseService.sendCaseCreationNotificationsAsync(agent, ic);

			if (!selfInspect) {
				String url = urlShortnerService.shortenUrl("customer-inspection/", ic.getId(),
						ic.getInsuranceCompany().getName());
				
				Map<String, String> dataMap = new HashMap<>();
				dataMap.put(UtilHelper.MSG_COMPANY_NAME_KEY, ic.getInsuranceCompany().getName());
				dataMap.put(UtilHelper.MSG_VEHICLE_NUMBER, ic.getVehicleNumber());
				dataMap.put(UtilHelper.MSG_URL, url);
				dataMap.put(UtilHelper.MSG_SUPPORT_CONTACT_NUMBER_KEY, UtilHelper.MSG_SUPPORT_CONTACT_NUMBER_VALUE);
				
				String msg = smsService.getMessageForSms(dataMap);
				smsService.sendSMSAsync(customerPhoneNumber,msg);
				connectCustomerToFlow.callCustomerAfterCaseCreate(customerPhoneNumber);
			}
			if (appConfigService.getBooleanProperty("IS_TO_PUSH_CREATE_CASE_BY_AGENT_IN_CACHE", true)) {
				try {
					if (agent != null && ic != null)
						redisService.cacheCaseAgainstAgent(agent, ic, 0);
				} catch (Exception e) {
					e.printStackTrace();
					LOGGER.error("Exception catched while caching case={}, error={}", ic.getId(), e.getMessage());
				}
			}
			LOGGER.info("case has been created, returning case details={} to the client", ic);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(
					"Exception raised while creating case for user={},purpose_of_inspection={}, customer_name={}, customer_phone_number={}, self_inspect={}, vehicle_number={}, inspection_time={}, latitude={}, longitude={}, error={} ",
					agent.getPhoneNumber(), purposeOfInspection, customerName, customerPhoneNumber, selfInspect,
					vehicleNumber, date, latitude, longitude, e.getMessage());
		}
		LOGGER.info(
				"/create-case request finished with data purpose_of_inspection={}, customer_name={}, customer_phone_number={}, self_inspect={}, vehicle_number={}, inspection_time={}, latitude={}, longitude={}",
				purposeOfInspection, customerName, customerPhoneNumber, selfInspect, vehicleNumber, date, latitude,
				longitude);
		return new ResponseEntity<>(ic, HttpStatus.OK);

	}

	@RequestMapping(value = "/update-inspection-time", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void updateInspectionTime(@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "new_inspection_time", required = true) @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss") Date date,
			@RequestParam(value = "reason", required = true) String reason) throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		InspectionCase ic = (InspectionCase) caseDAO.getcaseByStage(3, user.getUsername(), caseId);

		if (ic == null)
			throw new Exception("Unable to find case with given details. Either case doesn't exists or is updated!");

		Date fromTime = ic.getInspectionTime();

		caseDAO.updateCaseInspectionTime(caseId, date);

		caseLogDAO.addUpdateInspectionTime(user, ic, fromTime, date);

	}

	@RequestMapping(value = "/cases/start-inspection", produces = "application/json")
	@ResponseBody
	public void startInspection(@RequestParam(value = "case_id", required = true) Long caseId) throws Exception {
		LOGGER.info("/agent/cases/start-inspection request recieved at={} for case={}", new Date(), caseId);

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		InspectionCase ic = (InspectionCase) caseDAO.getcaseByStage(3, user.getUsername(), caseId);

		if (ic == null)
			throw new Exception("Unable to find case with given details. Either case doesn't exists or is updated!");

		ic.setInspectionStartTime(UtilHelper.getDate());
		ic.setInspectionStage(1);
		ic.setLastUpdatedTime(new Date());
		caseDAO.save(ic);
		LOGGER.info("/agent/cases/start-inspection request finished at={} for case={}", new Date(), caseId);

	}

	@RequestMapping(value = "/cases/submit-report", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void submitReport(@RequestParam(value = "vehicle_photos", required = true) List<MultipartFile> vehiclePhotos,
			@RequestParam(value = "details", required = true) String details,
			@RequestParam(value = "vehicle_number", required = true) String vehicleNumber,
			@RequestParam(value = "vehicle_type", required = true) String vehicleType,
			@RequestParam(value = "vehicle_color", required = true) String vehicleColor,
			@RequestParam(value = "make_model_id", required = true) int makeModelId,
			@RequestParam(value = "fuel_type", required = true) String fuelType,
			@RequestParam(value = "yom", required = true) int YOM,
			@RequestParam(value = "case_id", required = true) int caseId,
			@RequestParam(value = "vehicle_video", required = true) MultipartFile vehicleVideo)
			throws JSONException, Exception {
		LOGGER.info(
				"/cases/submit-report request recieved with parameters vehicle_photos={},details={},vehicle_number={},vehicle_type={},vehicle_color={},makeModelId={},fuelType={},yom={},case_id={},vehicle_video={}",
				vehiclePhotos, details, vehicleNumber, vehicleType, vehicleColor, makeModelId, fuelType, YOM, caseId,
				vehicleVideo);

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();

		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());
		try {
			// Vehicle vehicle = vehicleDAO.getVehicleById(makeModelId);

			JSONArray detailsObj = new JSONArray(details);

			InspectionCase c = (InspectionCase) caseDAO.getInspectionCase(caseId, user.getUsername());

			if (c == null)
				throw new Exception("Unable to find case with given id. ");

			if (c.getCurrentStage() > 3)
				throw new Exception("Photos already submitted. Can't be submitted again!");

			String rootFolder = "cases/" + caseId;

			s3Service.uploadMultipartFile(vehicleVideo, rootFolder + "/video.mp4");

			for (MultipartFile vehiclePhoto : vehiclePhotos) {
				s3Service.uploadMultipartImage(vehiclePhoto, rootFolder + "/" + vehiclePhoto.getOriginalFilename());
			}

			caseService.submitReport(caseId, makeModelId, fuelType, YOM, vehicleColor, vehicleNumber, vehicleType,
					detailsObj, user);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception raised while submitting case={},vehicle_number={} for user={}, error={}", caseId,
					vehicleNumber, user.getPhoneNumber(), e.getMessage());
		}
		LOGGER.info(
				"/cases/submit-report request finished with parameters vehicle_photos={},details={},vehicle_number={},vehicle_type={},vehicle_color={},makeModelId={},fuelType={},yom={},case_id={},vehicle_video={}",
				vehiclePhotos, details, vehicleNumber, vehicleType, vehicleColor, makeModelId, fuelType, YOM, caseId,
				vehicleVideo);
	}

	@RequestMapping(value = "/cases/notify-inspection-completion", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void notifyInspectionCompletion(@RequestParam(value = "case_id", required = true) int caseId)
			throws Exception {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		InspectionCase c = (InspectionCase) caseDAO.getInspectionCase(caseId, user.getUsername());

		if (c == null)
			return;

		if (c.getCurrentStage() != 3)
			return;

		c.setFileAvailable(true);
		c.setFileAvailableTime(new Date());
		c.setInspectionStage(2);
		c.setLastUpdatedTime(new Date());
		caseDAO.save(c);

	}

	@RequestMapping(value = "/cases/notify-zip-upload", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void notifyZipUpload(@RequestParam(value = "case_id", required = true) int caseId) throws Exception {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		InspectionCase c = (InspectionCase) caseDAO.getInspectionCase(caseId, user.getUsername());

		if (c == null)
			return;

		if (c.getCurrentStage() != 3)
			return;

		if (s3Service.exists("cases/" + c.getId() + "/" + c.getId() + ".zip")) {
			c.setCurrentStage(4);
			c.setLastUpdatedTime(new Date());
			caseDAO.save(c);
			caseService.uploadZipToS3Async(caseId, "cases/" + c.getId() + "/" + c.getId() + ".zip", false);
		} else {
			throw new Exception("Inspection file not uploaded to server!");
		}
	}

	@RequestMapping(value = "/cases/additional-zip", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String pushAdditionalZip(@RequestParam(value = "case_id", required = true) int caseId,
			@RequestParam(value = "case_file", required = true) MultipartFile caseFile) throws Exception {
		LOGGER.info("Date={}, /cases/additional-zip request recieved for case={}, caseFile={}", new Date(), caseId,
				caseFile);
		JSONObject result = new JSONObject();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		InspectionCase c = (InspectionCase) caseDAO.getInspectionCase(caseId, user.getUsername());
                if (c == null) {
                        LOGGER.info("Date={}, Unable to find case with given id={} for user={} ", new Date(), caseId,
                                        user.getPhoneNumber());
                        throw new Exception("Unable to find case with given id. ");
                }

                String extension = FilenameUtils.getExtension(caseFile.getOriginalFilename());

                if (!extension.equals("zip")) {
                        LOGGER.info("Invalid zip file={} for user={} ", caseFile, user.getPhoneNumber());
                        throw new Exception("Invalid zip file!");
                }

                s3Service.uploadMultipartFileDirect(caseFile, "cases/" + c.getId() + "/" + caseFile.getOriginalFilename());

                if (s3Service.exists("cases/" + c.getId() + "/" + caseFile.getOriginalFilename())) {
                        result.put("success", true);
                        c.setCurrentStage(4);
                        c.setLastUpdatedTime(new Date());
                        caseDAO.save(c);

                        caseService.uploadZipToS3Async(caseId, "cases/" + c.getId() + "/" + caseFile.getOriginalFilename(),
                                        true);
                } else {
                        throw new Exception("Inspection file not uploaded to server!");
                }
		LOGGER.info("Date={}, /cases/additional-zip request finished for case={}, caseDile={}", new Date(), caseId,
				caseFile);

		return result.toString();

	}

	@RequestMapping(value = "/cases/submit-report-zip", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String submitReportZip(@RequestParam(value = "case_id", required = true) int caseId,
			@RequestParam(value = "case_file", required = true) MultipartFile caseFile) throws Exception {
		LOGGER.info("/cases/submit-report-zip request recieved for case={}, caseDile={}", caseId,
				caseFile.getOriginalFilename());
		JSONObject result = new JSONObject();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		InspectionCase c = (InspectionCase) caseDAO.getInspectionCase(caseId, user.getUsername());
		try {
			if (c == null) {
				LOGGER.info("Unable to find case with given id. for case={},caseFile={} ", caseId,
						caseFile.getOriginalFilename());
				throw new Exception("Unable to find case with given id. ");
			}

			if (c.getCurrentStage() > 3) {
				LOGGER.info("Case has already been submitted to QC! for case={},caseFile={} ", caseId,
						caseFile.getOriginalFilename());
				throw new Exception("Case has already been submitted to QC!");
			}

			if (c.getCurrentStage() < 3) {
				LOGGER.info("Case has not been scheduled yet! for case={},caseFile={} ", caseId,
						caseFile.getOriginalFilename());
				throw new Exception("Case has not been scheduled yet!");
			}

			String extension = FilenameUtils.getExtension(caseFile.getOriginalFilename());

			if (!extension.equals("zip")) {
				LOGGER.info("Invalid zip file! for case={},caseFile={} ", caseId, caseFile.getOriginalFilename());
				throw new Exception("Invalid zip file!");
			}

			s3Service.uploadMultipartFileDirect(caseFile, "cases/" + c.getId() + "/" + c.getId() + ".zip");

			if (s3Service.exists("cases/" + c.getId() + "/" + c.getId() + ".zip")) {
				result.put("success", true);
				c.setCurrentStage(4);
				c.setLastUpdatedTime(new Date());
				caseDAO.save(c);
				caseService.uploadZipToS3Async(caseId, "cases/" + c.getId() + "/" + c.getId() + ".zip", false);
			} else {
				LOGGER.info("Inspection zip file not uploaded to server! for case={},caseFile={} ", caseId,
						caseFile.getOriginalFilename());
				throw new Exception("Inspection file not uploaded to server!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception raised while sumbitting zip file for case={} ,caseFile={} , user={}, error={}",
					caseId, caseFile.getOriginalFilename(), user.getPhoneNumber(), e.getMessage());
		}
		LOGGER.info("/cases/submit-report-zip request finished for case={}, caseDile={}", caseId, caseFile);

		return result.toString();

	}

	@RequestMapping(value = "/cases/cancel-case", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void submitReport(@RequestParam(value = "case_id", required = true) int caseId,
			@RequestParam(value = "reason", required = false) String reason) throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		InspectionCase ic = (InspectionCase) caseDAO.getCase(caseId, user.getUsername());

		if (ic == null)
			throw new Exception("Case not found");

		if (ic.getCurrentStage() == -1)
			throw new Exception("Case has already been cancelled!");

		ic.setCurrentStage(-1);
		ic.setCloseTime(UtilHelper.getDate());
		ic.setCloseReason(reason);
		ic.setLastUpdatedTime(new Date());
		ic.setInspectionStage(3);
		caseDAO.save(ic);
		caseLogDAO.addCloseCase(user, ic);
	}

	@RequestMapping(value = "/cases/get-upload-url", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String getSignedUploadUrl(@RequestParam(value = "case_id", required = true) int caseId,
			HttpServletResponse response) throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		InspectionCase ic = (InspectionCase) caseDAO.getCase(caseId, user.getUsername());

		if (ic == null)
			throw new Exception("Case not found");

		if (ic.getCurrentStage() != 3)
			throw new Exception("Case document has either been uploaded or case has been closed");

		JSONObject result = new JSONObject();

		result.put("url", s3Service.getSignedUrl("cases/" + ic.getId() + "/" + ic.getId() + ".zip"));

		return result.toString();
	}

	public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
		ErrorHandler.handleError(ex, response);
		System.out.println(ex.getStackTrace());
	}
}
