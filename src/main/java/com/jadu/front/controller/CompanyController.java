package com.jadu.front.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.jadu.dao.AgentDAOImpl;
import com.jadu.dao.CaseDAO;
import com.jadu.dao.CompanyBranchDivisionDAO;
import com.jadu.dao.InsuranceCompanyDAO;
import com.jadu.dao.OnlineCasesDAO;
import com.jadu.dao.PurposeOfInspectionDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.dao.VehicleTypeDAO;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.Agent;
import com.jadu.model.CaseType;
import com.jadu.model.CompanyBranchDivision;
import com.jadu.model.InspectionCase;
import com.jadu.model.OnlineCases;
import com.jadu.model.PurposeOfInspection;
import com.jadu.model.builder.InspectionCaseBuilder;
import com.jadu.service.AppConfigService;
import com.jadu.service.CreateUserService;
import com.jadu.service.SmsService;
import com.jadu.service.URLShortnerService;
import java.net.URI;
import java.net.URL;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/company")
public class CompanyController {

	@Autowired
	private UserDAOImpl userDAO;

	@Autowired
	private PurposeOfInspectionDAO poiDAO;

	@Autowired
	private AgentDAOImpl agentDAO;

	@Autowired
	private InsuranceCompanyDAO insuranceCompanyDAO;

	@Autowired
	private CreateUserService createUserService;

	@Autowired
	private SmsService smsService;

	@Autowired
	private CaseDAO caseDAO;

	@Autowired
	private URLShortnerService urlShortnerService;

	@Autowired
	private CompanyBranchDivisionDAO companyBranchDivisionDAO;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private OnlineCasesDAO onlineCasesDAO;

	@Autowired
	private VehicleTypeDAO vehicleTypeDAO;

	private static final Logger LOGGER = LoggerFactory.getLogger(CompanyController.class);

	@RequestMapping(value = "/case", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> createCase(
			@RequestParam(value = "purpose_of_inspection", required = false) String purposeOfInspection,
			@RequestParam(value = "customer_name", required = true) String customerName,
			@RequestParam(value = "customer_phone_number", required = true) String customerPhoneNumber,
			@RequestParam(value = "vehicle_number", required = true) String vehicleNumber,
			@RequestParam(value = "agent_phone_number", required = false) String agentPhoneNumber,
			@RequestParam(value = "quote_number", required = false, defaultValue="") String quote_number,
			@RequestParam(value = "vehicle_type", required = false, defaultValue="") String vehicle_type, 
                        @RequestParam(value = "chassis_number", required = false, defaultValue="") String chassisNumber,
                        @RequestParam(value = "engine_number", required = false, defaultValue="") String engineNumber,
                        @RequestParam(value = "inspection_number", required = false, defaultValue="") String inspectionNumber,
                        HttpServletRequest request)
			throws Exception {
		LOGGER.info(
				"Date={} /company/case request recieved with parameters customer_name={}, customer_phone_number={}, vehicle_number={}, agent_phone_number={},quote_number={},vehicle_type={}",
				new Date(), customerName, customerPhoneNumber, vehicleNumber, agentPhoneNumber, quote_number,
				vehicle_type);
		PurposeOfInspection poi;
		JSONObject result = new JSONObject();

		if (purposeOfInspection != null)
			poi = poiDAO.getPurposeOfInspectionById(purposeOfInspection);
		else {
			poi = poiDAO.getPurposeOfInspectionById(
					appConfigService.getProperty("PURPOSE_OF_INSPECTION_FOR_IFFCO_TWO_WHEELER", "break_in"));
		}
                
		if (poi == null)
                    return new ResponseEntity<>("Invalid purpose of inspection!", HttpStatus.UNAUTHORIZED);

		Integer branchId = (Integer) request.getAttribute("branch_id");
		if (branchId == null) {
                    LOGGER.info("branch={} invaid", branchId);
                    result.put("status", "failed");
                    result.put("errorMessage", "Invalid branch");
                    return new ResponseEntity<>(result.toString(), HttpStatus.UNAUTHORIZED);

		}
		CompanyBranchDivision cbd = companyBranchDivisionDAO.getCompanyBranchDivisionById(branchId);
		Agent refAgent = null;
		if (agentPhoneNumber != null)
                    refAgent = agentDAO.getUniqueAgentByPhoneNumber(agentPhoneNumber);

		if (refAgent == null)
                    refAgent = agentDAO.getUniqueAgentByPhoneNumber(cbd.getDefaultAgent());

		if (refAgent == null) {
                    LOGGER.info("Default agent is not setup yet for branchId={}. Please contact administrator!", branchId);
                    result.put("status", "failed");
                    result.put("errorMessage", "Default agent is not setup yet. Please contact administrator!");
                    return new ResponseEntity<>(result.toString(), HttpStatus.UNAUTHORIZED);

		}

		InspectionCaseBuilder icBuilder = new InspectionCaseBuilder(customerName, customerPhoneNumber,
				refAgent.getFirstName() + " " + refAgent.getLastName(), refAgent.getEmail(), refAgent.getPhoneNumber(),
				vehicleNumber, poi, cbd, CaseType.ONLINE)
						.setInsuranceCompany(insuranceCompanyDAO.getInsuranceCompanyById(cbd.getCompany()), cbd)
						.setInspectionTime(new Date(), 0.0, 0.0).setStage(1);

		com.jadu.model.User customer = userDAO.getUserByPhoneOrEmail(customerPhoneNumber);

		if (customer == null)
			customer = createUserService.createCustomer("", customerPhoneNumber);

		icBuilder.addCaseToUserForStage(refAgent, 1);

		icBuilder.addCaseToUserForStage(customer, 3).setInspectionType("ASSIGN_TO_CUSTOMER").setStage(3);

		InspectionCase ic = icBuilder.build();

		ic.setEncryptionKey(UtilHelper.getRandomStringUpperCase(16));

		ic.setDownloadKey(UtilHelper.getRandomString(64) + UtilHelper.getCurrentTimestampAsString() + "_");
		if (vehicle_type != null && !vehicle_type.isEmpty())
			ic.setVehicleType(vehicleTypeDAO.get(format(vehicle_type)));
		ic.setInspectionStage(0);
                ic.setChassisNumber(chassisNumber);
                ic.setEngineNumber(engineNumber);
		caseDAO.save(ic);

		OnlineCases onlineCases = new OnlineCases();
		onlineCases.setInspectionCaseId(ic.getId());
		onlineCases.setQuoteNumber(quote_number);
                onlineCases.setChassisNumber(chassisNumber);
                onlineCases.setEngineNumber(engineNumber);
                onlineCases.setInspectionNumber(inspectionNumber);
		onlineCasesDAO.saveOrUpdate(onlineCases);
		String url = urlShortnerService.shortenUrl("customer-inspection/", ic.getId(),
				ic.getInsuranceCompany().getName() + " ONLINE");
		if (appConfigService.getBooleanProperty("IS_TO_SEND_SMS_TO_CUSTOMER_IN_ONLINE_CASES", true)){
                    if(cbd.getCompany().equals("IFFCO Tokio General Insurance Co. Ltd.")){
                        smsService.sendSMSAsync(customerPhoneNumber, "Dear Customer, Self Inspection request received for vehicle "
					+ ic.getVehicleNumber() + " from IFFCO TOKIO. Download application from " + url
					+ " to complete self inspection in next 24 hours. The policy will start only after successful inspection. Call 7290049100 for any support.");
                    }else if(cbd.getCompany().equals("Policybachat")){
                        smsService.sendSMSAsync(customerPhoneNumber, String.format("Dear Customer, Self Inspection request received for vehicle %s from Policy Bachat. Download mobile application from %s to complete inspection. Call %s for any support. IMP: Confirm Chassis No. location before inspection", ic.getVehicleNumber(), urlShortnerService.shortenUrl("customer-inspection/", ic.getId(),
						ic.getInsuranceCompany().getName()), UtilHelper.MSG_SUPPORT_CONTACT_NUMBER_VALUE));
<<<<<<< HEAD
=======
                    }else if(cbd.getCompany().equals("Coverfox Insurance Broking Private Limited")){
                        smsService.sendSMSAsync(customerPhoneNumber, String.format("Dear Customer, Self Inspection request received for vehicle %s from Coverfox. Download mobile application from %s to complete vehicle inspection. Call %s for any support. NOTE: Confirm Chassis Number location before inspection", ic.getVehicleNumber(), urlShortnerService.shortenUrl("customer-inspection/", ic.getId(),
						ic.getInsuranceCompany().getName()), UtilHelper.MSG_SUPPORT_CONTACT_NUMBER_VALUE));
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
                    }else{
                        smsService.sendSMSAsync(customerPhoneNumber, String.format("Dear Customer, Self Inspection request received for vehicle %s from insurance company. Download mobile application from %s to complete inspection. Call %s for any support. IMP: Confirm Chassis No. location before inspection", ic.getVehicleNumber(), urlShortnerService.shortenUrl("customer-inspection/", ic.getId(),
						ic.getInsuranceCompany().getName()), UtilHelper.MSG_SUPPORT_CONTACT_NUMBER_VALUE));
                    }
                    
                }
			
		if (appConfigService.getBooleanProperty("IS_TO_RETURN_CASE_DETAILS_IN_RESPONSE_TO_COMPANY", false)) {
			result.put("case_id", ic.getId());
			result.put("status", "success");
			return new ResponseEntity<>(result.toString(), HttpStatus.OK);
		} else {
			result.put("case_id", ic.getId());
			result.put("status", "success");
			return new ResponseEntity<>(result.toString(), HttpStatus.OK);

		}

	}

	private String format(String vehicle_type) {
		if ("TWP".equalsIgnoreCase(vehicle_type) || "2-wheeler".equalsIgnoreCase(vehicle_type) )
			return "2-wheeler";
		else if ("PCP".equalsIgnoreCase(vehicle_type) || "4-wheeler".equalsIgnoreCase(vehicle_type))
			return "4-wheeler";
		else
			return vehicle_type;
	}

	@RequestMapping(value = "/case/case-status", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public InspectionCase fethcCaseStatus(
			@RequestParam(value = "case_id", required = true) long caseId,
			HttpServletRequest request) throws Exception {
            
            URL url = new URL(request.getRequestURL().toString());

            String path = (String) request.getAttribute("javax.servlet.forward.request_uri");
            String query = (String) request.getAttribute("javax.servlet.forward.query_string");
            URI uri = new URI(url.getProtocol(),url.getUserInfo(),url.getHost(),url.getPort(),path,query,null);

            InspectionCase ic = ((InspectionCase) caseDAO.getCaseById(caseId));
            if(uri.toString().contains("localhost"))
                ic.setDownloadKey(uri + "/jadu2/util/cases/download-uploaded-photos/" + ic.getDownloadKey());
            else
                ic.setDownloadKey(uri + "/util/cases/download-uploaded-photos/" + ic.getDownloadKey());
            return ic;
	}

	@ExceptionHandler(Exception.class)
	public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
		ErrorHandler.handleError(ex, response);
		System.out.println(ex.getStackTrace());
	}
}
