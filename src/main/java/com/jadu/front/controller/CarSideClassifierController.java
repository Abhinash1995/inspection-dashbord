package com.jadu.front.controller;

import com.ibm.watson.developer_cloud.visual_recognition.v3.VisualRecognition;
import com.ibm.watson.developer_cloud.visual_recognition.v3.model.ClassifiedImages;
import com.ibm.watson.developer_cloud.visual_recognition.v3.model.ClassifyOptions;
import com.jadu.error.handler.ErrorHandler;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/classify")
public class CarSideClassifierController {
    
    @RequestMapping(value = "/sides", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public ClassifiedImages classify(
            @RequestParam(value = "vehicle_photo", required=true) MultipartFile vehiclePhoto
    ){
        InputStream imagesStream = null;
        try {
            VisualRecognition service = new VisualRecognition(
                    VisualRecognition.VERSION_DATE_2016_05_20
            );    
            
            service.setApiKey("2266776125d2ba3e07cdce0d63a697b0a46d3f63");
            
            imagesStream = vehiclePhoto.getInputStream();  //new FileInputStream("./fruitbowl.jpg");
            
            ClassifyOptions classifyOptions = new ClassifyOptions.Builder()
                    .imagesFile(imagesStream)
                    .imagesFilename(vehiclePhoto.getOriginalFilename())
                    .parameters("{\"classifier_ids\": [\"CarSideClassifier_1273347909\"], \"owners\" : [ \"me\"]}")
                    .build();
            
            ClassifiedImages result = service.classify(classifyOptions).execute();
            
            return (result);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CarSideClassifierController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CarSideClassifierController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                imagesStream.close();
            } catch (IOException ex) {
                Logger.getLogger(CarSideClassifierController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return null;
    }
    
    @RequestMapping(value = "/cars", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public ClassifiedImages classifyCars(
            @RequestParam(value = "vehicle_photo", required=true) MultipartFile vehiclePhoto
    ){
        InputStream imagesStream = null;
        try {
            VisualRecognition service = new VisualRecognition(
                    VisualRecognition.VERSION_DATE_2016_05_20
            );    
            
            service.setApiKey("6f4d6cf67526375ecd7776a77f5455fca6fba42f");
            
            imagesStream = vehiclePhoto.getInputStream();  //new FileInputStream("./fruitbowl.jpg");
            
            ClassifyOptions classifyOptions = new ClassifyOptions.Builder()
                    .imagesFile(imagesStream)
                    .imagesFilename(vehiclePhoto.getOriginalFilename())
                    .parameters("{\"classifier_ids\": [\"CarModelClassifier_808688106\"], \"owners\" : [ \"me\"]}")
                    .build();
            
            ClassifiedImages result = service.classify(classifyOptions).execute();
            
            return (result);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CarSideClassifierController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CarSideClassifierController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                imagesStream.close();
            } catch (IOException ex) {
                Logger.getLogger(CarSideClassifierController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return null;
    }
    
    @ExceptionHandler(Exception.class)
    public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
        ErrorHandler.handleError(ex, response);
        ex.printStackTrace();
    }
    
}
