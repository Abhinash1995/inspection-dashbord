/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.api;

import com.itgi.web.service.DCTMWebServiceImpl;
import com.itgi.web.service.DCTMWebServiceImplServiceLocator;
import com.itgi.web.service.DCTMWebServiceResult;
import com.itgi.web.service.KeyValueMap;
import com.itgi.web.service.WebContent;
import com.jadu.model.CasePhoto;
import com.jadu.model.CaseType;
import com.siebel.CustomUI.ITGIMotorPreInspectionAgencyRecommendationResponse_Input;
import com.siebel.CustomUI.ITGIMotorPreInspectionAgencyRecommendationResponse_Output;
import com.siebel.CustomUI.ITGIMotorPreInspectionAgencyResponse;
import com.siebel.CustomUI.ITGI_spcMotor_spcPreInspection_spcAgency_spcResponseLocator;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.rpc.ServiceException;

/**
 *
 * @author gkumar
 */
public class IffcoCentralOffice extends ThirdPartyApi{

    @Override
    public void execute() {
        try {
            ITGI_spcMotor_spcPreInspection_spcAgency_spcResponseLocator l = new ITGI_spcMotor_spcPreInspection_spcAgency_spcResponseLocator();
            ITGIMotorPreInspectionAgencyResponse s = l.getITGIMotorPreInspectionAgencyResponse();
            
            ITGIMotorPreInspectionAgencyRecommendationResponse_Input input = new ITGIMotorPreInspectionAgencyRecommendationResponse_Input(
                    this.onlineCase.getInspectionNumber(),
                    getRecommendationResponse(),
                    ic.getInspectionLatitude() + " ," + ic.getInspectionLongitude(),
                    getInspectionLocation(),
                    String.valueOf(ic.getId()),
                    ic.getComment(),
                    ic.getChassisNumber(),
                    getInspectionTime(),
                    ic.getEngineNumber()
            );
            
            ITGIMotorPreInspectionAgencyRecommendationResponse_Output res = s.ITGIMotorPreInspectionAgencyRecommendationResponse(input);
            
            System.out.println(res);
            
            for(CasePhoto c : this.casePhotos){
                DCTMWebServiceResult result = uploadDocument(c.getFileName(), getDocCode(c));
                System.out.println(result);
            }
            
            DCTMWebServiceResult result = uploadDocument("report.pdf", "INSPECTION_RPT");
            System.out.println(result);
            
            
                    
        } catch (ServiceException | RemoteException ex) {
            Logger.getLogger(IffcoCentralOffice.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(IffcoCentralOffice.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public boolean isQualified() {
        return (ic.getRemark().equals("recommended") || ic.getRemark().equals("not-recommended") || ic.getRemark().equals("underwriter") );
    }
    
    private DCTMWebServiceResult uploadDocument(
            String fileName,
            String docCode
    ) throws ServiceException, RemoteException{
        DCTMWebServiceImplServiceLocator ll = new DCTMWebServiceImplServiceLocator();
        DCTMWebServiceImpl ser = ll.getDCTMWebServiceImpl();

        KeyValueMap[] metadata = {
            new KeyValueMap("source", "upreinsp"),
            new KeyValueMap("product", getProduct()),
            new KeyValueMap("file_name", fileName),
            new KeyValueMap("doc_code", docCode),
            new KeyValueMap("preinsp_id", onlineCase.getInspectionNumber())
        };

        WebContent webContent = new WebContent(
                s3Service.readFile("cases/" + ic.getId() + "/" + fileName)
                , metadata);

        WebContent[] webContents = {webContent};
        return ser.upload(webContents);
    }
    
    private String getProduct(){
        switch(ic.getVehicleType().getId()){
            case "2-wheeler": return "TWP";
            case "4-wheeler": return "PCP";
            case "commercial": return "CVI";
        }
        return null;
    }
    
    private String getDocCode(CasePhoto casePhoto){
        switch(casePhoto.getPhotoType().getId()){
            case "rc": return "RC";
            default: return "OTHRS";
        }
    }
    
    private String getRecommendationResponse() throws Exception{
        switch(ic.getRemark()){
            case "recommended":     return "Report Recommended";
            case "not-recommended": return "Report Not Recommended";
            case "underwriter":     return "Report Referred To Underwriter";
        }
        
        throw new Exception("Invalid Recommendation status!");
    }
    
    private String getInspectionTime(){
        String pattern = "MM/dd/YYYY HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(ic.getQcTime());
    }
    
    private String getInspectionLocation(){
        String result =  googleLocationService.getAreaByLatLng (casePhotos.get(0).getLatitude(), casePhotos.get(0).getLongitude());        
        return result == null ? "NOT_FOUND" : result;
    }
}
