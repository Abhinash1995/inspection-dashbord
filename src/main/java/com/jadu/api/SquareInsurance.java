/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.api;

import com.jadu.model.InspectionCase;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import okhttp3.Credentials;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author gkumar
 */
public class SquareInsurance extends ThirdPartyApi{

    @Override
    public void execute() {
        try {
            
            String downloadKey = ("https://iffco.wimwisure.com/util/cases/download-uploaded-photos/" + ic.getDownloadKey());
            
            // OkHttpClient client = new OkHttpClient();
            
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        
            OkHttpClient client = builder.build();
            
            
            MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
            params.add("token", "e598f18276f98587944c8d430197432b");
            params.add("reportDownloadLink", downloadKey);
            params.add("caseId", String.valueOf(ic.getId()));
            params.add("inspectionDate", ic.getQcTime().toString());
            params.add("branch", ic.getCompanyBranchDivision().getBranch());
            params.add("vehicleRegNo", ic.getVehicleNumber());
            params.add("status", convertStatus(ic));
            
            UriComponents uriComponents =     UriComponentsBuilder.fromHttpUrl("https://squareinsurance.in/api/breaking/iffco.php").queryParams(params).build();

            System.out.println(uriComponents.toUriString());
            
            Request request = new Request.Builder()
                .url(uriComponents.toUriString())
                .get()
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("cache-control", "no-cache")
                .addHeader("Postman-Token", "0ce4a033-358e-4508-b92c-a654cdca3878")
                .build();

            Response response = client.newCall(request).execute();
            System.out.println(response);

        } catch (JSONException ex) {
            Logger.getLogger(PolicyBachat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PolicyBachat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PolicyBachat.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    private static String convertStatus(InspectionCase ic) throws Exception {
        if(ic.getCurrentStage() == -1) 
            return "CLOSED";
        
        String remark = ic.getRemark();
        if (remark != null && remark.equalsIgnoreCase("recommended")) {
            return "APPROVED";
        } else if (remark != null && remark.equalsIgnoreCase("not-recommended")) {
            return "REJECTED";
        }
        throw new Exception("Invalid Status");
    }

    @Override
    public boolean isQualified() {
        return (ic.getRemark().equals("recommended") || ic.getRemark().equals("not-recommended") || ic.getRemark().equals("underwriter") );
    }
    
}
