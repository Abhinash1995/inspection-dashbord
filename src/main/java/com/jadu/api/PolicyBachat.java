/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.api;

import com.jadu.model.InspectionCase;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import okhttp3.Credentials;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author gkumar
 */
public class PolicyBachat extends ThirdPartyApi{

    @Override
    public void execute() {
        try {
            
            String downloadKey = ("https://iffco.wimwisure.com/util/cases/download-uploaded-photos/" + ic.getDownloadKey());
            
            OkHttpClient client = new OkHttpClient();
            
            MediaType mediaType = MediaType.parse("application/json");
            JSONObject result = new JSONObject();
            result.put("vehicleRegNo", ic.getVehicleNumber());
            result.put("caseId", ic.getId());
            result.put("inspectionDate", ic.getQcTime().toString());
            result.put("status", convertStatus(ic));
            result.put("branch", ic.getCompanyBranchDivision().getBranch());
            result.put("reportDownloadLink", downloadKey);
            RequestBody body = RequestBody.create(mediaType, result.toString());
            
            String credential = Credentials.basic("pbwimwisure", "pbwimwisure@$32!");
            
            Request request = new Request.Builder()
<<<<<<< HEAD
                    .url("https://uatexservice.policybachat.com/api/WimwisureApi/UpdateCaseStatus")
=======
                    .url("https://externalservices.policybachat.com/inspectionapi/Wimwisure/UpdateCaseStatus")
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
                    .post(body)
                    .addHeader("APIKey", "QGP759ZGM153AGL456")
                    .addHeader("Content-Type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("Authorization", credential)
                    .build();
            
            Response response = client.newCall(request).execute();
            System.out.println(response);
        } catch (JSONException ex) {
            Logger.getLogger(PolicyBachat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PolicyBachat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PolicyBachat.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    private static String convertStatus(InspectionCase ic) throws Exception {
        if(ic.getCurrentStage() == -1) 
            return "CLOSED";
        
        String remark = ic.getRemark();
        if (remark != null && remark.equalsIgnoreCase("recommended")) {
            return "APPROVED";
        } else if (remark != null && remark.equalsIgnoreCase("not-recommended")) {
            return "REJECTED";
        }
        throw new Exception("Invalid Status");
    }

    @Override
    public boolean isQualified() {
        return (ic.getRemark().equals("recommended") || ic.getRemark().equals("not-recommended") || ic.getRemark().equals("underwriter") );
    }
    
}
