package com.jadu.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.jadu.dao.CaseDAO;
import com.jadu.dao.CaseLogDAO;
import com.jadu.dao.CasePhotoDAO;
import com.jadu.dao.InspectionCasesHistoryDAO;
import com.jadu.dao.PhotoTypeDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.dao.VehicleDAO;
import com.jadu.dao.VehicleFuelTypeDAO;
import com.jadu.dao.VehicleTypeDAO;
import com.jadu.model.CaseType;
import com.jadu.model.InspectionCase;
import com.jadu.model.InspectionCasesHistory;
import com.jadu.push.notification.AndroidPushNotificationsService;

@Service
public class PruneCaseService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PruneCaseService.class);

	@Autowired
	S3DirectUploadService s3DirectUploadService;

	@Autowired
	S3Service s3Service;

	@Autowired
	CaseDAO caseDAO;

	@Autowired
	Environment env;

	@Autowired
	VehicleDAO vehicleDAO;

	@Autowired
	PhotoTypeDAO photoTypeDAO;

	@Autowired
	VehicleFuelTypeDAO vehicleFuelTypeDAO;

	@Autowired
	VehicleTypeDAO vehicleTypeDAO;

	@Autowired
	AndroidPushNotificationsService androidPushNotificationsService;

	@Autowired
	CaseLogDAO caseLogDAO;

	@Autowired
	UserDAOImpl userDAO;

	@Autowired
	CasePhotoDAO casePhotoDAO;

	@Autowired
	SmsService smsService;

	@Autowired
	URLShortnerService urlShortnerService;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private InspectionCasesHistoryDAO inspectionCasesHistoryDAO;

	public String pruneAndPersistInInspectionCasesHistory(Date fromDate, Date toDate, Integer currentStage,
			int caseType) {
		String prunedCases = "";
		List<InspectionCase> cases = caseDAO.getCaseByTypeAndStageAndCreatedTime(currentStage, caseType, fromDate,
				toDate);
		LOGGER.info("Total={} cases to be pruned for currentStage={},caseType={},from={},to={}", cases.size(),
				currentStage, caseType, fromDate, toDate);
		for (InspectionCase inspectionCase : cases) {
			boolean isPersisted = persistInInspectionCasesHistory(inspectionCase);
			if (isPersisted) {
				pruneFromInspectioncase(inspectionCase);
				LOGGER.info("case={} has been pruned", inspectionCase.getId());
				prunedCases += inspectionCase.getId() + " , ";
			}
		}
		return prunedCases;

	}

	private void pruneFromInspectioncase(InspectionCase inspectionCase) {
		try {
			if (appConfigService.getBooleanProperty("IS_TO_DELETE_CASE_UNDER_PRUNING_ACTIVITY", true)) {
				caseDAO.deleteCase(inspectionCase);
				LOGGER.info("case={} has been deleted", inspectionCase.getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception while deleting case={}, error={}", inspectionCase.getId(), e.getMessage());
		}
	}

	public String pruneAndPersistInInspectionCasesHistory(Integer currentStage, int caseType) {
		String prunedCases = "";
		List<InspectionCase> cases = caseDAO.getCaseByCaseTypeAndStage(currentStage, caseType, getThresholdDate());
		LOGGER.info("Total={} cases to be pruned for currentStage={},caseType={}", cases.size(), currentStage,
				caseType);
		for (InspectionCase inspectionCase : cases) {
			boolean isPersisted = persistInInspectionCasesHistory(inspectionCase);
			if (isPersisted) {
				pruneFromInspectioncase(inspectionCase);
				LOGGER.info("case={} has been pruned", inspectionCase.getId());
				prunedCases += inspectionCase.getId() + " , ";
			}
		}
		return prunedCases;
	}

	private Date getThresholdDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MONTH, -appConfigService.getIntProperty("THRESHOLD_TIME_NOT_TO_BE_PRUNED", 6));
		return calendar.getTime();
	}

	public String pruneAndPersistInInspectionCasesHistory(Long caseId, int caseType) {
		InspectionCase inspectionCase = null;
		try {
			inspectionCase = (InspectionCase) caseDAO.getCaseById(caseId);
			if (inspectionCase != null && isToPruneCase(inspectionCase, caseType)) {
				boolean isPersisted = persistInInspectionCasesHistory(inspectionCase);
				if (isPersisted) {
					pruneFromInspectioncase(inspectionCase);
					LOGGER.info("case={} has been pruned", inspectionCase.getId());
					return "" + caseId;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception raised while pruning case={}, error={}", caseId, e.getMessage());
		}
		return null;
	}

	private boolean isToPruneCase(InspectionCase inspectionCase, int caseType) {
		if (inspectionCase.getCurrentStage() == 3 || inspectionCase.getCurrentStage() == 4
				|| CaseType.OFFLINE.ordinal() != caseType)
			return false;
		return true;
	}

	private boolean persistInInspectionCasesHistory(InspectionCase inspectionCase) {
		try {
			List<InspectionCasesHistory> inspectionCasesHistoryList = inspectionCasesHistoryDAO
					.getCaseById(inspectionCase.getId());
			if (inspectionCasesHistoryList != null && !inspectionCasesHistoryList.isEmpty()) {
				return true;
			}
			try {
				InspectionCasesHistory inspectionCasesHistory = new InspectionCasesHistory();
				inspectionCasesHistory.setId(inspectionCase.getId());
				inspectionCasesHistory.setAllocationAttempts(inspectionCase.getAllocationAttempts());
				inspectionCasesHistory.setAllocationStatus(inspectionCase.getAllocationStatus());
				inspectionCasesHistory.setAttemptExpiryTime(inspectionCase.getAttemptExpiryTime());
				inspectionCasesHistory.setBranch(inspectionCase.getBranch());
				inspectionCasesHistory.setCaseAttemptAllocation(inspectionCase.getCaseAttemptAllocation());
				inspectionCasesHistory.setCaseType(inspectionCase.getCaseType());
				inspectionCasesHistory.setCaseUserAllocations(inspectionCase.getCaseUserAllocations());
				inspectionCasesHistory.setChassisNumber(inspectionCase.getChassisNumber());
				inspectionCasesHistory.setCloseReason(inspectionCase.getCloseReason());
				inspectionCasesHistory.setCloseTime(inspectionCase.getCloseTime());
				inspectionCasesHistory.setComment(inspectionCase.getComment());
				inspectionCasesHistory.setCompanyBranchDivision(inspectionCase.getCompanyBranchDivision());
				inspectionCasesHistory.setCreationTime(inspectionCase.getCreationTime());
				inspectionCasesHistory.setCurrentStage(inspectionCase.getCurrentStage());
				inspectionCasesHistory.setCustomerEmail(inspectionCase.getCustomerEmail());
				inspectionCasesHistory.setCustomerName(inspectionCase.getCustomerName());
				inspectionCasesHistory.setCustomerPhoneNumber(inspectionCase.getCustomerPhoneNumber());
				inspectionCasesHistory.setDivision(inspectionCase.getDivision());
				inspectionCasesHistory.setDownloadKey(inspectionCase.getDownloadKey());
				inspectionCasesHistory.setEncryptionKey(inspectionCase.getEncryptionKey());
				inspectionCasesHistory.setEngineNumber(inspectionCase.getEngineNumber());
				inspectionCasesHistory.setZone(inspectionCase.getZone());
				inspectionCasesHistory.setVideosUploaded(inspectionCase.isVideosUploaded());
				inspectionCasesHistory.setVehicleYOM(inspectionCase.getVehicleYOM());
				inspectionCasesHistory.setVehicleType(inspectionCase.getVehicleType());
				inspectionCasesHistory.setVehicleNumber(inspectionCase.getVehicleNumber());
				inspectionCasesHistory.setVehicleFuelType(inspectionCase.getVehicleFuelType());
				inspectionCasesHistory.setVehicleColor(inspectionCase.getVehicleColor());
				inspectionCasesHistory.setVehicle(inspectionCase.getVehicle());
				inspectionCasesHistory.setState(inspectionCase.getState());
				inspectionCasesHistory.setRequestorPhoneNumber(inspectionCase.getRequestorPhoneNumber());
				inspectionCasesHistory.setRequestorName(inspectionCase.getRequestorName());
				inspectionCasesHistory.setRequestorEmail(inspectionCase.getRequestorEmail());
				inspectionCasesHistory.setReopenTime(inspectionCase.getReopenTime());
				inspectionCasesHistory.setReopenReason(inspectionCase.getReopenReason());
				inspectionCasesHistory.setRemark(inspectionCase.getRemark());
				inspectionCasesHistory.setRecommendation(inspectionCase.getRecommendation());
				inspectionCasesHistory.setQcTime(inspectionCase.getQcTime());
				inspectionCasesHistory.setQcReopenTime(inspectionCase.getQcReopenTime());
				inspectionCasesHistory.setQcReopenReason(inspectionCase.getQcReopenReason());
				inspectionCasesHistory.setQcPhone(inspectionCase.getQcPhone());
				inspectionCasesHistory.setQcName(inspectionCase.getQcName());
				inspectionCasesHistory.setQcEmail(inspectionCase.getQcEmail());
				inspectionCasesHistory.setPurposeOfInspection(inspectionCase.getPurposeOfInspection());
				inspectionCasesHistory.setPhotosUploaded(inspectionCase.isPhotosUploaded());
				inspectionCasesHistory.setOdometerReading(inspectionCase.getOdometerReading());
				inspectionCasesHistory.setModel(inspectionCase.getModel());
				inspectionCasesHistory.setMake(inspectionCase.getMake());
				inspectionCasesHistory.setLastUpdatedTime(inspectionCase.getLastUpdatedTime());
				inspectionCasesHistory.setInsuranceCompany(inspectionCase.getInsuranceCompany());
				inspectionCasesHistory.setInspectionType(inspectionCase.getInspectionType());
				inspectionCasesHistory.setInspectionAddress(inspectionCase.getInspectionAddress());
				inspectionCasesHistory.setInspectionLatitude(inspectionCase.getInspectionLatitude());
				inspectionCasesHistory.setInspectionLongitude(inspectionCase.getInspectionLongitude());
				inspectionCasesHistory.setInspectionStage(inspectionCase.getInspectionStage());
				inspectionCasesHistory.setInspectionStartTime(inspectionCase.getInspectionStartTime());
				inspectionCasesHistory.setInspectionSubmitTime(inspectionCase.getInspectionSubmitTime());
				inspectionCasesHistory.setInspectionTime(inspectionCase.getInspectionTime());
				inspectionCasesHistory.setFileAvailableTime(inspectionCase.getFileAvailableTime());
				inspectionCasesHistory.setFileAvailable(inspectionCase.isFileAvailable());
				inspectionCasesHistory.setPruneTime(new Date());
				inspectionCasesHistory = inspectionCasesHistoryDAO.save(inspectionCasesHistory);
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error("Exception while persisting case={} in history, error={}", inspectionCase.getId(),
						e.getMessage());
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception while persisting case={} in history, error={}", inspectionCase.getId(),
					e.getMessage());
			return false;
		}

	}

}
