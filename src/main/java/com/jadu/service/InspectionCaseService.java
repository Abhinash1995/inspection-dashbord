package com.jadu.service;

import com.jadu.dao.CaseDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class InspectionCaseService {
    @Autowired
    CaseDAO caseDAO;
    
    @Autowired
    GoogleLocationService googleLocationService;
    
    @Async
    public void updateInspectionCaseAddress(long inspectionCase, Double latitude, Double longitude){
        String locality = googleLocationService.getLocalityByLatLng(latitude, longitude);
        caseDAO.updateCaseAddress(inspectionCase, locality);
        System.out.println("Updated case address!");
    }
}
