package com.jadu.service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.HttpMethod;
import com.amazonaws.Protocol;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.MultipleFileDownload;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.jadu.helpers.FileHelper;
import com.jadu.helpers.FileMimeType;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class S3Service {

    @Autowired
    private AppConfigService appConfigService;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(S3Service.class);

    private final static String keyId = "S3_ACCESS_KEY";
    private final static String secretKeyId = "S3_SECRET_KEY";
    private final static String bucketId = "S3_BUCKET_NAME";

    public void uploadFile(String uploadFullPath, InputStream is, ObjectMetadata meta) {
        AmazonS3 s3client = new AmazonS3Client(new BasicAWSCredentials(appConfigService.getProperty(keyId),
                appConfigService.getProperty(secretKeyId)));

        s3client.putObject(appConfigService.getProperty(bucketId), uploadFullPath, is, meta);
    }

    public void uploadMultipartImage(MultipartFile multipartFile, String uploadFullPath, String uploadThumbFullPath) throws Exception {
        File serverFile = FileHelper.convert(multipartFile);

        BufferedImage resizeMe = ImageIO.read(serverFile);

        String extension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());

        // resizeMe.getWidth();
        Dimension newMaxSize = new Dimension(255, 255);
        BufferedImage resizedImg = Scalr.resize(resizeMe, Method.QUALITY, newMaxSize.width, newMaxSize.height);
        File file = File.createTempFile(Long.toString(System.currentTimeMillis()), "." + extension);
        ImageIO.write(resizedImg, extension, file);
        try {
            this.uploadFile(serverFile, uploadFullPath);
            this.uploadFile(file, uploadThumbFullPath);
        } finally {
            if (serverFile != null) {
                serverFile.delete();
            }
        }

    }

    public void uploadMultipartImage(MultipartFile multipartFile, String uploadFullPath) throws Exception {
        File serverFile = FileHelper.convert(multipartFile);

        try {
            this.uploadFile(serverFile, uploadFullPath);
        } finally {
            if (serverFile != null) {
                serverFile.delete();
            }
        }

    }

    public void uploadMultipartFile(MultipartFile multipartFile, String uploadFullPath /* Directory concatenated with file name */
    ) throws Exception {
        File serverFile = FileHelper.convert(multipartFile);

        try {
            this.uploadFile(serverFile, uploadFullPath);
        } finally {
            if (serverFile != null) {
                serverFile.delete();
            }
        }

    }

    public void uploadMultipartFileDirect(MultipartFile multipartFile, String uploadFullPath) throws Exception {

        String mimeType = FileMimeType.fromExtension(FilenameUtils.getExtension(multipartFile.getOriginalFilename()))
                .mimeType();
        byte[] buffer = new byte[1024];

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                InputStream in = multipartFile.getInputStream();) {

            int len;
            while ((len = in.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }

            ObjectMetadata meta = new ObjectMetadata();
            meta.setContentLength(outputStream.size());
            meta.setContentType(mimeType);

            try (ByteArrayInputStream is = new ByteArrayInputStream(outputStream.toByteArray())) {
                uploadFile(uploadFullPath, is, meta);
            }
        }

    }

    public void uploadFile(String uploadFullPath, byte[] imageByte, String contentType) throws Exception {
        AmazonS3 s3client = new AmazonS3Client(new BasicAWSCredentials(appConfigService.getProperty(keyId),
                appConfigService.getProperty(secretKeyId)));
        try {
            InputStream fis = new ByteArrayInputStream(imageByte);

            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(imageByte.length);
<<<<<<< HEAD
            metadata.setContentType(contentType);
=======
            
            if(contentType != null)
                metadata.setContentType(contentType);
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb

            /*
             * PutObjectRequest putObjectRequest = new PutObjectRequest(
             * appConfigService.getProperty(bucketId), uploadFullPath, file );
             * putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead); // public
             * for all s3client.putObject(putObjectRequest); // upload file
             */
            s3client.putObject(appConfigService.getProperty(bucketId), uploadFullPath, fis, metadata);

        } catch (AmazonServiceException ase) {
            /*
             * Caught an AmazonServiceException, which means your request made it to Amazon
             * S3, but was rejected with an error response for some reason.
             */

            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
            LOGGER.info("Exception raised while uplaoding file, error={}", ase.getMessage());
            throw new Exception("Unable to upload file!");
        } catch (AmazonClientException ace) {
            /*
             * Caught an AmazonClientException, which means the client encountered an
             * internal error while trying to communicate with S3, such as not being able to
             * access the network.
             */

            System.out.println("Error Message: " + ace.getMessage());
            LOGGER.info("Exception raised while uplaoding file, error={}", ace.getMessage());
            throw new Exception("Unable to upload file!");
        }
    }

    public void uploadFile(File file, String uploadFullPath /* Directory concatenated with file name */
    ) throws Exception {

        AmazonS3 s3client = new AmazonS3Client(new BasicAWSCredentials(appConfigService.getProperty(keyId),
                appConfigService.getProperty(secretKeyId)));
        try {
            PutObjectRequest putObjectRequest = new PutObjectRequest(appConfigService.getProperty(bucketId),
                    uploadFullPath, file);
            putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead); // public for all
            s3client.putObject(putObjectRequest); // upload file

        } catch (AmazonServiceException ase) {
            /*
             * Caught an AmazonServiceException, which means your request made it to Amazon
             * S3, but was rejected with an error response for some reason.
             */

            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
            LOGGER.info("Exception raised while uplaoding file, error={}", ase.getMessage());
            throw new Exception("Unable to upload file!");
        } catch (AmazonClientException ace) {
            /*
             * Caught an AmazonClientException, which means the client encountered an
             * internal error while trying to communicate with S3, such as not being able to
             * access the network.
             */

            System.out.println("Error Message: " + ace.getMessage());
            LOGGER.info("Exception raised while uplaoding file, error={}", ace.getMessage());
            throw new Exception("Unable to upload file!");
        }
    }

    public byte[] readFile(String fileFullPath) {
        try {
            byte[] result = null;
            try (InputStream objectData = readFileStream(fileFullPath)) {
                if (objectData != null) {
                    result = IOUtils.toByteArray(objectData);
                }
            }
            return result;
        } catch (IOException | AmazonS3Exception ex) {
            Logger.getLogger(S3Service.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public InputStream readFileStream(String fileFullPath) {
        try {
            /*
             * AmazonS3 s3Client = new AmazonS3Client(new
             * BasicAWSCredentials(appConfigService.getProperty(keyId),
             * appConfigService.getProperty(secretKeyId)));
             */
            AmazonS3 s3client = getAwsClient();

            S3Object object = s3client
                    .getObject(new GetObjectRequest(appConfigService.getProperty(bucketId), fileFullPath));

            return object.getObjectContent();
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Exception while downloading file={}, error={}", fileFullPath, e.getMessage());
        }
        return null;
    }

    public AmazonS3 getAwsClient() {

        ClientConfiguration clientConfig = new ClientConfiguration();
        clientConfig.setProtocol(Protocol.HTTP);
        clientConfig.setUseReaper(true);
        clientConfig.setConnectionMaxIdleMillis(1000);
        BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(appConfigService.getProperty(keyId),
                appConfigService.getProperty(secretKeyId));

        return AmazonS3ClientBuilder.standard().withClientConfiguration(clientConfig)
                .withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials))
                .withRegion(appConfigService.getProperty("AWS_S3_REGION", "us-east-2")).build();

    }

    public ObjectMetadata readFileMeta(String fileFullPath) {
        AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(appConfigService.getProperty(keyId),
                appConfigService.getProperty(secretKeyId)));

        S3Object object = s3Client
                .getObject(new GetObjectRequest(appConfigService.getProperty(bucketId), fileFullPath));

        return object.getObjectMetadata();
    }

    public List<String> getObjectslistFromFolder() {

        AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(appConfigService.getProperty(keyId),
                appConfigService.getProperty(secretKeyId)));

        ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
                .withBucketName(appConfigService.getProperty(bucketId));

        List<String> keys = new ArrayList<>();

        ObjectListing objects = s3Client.listObjects(listObjectsRequest);

        for (S3ObjectSummary summary : objects.getObjectSummaries()) {
            keys.add(summary.getKey());
        }

        return keys;
    }

    public boolean exists(String fileFullPath) {
        AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(appConfigService.getProperty(keyId),
                appConfigService.getProperty(secretKeyId)));

        try {
            S3Object object = s3Client
                    .getObject(new GetObjectRequest(appConfigService.getProperty(bucketId), fileFullPath));
            object.getObjectMetadata();
        } catch (AmazonServiceException e) {
            return false;
        }
        return true;
    }

    public String getSignedUrl(String objectKey) throws Exception {
        return getSignedUrl(objectKey, 60);
    }
    
    public String getSignedUrl(String objectKey, int expiryMinutes) throws Exception {
        try {

            AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(appConfigService.getProperty(keyId),
                    appConfigService.getProperty(secretKeyId)));

            // Set the presigned URL to expire after one hour.
            java.util.Date expiration = new java.util.Date();
            long expTimeMillis = expiration.getTime();
            expTimeMillis += 1000 * 60 * expiryMinutes;
            expiration.setTime(expTimeMillis);

            // Generate the pre-signed URL.
            System.out.println("Generating pre-signed URL.");
            GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(
                    appConfigService.getProperty(bucketId), objectKey).withMethod(HttpMethod.PUT)
                    .withExpiration(expiration);

            URL url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);

            return url.toString();
        } catch (AmazonServiceException e) {
			// The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            e.printStackTrace();
            LOGGER.info("Exception raised while uplaoding file, error={}", e.getMessage());
            throw new Exception("Storage server couldn't process the request. Please try again later!");
        } catch (SdkClientException e) {
			// Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
            LOGGER.info("Exception raised while uplaoding file, error={}", e.getMessage());
            throw new Exception("Unable to reach storage server. Please try again later!");
        }
    }
    
    public String getReadSignedUrl(String objectKey, int expiryMinutes) throws Exception {
        try {

            AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(appConfigService.getProperty(keyId),
                    appConfigService.getProperty(secretKeyId)));

            // Set the presigned URL to expire after one hour.
            java.util.Date expiration = new java.util.Date();
            long expTimeMillis = expiration.getTime();
            expTimeMillis += 1000 * 60 * expiryMinutes;
            expiration.setTime(expTimeMillis);

            // Generate the pre-signed URL.
            System.out.println("Generating pre-signed URL.");
            GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(
                    appConfigService.getProperty(bucketId), objectKey).withMethod(HttpMethod.GET)
                    .withExpiration(expiration);

            URL url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);

            return url.toString();
        } catch (AmazonServiceException e) {
			// The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            e.printStackTrace();
            LOGGER.info("Exception raised while uplaoding file, error={}", e.getMessage());
            throw new Exception("Storage server couldn't process the request. Please try again later!");
        } catch (SdkClientException e) {
			// Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
            LOGGER.info("Exception raised while uplaoding file, error={}", e.getMessage());
            throw new Exception("Unable to reach storage server. Please try again later!");
        }
    }

    public void copyFile(String source, String destination) {

        AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(appConfigService.getProperty(keyId),
                appConfigService.getProperty(secretKeyId)));

        CopyObjectRequest copyObjRequest = new CopyObjectRequest(appConfigService.getProperty(bucketId), source,
                appConfigService.getProperty(bucketId), destination);

        s3Client.copyObject(copyObjRequest);
    }

    @Async
    public void downloadFolder(String source, String destination) {
        copyFile(source, destination);
    }

    public MultipleFileDownload downloadFolder(String caseId)
            throws AmazonServiceException, AmazonClientException, InterruptedException {

        AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(appConfigService.getProperty(keyId),
                appConfigService.getProperty(secretKeyId)));

        TransferManager transferManager = TransferManagerBuilder.standard()
                .withS3Client(AmazonS3ClientBuilder.standard()
                        .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(
                                                appConfigService.getProperty(keyId), appConfigService.getProperty(secretKeyId))))
                        .withRegion("us-east-2").build())
                .build();// TransferManagerBuilder.standard().build();
        String home = System.getProperty("user.home");

        File dir = new File(home + "/tmp/");

        MultipleFileDownload download = transferManager.downloadDirectory(appConfigService.getProperty(bucketId),
                "cases/" + caseId + "/", dir);
        download.waitForCompletion();
        return download;
    }

    public List<S3ObjectSummary> getObjectsInBucket(String prefix, String regex) {

        AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(appConfigService.getProperty(keyId),
                appConfigService.getProperty(secretKeyId)));

        ObjectListing listing = s3Client.listObjects(appConfigService.getProperty(bucketId), prefix);
        List<S3ObjectSummary> summaries = listing.getObjectSummaries();

        while (listing.isTruncated()) {
            listing = s3Client.listNextBatchOfObjects(listing);

            summaries.addAll(listing.getObjectSummaries());
        }

        if (regex != null) {
            List<S3ObjectSummary> result = new ArrayList<>();
            for (S3ObjectSummary s : summaries) {
                if (s.getKey().contains(regex)) {
                    result.add(s);
                }
            }

            return result;
        } else {
            return summaries;
        }
    }
}
