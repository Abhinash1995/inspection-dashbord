package com.jadu.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.jadu.dao.OTPDAO;
import com.jadu.helpers.TimeHelper;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.CaseType;
import com.jadu.model.InspectionCase;
import com.jadu.model.OTP;
import com.jadu.model.User;

@Service
public class SmsService {

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private OTPDAO otpDAO;

	@Autowired
	private RedisService redisService;

	@Autowired
	private URLShortnerService urlShortnerService;

	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(SmsService.class);

	@Async
	public void sendSMSAsync(String mobileNumber, String message) {
		try {
			sendSMS(mobileNumber, message);
		} catch (IOException ex) {
			Logger.getLogger(SmsService.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void sendSMS(String mobileNumber, String message) throws MalformedURLException, IOException {
		String url = appConfigService.getProperty("SMS_SERVICE_URL");
		url += "?APIKEY=" + java.net.URLEncoder.encode(appConfigService.getProperty("SMS_SERVICE_API_KEY"), "UTF-8");
		url += "&ServiceName=TEMPLATE_BASED";
		url += "&SenderID=" + java.net.URLEncoder.encode(appConfigService.getProperty("SMS_SERVICE_ID"), "UTF-8");
		url += "&MobileNo=" + java.net.URLEncoder.encode(mobileNumber, "UTF-8");
		url += "&Message=" + java.net.URLEncoder.encode(message, "UTF-8");

		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		int responseCode = con.getResponseCode();

		if (responseCode != 200) {
			StringBuilder response;
			try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
				String inputLine;
				response = new StringBuilder();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
			}

			throw new IOException("Unable to send sms!");
		}
	}

	/**
	 * 
	 * @param user
	 * 
	 *             Send the same otp if not expired otherwise new but do not send
	 *             otp if it has been already sent 5 times in a day
	 */
	@Async
	public void sendRegistrationOTP(User user) {
		boolean isToUseCache = appConfigService.getBooleanProperty("IS_TO_FETCH_SMS_OTP_COUNT_FROM_CACHE", true);
		String otpCount = isToUseCache == true
				? redisService.get("OTP_COUNT_FOR_USER_" + user.getUsername() + "_IN_LAST_24_HRS")
				: null;
		int validity = appConfigService.getIntProperty("PHONE_OTP_VALIDITY", 1440);
		int count = otpCount != null ? Integer.parseInt(otpCount) : 0;
		List<OTP> previousOTPList = otpDAO.getPhoneOtpList(user.getUsername());
		OTP previousOTP = previousOTPList != null && !previousOTPList.isEmpty() ? previousOTPList.get(0) : null;
		// Generate and send otp only if it has not been expired
		if (previousOTP == null || !previousOTP.getExpiryTime().after(new Date())) {
			OTP otp = otpDAO.generatePhoneOtp(user.getUsername(), UtilHelper.getDateAddMinutes(validity), "REGISTER");
			sendRegistrationPhoneOTP(user, validity, otp);
			if (isToUseCache)
				redisService.storeKeyValue("OTP_COUNT_FOR_USER_" + user.getUsername() + "_IN_LAST_24_HRS",
						String.valueOf(count + 1),
						appConfigService.getIntProperty("PHONE_OTP_VALIDITY_IN_SECOND", 24 * 60 * 60));
		} else if (count < appConfigService.getIntProperty("MAX_PHONE_OTP_LIMIT_PER_DAY", 5)) {
			try {
				this.sendSMS(user.getPhoneNumber(), "Your WIMWISure OTP - " + previousOTP.getOtp() + ". Valid for "
						+ TimeHelper.getTimeString(validity) + ".");
				if (isToUseCache)
					redisService.storeKeyValue("OTP_COUNT_FOR_USER_" + user.getUsername() + "_IN_LAST_24_HRS",
							String.valueOf(count + 1),
							appConfigService.getIntProperty("PHONE_OTP_VALIDITY_IN_SECOND", 24 * 60 * 60));
			} catch (Exception e) {
				e.printStackTrace();
				Logger.getLogger(SmsService.class.getName()).log(Level.SEVERE, null, e);

			}
		} else {
			LOGGER.info("OTP quota for the day is over for user={}", user.getPhoneNumber());
		}
	}

	private void sendRegistrationPhoneOTP(User user, int validity, OTP otp) {
		LOGGER.info("OTP for user={} is={}", user.getPhoneNumber(), otp.getOtp());
		try {

			this.sendSMS(user.getPhoneNumber(),
					"Your WIMWISure OTP - " + otp.getOtp() + ". Valid for " + TimeHelper.getTimeString(validity) + ".");
		} catch (Exception ex) {
			Logger.getLogger(SmsService.class.getName()).log(Level.SEVERE, null, ex);
			otp.setSuccess(false);
			otp.setErrorLog(ex.getMessage());
			otpDAO.save(otp);
		}
	}

	@Async
	public void sendRegistrationPassword(User user, String password) {
		try {
			this.sendSMS(user.getPhoneNumber(),
					"Your WIMWISure username is " + user.getPhoneNumber() + " and password is " + password);
		} catch (IOException ex) {
			Logger.getLogger(SmsService.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public boolean validateRegistrationOTP(User user, String otpVal) throws Exception {
		return validateRegistrationOTP(user, otpVal,
				appConfigService.getIntProperty("PHONE_OTP_VALIDITY_IN_SECOND", 24 * 60 * 60));
	}

	public boolean validateRegistrationOTP(User user, String otpVal, int seconds) throws Exception {
		LOGGER.info("OTP Verification request received for user={}", user.getPhoneNumber());
		OTP otp = (OTP) otpDAO.getPhoneOtp(user.getUsername(), otpVal);
		LOGGER.info("OTP in db for user={} is={}", user.getPhoneNumber(), otp);
		if (otp == null || !otp.getOtp().equals(otpVal)) {
			LOGGER.info("OTP Verification is unsuccessfull for user={}, reason incorrect otp={}", user.getPhoneNumber(),
					otpVal);
			throw new Exception("Incorrect OTP");
		}

		if (UtilHelper.getDate().getTime() - otp.getCreationTime().getTime() >= seconds * 1000) {
			LOGGER.info("OTP Verification is unsuccessfull for user={}, reason otp expired", user.getPhoneNumber());
			throw new Exception("OTP expired!");
		}
		LOGGER.info("OTP Verification is successfull for user={}", user.getPhoneNumber());

		return true;
	}

	public boolean sendSMS(InspectionCase ic) throws MalformedURLException, IOException {

		Date now = new Date();
		Date updatedAt = ic.getCreationTime();
		long hours = (now.getTime() - updatedAt.getTime()) / (60 * 60 * 1000);
		String url = urlShortnerService.shortenUrl("customer-inspection/", ic.getId(),
				ic.getInsuranceCompany().getName());
		if (CaseType.OFFLINE.equals(ic.getCaseType())) {
			return sendOfflineCasesSMS(ic, hours, url);
		} else {
			return sendOnlineCasesSMS(ic, hours, url);
		}
	}

	private boolean sendOnlineCasesSMS(InspectionCase ic, long hours, String url)
			throws MalformedURLException, IOException {

		if ("2-wheeler".equals(ic.getVehicleType().getId())) {
			if (hours < appConfigService.getLongProperty("ONLINE_TWO_WHEELER_SEND_SMS_PRIMARY_HOURS_THRESHOLD", 24l)) {
				LOGGER.info("SMS is being sent for to user={} and case={}", ic.getCustomerPhoneNumber(), ic.getId());
				sendSMS(ic.getCustomerPhoneNumber(), "Dear Customer, Self Inspection request received for vehicle "
						+ ic.getVehicleNumber() + " from IFFCO TOKIO. Download application from " + url
						+ " to complete self inspection in next 24 hours. The policy will start only after successful inspection. Call 7290049100 for any support.");
				return true;
			} else {
				LOGGER.info("SMS is being sent for to user={} and case={}", ic.getCustomerPhoneNumber(), ic.getId());
				sendSMS(ic.getCustomerPhoneNumber(), "Dear Customer, Self Inspection request pending for vehicle "
						+ ic.getVehicleNumber() + " from IFFCO Tokio. Download the application from " + url
						+ " to complete self inspection today. The policy will be cancelled and refunded if inspection is not completed today. Call 7290049100 for support.");
				return true;
			}
		} else if ("4-wheeler".equals(ic.getVehicleType().getId())) {
			if (hours < appConfigService.getLongProperty("ONLINE_FOUR_WHEELER_SEND_SMS_PRIMARY_HOURS_THRESHOLD", 48l)) {
				LOGGER.info("SMS is being sent for to user={} and case={}", ic.getCustomerPhoneNumber(), ic.getId());
				sendSMS(ic.getCustomerPhoneNumber(), "Dear Customer, Self Inspection request received for vehicle "
						+ ic.getVehicleNumber() + " from IFFCO TOKIO. Download application from " + url
						+ " to complete self inspection in next 48 hours. The policy will start only after successful inspection. Call 7290049100 for any support.");
				return true;
			} else {
				LOGGER.info("SMS is being sent for to user={} and case={}", ic.getCustomerPhoneNumber(), ic.getId());
				sendSMS(ic.getCustomerPhoneNumber(), "Dear Customer, Self Inspection request pending for vehicle "
						+ ic.getVehicleNumber() + " from IFFCO Tokio. Download the application from " + url
						+ " to complete self inspection today. The policy will be cancelled and refunded if inspection is not completed today. Call 7290049100 for support.");
				return true;
			}
		} else {
			sendSMS(ic.getCustomerPhoneNumber(), "We have an insurance inspection request for " + ic.getVehicleNumber()
					+ ". Please download the app " + url + " to complete the inspection. Call 7290049100 for support");
			LOGGER.info("SMS is being sent for to user={} and case={}", ic.getCustomerPhoneNumber(), ic.getId());
			return true;
		}

	}

	private boolean sendOfflineCasesSMS(InspectionCase ic, long hours, String url)
			throws MalformedURLException, IOException {
		if (hours < appConfigService.getLongProperty("OFFLINE_SEND_SMS_PRIMARY_HOURS_THRESHOLD", 24l)) {
			LOGGER.info("SMS is being sent for to user={} and case={}", ic.getCustomerPhoneNumber(), ic.getId());
			sendSMS(ic.getCustomerPhoneNumber(), "We have an insurance inspection request for " + ic.getVehicleNumber()
					+ ". Please download the app " + url + " to complete the inspection. Call 7290049100 for support");
			return true;
		} else {
			LOGGER.info("SMS is being sent for to user={} and case={}", ic.getCustomerPhoneNumber(), ic.getId());
			sendSMS(ic.getCustomerPhoneNumber(), "Dear Customer, Self Inspection request received for vehicle "
					+ ic.getVehicleNumber() + " from IFFCO TOKIO. Download application from " + url
					+ " to complete self inspection in next 24 hours. The policy will start only after successful inspection. Call 7290049100 for any support.");
			return true;
		}
	}
	
	public String getMessageForSms(Map<String, String> dataMap) {
		String company = dataMap.get(UtilHelper.MSG_COMPANY_NAME_KEY); 
		String msg = null;
		if(null != company) {
			switch(company) {
				case UtilHelper.COMPANY_NAME_SHRIRAM:
					msg = String.format("Dear Customer, Self Inspection request received for vehicle %s from Shri Ram Gen Insurance. Download application from http://bit.ly/SRCust or submit photos online %s to complete inspection. Call  %s for any support.", dataMap.get(UtilHelper.MSG_VEHICLE_NUMBER), dataMap.get(UtilHelper.MSG_URL), dataMap.get(UtilHelper.MSG_SUPPORT_CONTACT_NUMBER_KEY));
				break;
				case UtilHelper.COMPANY_NAME_RAHEJA:
					msg = String.format("Dear Customer, Self Inspection request received for vehicle %s from Raheja QBE. Download application from %s to complete inspection. Call %s for any support. IMP: Confirm Chassis No Location before inspection", dataMap.get(UtilHelper.MSG_VEHICLE_NUMBER), dataMap.get(UtilHelper.MSG_URL), dataMap.get(UtilHelper.MSG_SUPPORT_CONTACT_NUMBER_KEY));
				break;
				case UtilHelper.COMPANY_NAME_SBI:
					msg = String.format("Dear Customer, Self Inspection request received for vehicle %s from SBI General. Download application from %s to complete inspection. Call %s for any support. IMP: Confirm Chassis Number Location before inspection", dataMap.get(UtilHelper.MSG_VEHICLE_NUMBER), dataMap.get(UtilHelper.MSG_URL), dataMap.get(UtilHelper.MSG_SUPPORT_CONTACT_NUMBER_KEY));
				break;
				case UtilHelper.COMPANY_NAME_IFFCO:
					msg = String.format("Dear Customer, Self Inspection request received for vehicle %s from IFFCO Tokio. Download mobile application from %s to complete inspection. Call %s for any support. IMP: Confirm Chassis No. location before inspection", dataMap.get(UtilHelper.MSG_VEHICLE_NUMBER), dataMap.get(UtilHelper.MSG_URL), dataMap.get(UtilHelper.MSG_SUPPORT_CONTACT_NUMBER_KEY));
				break;
				case UtilHelper.COMPANY_NAME_PBACHAT:
					msg = String.format("Dear Customer, Self Inspection request received for vehicle %s from Policy Bachat. Download mobile application from %s to complete inspection. Call %s for any support. IMP: Confirm Chassis No. location before inspection", dataMap.get(UtilHelper.MSG_VEHICLE_NUMBER), dataMap.get(UtilHelper.MSG_URL), dataMap.get(UtilHelper.MSG_SUPPORT_CONTACT_NUMBER_KEY));
				break;
				case UtilHelper.COMPANY_NAME_COVERFOX:
					msg = String.format("Dear Customer, Self Inspection request received for vehicle %s from CoverFox. Download mobile application from %s to complete inspection. Call %s for any support. IMP: Confirm Chassis No. location before inspection", dataMap.get(UtilHelper.MSG_VEHICLE_NUMBER), dataMap.get(UtilHelper.MSG_URL), dataMap.get(UtilHelper.MSG_SUPPORT_CONTACT_NUMBER_KEY));
				break;
				default:
					msg = String.format("Dear Customer, Self Inspection request received for vehicle %s from insurance company. Download mobile application from %s to complete inspection. Call %s for any support. IMP: Confirm Chassis No. location before inspection",dataMap.get(UtilHelper.MSG_VEHICLE_NUMBER), dataMap.get(UtilHelper.MSG_URL), dataMap.get(UtilHelper.MSG_SUPPORT_CONTACT_NUMBER_KEY));
			}
			LOGGER.debug( String.format("Message to be send via sms created ::: %s", msg));
		}
		return msg;
	}

}