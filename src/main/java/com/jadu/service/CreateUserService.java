package com.jadu.service;

import com.jadu.dao.UserDAOImpl;
import com.jadu.helpers.TimeHelper;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.Authority;
import com.jadu.model.Customer;
import com.jadu.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreateUserService {
    
    @Autowired 
    UserDAOImpl userDAO;
    
    public User createCustomer(
            String name,
            String phoneNumber
    ){
        Customer c = new Customer();

        c.setProfilePhotoUrl("'");
        c.setEmail("");
        c.setDeviceId("");
        c.setDeviceLocked(false);
        c.setEmailVerified(true);
        c.setEnabled(true);
        c.setFirstName(name);
        c.setLastName("");
        c.setPassword(phoneNumber);
        c.setPasswordChangeRequired(false);
        c.setPhoneNumber(phoneNumber);
        c.setUsername("USER_" + UtilHelper.getNumericString(5) + TimeHelper.getCurrentTimestampString());
        c.setPhoneNumberVerified(true);


        Authority authority = new Authority();
        authority.setAuthority("ROLE_CUSTOMER");
        authority.setUser(c);

        c.setAuthority(authority);

        userDAO.save(c);
        
        return c;
    }
}
