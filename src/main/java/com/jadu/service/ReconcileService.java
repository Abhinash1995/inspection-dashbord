package com.jadu.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.jadu.dao.CaseCommentDAO;
import com.jadu.dao.CaseDAO;
import com.jadu.model.CaseType;
import com.jadu.model.InspectionCase;
import com.jadu.model.User;

@Service
public class ReconcileService {

	@Autowired
	private CaseDAO caseDAO;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	ConnectCustomerToFlow connectCustomerToFlow;

	@Autowired
	private RedisService redisService;

	@Autowired
	private CaseCommentDAO caseCommentDAO;

	private static final Logger LOGGER = LoggerFactory.getLogger(ReconcileService.class);

	@Async
	public void remindCaseInspection(Long from, Long to, boolean self_inspection, final User user,
			final CaseType caseType, int currentStage) {
		Integer perBatchCallThreshold = appConfigService.getIntProperty("MAX_CALL_PER_BATCH_FOR_" + caseType, 5);
		int jobCount = 0;
		List<InspectionCase> scheduledCases = null;
		if (currentStage == 3 && caseType != null)
			scheduledCases = getScheduledCases(from, to, self_inspection, user, caseType);
		if (currentStage == 4)
			scheduledCases = getHoldCases(from, to, self_inspection, user, caseType);
		Date thresholdDateForOfflineCases = getThresholdDate(
				appConfigService.getIntProperty("THRESHOLD_CASES_DAYS_FOR_AUTOMATED_SYSTEM_CALL_FOR_" + caseType, -7));
		Date thresholdTimeForOnlineCases = getThresholdDate(
				appConfigService.getIntProperty("THRESHOLD_CASES_DAYS_FOR_AUTOMATED_SYSTEM_CALL_FOR_" + caseType, -7));
		/*
		 * Date thresholdTimeForOnlineCases = getFrom( appConfigService.getIntProperty(
		 * "THRESHOLD_CASES_DAYS_FOR_AUTOMATED_SYSTEM_CALL_FOR_" + caseType, 1));
		 */
		for (final InspectionCase scheduledCase : scheduledCases) {
			if (scheduledCase.getCaseType() != null && CaseType.OFFLINE.equals(caseType)
					&& CaseType.OFFLINE.equals(scheduledCase.getCaseType())) {
				try {
					if (scheduledCase.getCreationTime().after(thresholdDateForOfflineCases)) {
						if (checkCallCountCriteria(scheduledCase, caseType)) {
							if (checkCallDifferenceCriteriaBetweenTwoCall(scheduledCase, caseType)) {
								Runnable runnable = new Runnable() {
									public void run() {
										if (appConfigService.getBooleanProperty(
												"IS_TO_RUN_SYSTEM_AUTOMATED_CALL_FOR_" + caseType, false))
											connectCustomerToFlow.reminderCallForScheduledCase(
													scheduledCase.getCustomerPhoneNumber(), scheduledCase, user);
									}
								};

								Thread thread = new Thread(runnable);
								thread.start();
								jobCount++;
								if (!(jobCount <= perBatchCallThreshold)) {
									try {
										// wait for 5 minutes
										thread.sleep(appConfigService
												.getIntProperty("SYSTEM_CALL_SLEEP_TIME_FOR_" + caseType, 300000));
										jobCount = 0;
									} catch (InterruptedException e) {
										e.printStackTrace();
										throw new IllegalStateException(e);
									}
								}
							} else {
								LOGGER.info(
										"case={} has yet not crossed {} hours of time, so automated system call is being skipped",
										scheduledCase.getId(),
										appConfigService.getLongProperty("LAST_CALL_TIME_THRESHOLD_IN_HOURS", 24L));
							}
						} else {
							LOGGER.info(
									"case={} has been already followed up required number of time={}, so automated system call is being skipped",
									scheduledCase.getId(),
									appConfigService.getIntProperty("MAX_FOLLOW_UP_FOR_SCHEDULED_CASES", 4));
						}
					} else {
						LOGGER.info("case={} is older than {}, so automated system call is being skipped",
								scheduledCase.getId(), Math.abs(appConfigService
										.getIntProperty("THRESHOLD_DAYS_FOR_ASSIGN_TO_CUSTOMER_CALL", -7)));
					}

				} catch (Exception e) {
					e.printStackTrace();
					LOGGER.error("Exception raised while system call for case={}, error={}", scheduledCase.getId(),
							e.getMessage());
				}
			} else if (scheduledCase.getCaseType() != null && CaseType.ONLINE.equals(caseType)
					&& CaseType.ONLINE.equals(scheduledCase.getCaseType())) {

				try {
					if (scheduledCase.getCreationTime().after(thresholdTimeForOnlineCases)) {
						if (checkCallCountCriteria(scheduledCase, caseType)) {
							if (checkCallDifferenceCriteriaBetweenTwoCall(scheduledCase, caseType)) {
								Runnable runnable = new Runnable() {
									public void run() {
										if (appConfigService.getBooleanProperty(
												"IS_TO_RUN_SYSTEM_AUTOMATED_CALL_FOR_" + caseType, false))
											connectCustomerToFlow.reminderCallForScheduledCase(
													scheduledCase.getCustomerPhoneNumber(), scheduledCase, user);
									}
								};

								Thread thread = new Thread(runnable);
								thread.start();
								jobCount++;
								if (!(jobCount <= perBatchCallThreshold)) {
									try {
										// wait for 5 minutes
										thread.sleep(appConfigService
												.getIntProperty("SYSTEM_CALL_SLEEP_TIME_FOR_" + caseType, 300000));
										jobCount = 0;
									} catch (InterruptedException e) {
										e.printStackTrace();
										throw new IllegalStateException(e);
									}
								}
							} else {
								LOGGER.info(
										"case={} has yet not crossed {} hours of time, so automated system call is being skipped",
										scheduledCase,
										appConfigService.getLongProperty("LAST_CALL_TIME_THRESHOLD_IN_HOURS", 24L));
							}
						} else {
							LOGGER.info(
									"case={} has been already followed up required number of time={}, so automated system call is being skipped",
									scheduledCase,
									appConfigService.getIntProperty("MAX_FOLLOW_UP_FOR_SCHEDULED_CASES", 4));
						}
					} else {
						LOGGER.info("case={} is older than {}, so automated system call is being skipped",
								scheduledCase, Math.abs(appConfigService
										.getIntProperty("THRESHOLD_DAYS_FOR_ASSIGN_TO_CUSTOMER_CALL", -7)));
					}

				} catch (Exception e) {
					e.printStackTrace();
					LOGGER.error("Exception raised while system call for case={}, error={}", scheduledCase.getId(),
							e.getMessage());
				}

			} else {

				try {
					if (scheduledCase.getCreationTime().after(thresholdDateForOfflineCases)) {
						if (checkCallCountCriteria(scheduledCase, caseType)) {
							if (checkCallDifferenceCriteriaBetweenTwoCall(scheduledCase, caseType)) {
								Runnable runnable = new Runnable() {
									public void run() {
										if (appConfigService.getBooleanProperty(
												"IS_TO_RUN_SYSTEM_AUTOMATED_CALL_FOR_HOLD_CASES", true))
											connectCustomerToFlow.reminderCallForScheduledCase(
													scheduledCase.getCustomerPhoneNumber(), scheduledCase, user);
									}
								};

								Thread thread = new Thread(runnable);
								thread.start();
								jobCount++;
								if (!(jobCount <= perBatchCallThreshold)) {
									try {
										// wait for 5 minutes
										thread.sleep(appConfigService
												.getIntProperty("SYSTEM_CALL_SLEEP_TIME_FOR_" + caseType, 300000));
										jobCount = 0;
									} catch (InterruptedException e) {
										e.printStackTrace();
										throw new IllegalStateException(e);
									}
								}
							} else {
								LOGGER.info(
										"case={} has yet not crossed {} hours of time, so automated system call is being skipped",
										scheduledCase.getId(),
										appConfigService.getLongProperty("LAST_CALL_TIME_THRESHOLD_IN_HOURS", 24L));
							}
						} else {
							LOGGER.info(
									"case={} has been already followed up required number of time={}, so automated system call is being skipped",
									scheduledCase.getId(),
									appConfigService.getIntProperty("MAX_FOLLOW_UP_FOR_SCHEDULED_CASES", 4));
						}
					} else {
						LOGGER.info("case={} is older than {}, so automated system call is being skipped",
								scheduledCase.getId(), Math.abs(appConfigService
										.getIntProperty("THRESHOLD_DAYS_FOR_ASSIGN_TO_CUSTOMER_CALL", -7)));
					}

				} catch (Exception e) {
					e.printStackTrace();
					LOGGER.error("Exception raised while system call for case={}, error={}", scheduledCase.getId(),
							e.getMessage());
				}

			}
		}
		System.gc();

	}

	private List<InspectionCase> getHoldCases(Long from, Long to, boolean self_inspection, User user,
			CaseType caseType) {
		List<InspectionCase> scheduledCases = null;
		if (from != null && to != null) {
			Date fromDate = new Date(from);
			Date toDate = new Date(to);
			scheduledCases = caseDAO.getCaseByStageAndCreatedTime(4, fromDate, toDate);
		} else if (appConfigService.getBooleanProperty("INITIATE_SYSTEM_CALL_FOR_" + caseType + "_SCHEDULED_CASE",
				false)) {
			if (self_inspection) {
				scheduledCases = caseDAO.getCaseByTypeAndStage(4, "SELF_INSPECT");
			} else {
				scheduledCases = caseDAO.getCaseByTypeAndStage(4, "ASSIGN_TO_CUSTOMER");
			}
		}
		return scheduledCases;

	}

	private Date getFrom(Integer from) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.HOUR, -from);
		return calendar.getTime();
	}

	private List<InspectionCase> getScheduledCases(Long from, Long to, boolean self_inspection, User user,
			CaseType caseType) {
		List<InspectionCase> scheduledCases = null;
		if (from != null && to != null) {
			Date fromDate = new Date(from);
			Date toDate = new Date(to);
			if (self_inspection) {
				scheduledCases = caseDAO.getCaseByTypeAndStageAndCreatedTime(3, "SELF_INSPECT", fromDate, toDate,
						caseType.ordinal());
			} else {
				scheduledCases = caseDAO.getCaseByTypeAndStageAndCreatedTime(3, "ASSIGN_TO_CUSTOMER", fromDate, toDate,
						caseType.ordinal());
			}
		} else if (appConfigService.getBooleanProperty("INITIATE_SYSTEM_CALL_FOR_" + caseType + "_SCHEDULED_CASE",
				false)) {
			if (self_inspection) {
				scheduledCases = caseDAO.getCaseByTypeAndStage(3, "SELF_INSPECT", caseType.ordinal());
			} else {
				scheduledCases = caseDAO.getCaseByTypeAndStage(3, "ASSIGN_TO_CUSTOMER", caseType.ordinal());
			}
		}
		return scheduledCases;
	}

	private boolean checkCallDifferenceCriteriaBetweenTwoCall(InspectionCase scheduledCase, CaseType caseType) {
		String lastCallTime = redisService.get("LAST_CALL_TIME_FOR_SCHEDULED_CASE_" + scheduledCase.getId());
		Date lastCallDate = lastCallTime != null ? new Date(Long.valueOf(lastCallTime)) : null;
		return lastCallDate == null || getLastCallTimeDifference(lastCallDate) >= appConfigService
				.getLongProperty("LAST_CALL_TIME_THRESHOLD_IN_HOURS_FOR_" + caseType, 24L);
	}

	private Long getLastCallTimeDifference(Date lastCallDate) {
		long timediff = lastCallDate != null ? ((new Date().getTime() - lastCallDate.getTime()) / (1000 * 60 * 60))
				: 24;
		return timediff;

	}

	private boolean checkCallCountCriteria(InspectionCase scheduledCase, CaseType caseType) {
		int count = getCallCount(scheduledCase);
		return count <= appConfigService.getIntProperty("MAX_FOLLOW_UP_FOR_SCHEDULED_CASES_FOR_" + caseType, 4);
	}

	private int getCallCount(InspectionCase scheduledCase) {
		String followUpCount = redisService.get("FOLLOW_UP_COUNTER_FOR_SCHEDULED_CASE_" + scheduledCase.getId());
		return followUpCount != null ? Integer.parseInt(followUpCount) : 0;
	}

	private Date getThresholdDate(int days) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, days);
		Date thresholdDate = cal.getTime();
		return thresholdDate;
	}

	private Date getThresholdTime(Date from, int hours) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(from);
		calendar.add(Calendar.HOUR, hours);
		return calendar.getTime();
	}

	@Async
	public void closeCaseInspection(Long from, long to, boolean self_inspection, User user, CaseType caseType) {
		try {
			List<InspectionCase> scheduledCases = getScheduledCases(from, to, self_inspection, user, caseType);
			for (InspectionCase inspectionCase : scheduledCases) {
				if (inspectionCase.getCaseType() != null && CaseType.ONLINE == inspectionCase.getCaseType()) {
					if (isInspectionStarted(inspectionCase)) {
						LOGGER.info("Case={} passed inspection started criteria, checking other closing criteria",
								inspectionCase.getId());
						if (checkCallFollowupCriteria(inspectionCase)) {
							LOGGER.info("Case={} passed call followup criteria, checking other closing criteria",
									inspectionCase.getId());
							if (checkThresholdTimeCriteria(inspectionCase)) {
								LOGGER.info("Case={} passed threshold time criteria, checking other closing criteria",
										inspectionCase.getId());
								caseDAO.closeCase(inspectionCase);
								addComments(inspectionCase,
										appConfigService.getProperty("SYSTEM_COMMENTS_FOR_AUTO_CLOSED_CASE",
												"Case has been closed by system"),
										user);
								LOGGER.info("case={} has been closed", inspectionCase.getId());
							}
						}

					} else if (appConfigService.getBooleanProperty("IS_TO_AUTO_CLOSE_INSPECTION_NOT_STARTED_CASE",
							false)) {
						LOGGER.info("Case={} inspection has not been started, checking other closing criteria",
								inspectionCase.getId());
						if (checkThresholdTimeCriteria(inspectionCase)) {
							LOGGER.info(
									"Case={} inspection has not been started but passed threshold time criteria, checking other closing criteria",
									inspectionCase.getId());
							if (checkCallFollowupCriteria(inspectionCase)) {
								LOGGER.info(
										"Case={} inspection has not been started, but passed call followup criteria, checking other closing criteria",
										inspectionCase.getId());
								caseDAO.closeCase(inspectionCase);
								addComments(inspectionCase,
										appConfigService.getProperty("SYSTEM_COMMENTS_FOR_AUTO_CLOSED_CASE",
												"Case has been closed by system"),
										user);
								LOGGER.info("case={} has been closed", inspectionCase.getId());

							}

						}

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception raised while closing case by system, error={}", e.getMessage());
		}
		System.gc();

	}

	private boolean checkThresholdTimeCriteria(InspectionCase inspectionCase) {
		if (appConfigService.getBooleanProperty("IS_TO_CHECK_THRESHOLD_TIME_CRITERIA_FOR_CLOSING_CASE", true)) {
			Date creationTime = inspectionCase.getCreationTime();
			Date thresholdTime = getThresholdTime(creationTime,
					appConfigService.getIntProperty("THRESHOLD_TIME_FOR_CLOSING_CASE_FOR_IFFCO", 3));
			return new Date().after(thresholdTime);
		}
		return true;
	}

	private boolean checkCallFollowupCriteria(InspectionCase inspectionCase) {
		if (appConfigService.getBooleanProperty("IS_TO_CHECK_CALL_COUNT_CRITERIA_FOR_CLOSING_CASE", true)) {
			int callCount = getCallCount(inspectionCase);
			Integer thresholdCallCount = appConfigService.getIntProperty("THRESHOLD_COUNT_FOR_CLOSING_CASE_FOR_IFFCO",
					2);
			return callCount >= thresholdCallCount;
		}
		return true;
	}

	private boolean isInspectionStarted(InspectionCase inspectionCase) {
		return inspectionCase.getInspectionStage() != null && inspectionCase.getInspectionStage() != 0;
	}

	private void addComments(InspectionCase scheduledCase, String comments, User user) {
		try {
<<<<<<< HEAD
			caseCommentDAO.add(scheduledCase, comments, null, user);
=======
			caseCommentDAO.add(scheduledCase, comments, user);
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
			if (appConfigService.getBooleanProperty("IS_TO_UPDATE_COMMENT_IN_CACHE", true))
				redisService.updateCaseComment(scheduledCase, appConfigService.getProperty(
						"SYSTEM_COMMENTS_FOR_AUTOMATED_SCHEDULED_CALLS", "System automated call initiated"), user);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception raised while adding comments to case={} from automated calls, error={}",
					scheduledCase, e.getMessage());
		}
	}

}
