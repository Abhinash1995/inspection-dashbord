package com.jadu.service;

import com.jadu.dto.CaseAttemptAllocationDTO;
import com.jadu.model.Inspector;
import java.util.List;

public class InspectorService {
    public static boolean isInspectorAttempted(List<CaseAttemptAllocationDTO> attempts, Inspector inspector){
        for(CaseAttemptAllocationDTO attempt : attempts){
            if(attempt.getUser().equals(inspector.getUsername()))
                return true;
        }
        
        return false;
    }
}
