package com.jadu.schedule;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.springframework.stereotype.Component;

@Component
public class AllocateInspectorJobBuilder {
    public JobDetail jobDetail() {
        return JobBuilder.newJob().ofType(AllocateInspectorJob.class)
          .storeDurably()
          .withIdentity("Qrtz_Job_Detail")  
          .withDescription("Invoke Sample Job service...")
          .build();
    }
}
