package com.jadu.controller.dto;

import java.io.Serializable;
import java.util.Date;
import org.springframework.beans.factory.annotation.Required;

public class CreateCaseDTO implements Serializable {
	private String purposeOfInspection;
	private String customerName;
	private String customerPhoneNumber;
	private boolean selfInspect;
	private Date date;
	private Double latitude;
	private Double longitude;
	private String vehicleNumber;

	public String getPurposeOfInspection() {
		return purposeOfInspection;
	}

	@Required
	public void setPurposeOfInspection(String purposeOfInspection) {
		this.purposeOfInspection = purposeOfInspection;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPhoneNumber() {
		return customerPhoneNumber;
	}

	public void setCustomerPhoneNumber(String customerPhoneNumber) {
		this.customerPhoneNumber = customerPhoneNumber;
	}

	public boolean isSelfInspect() {
		return selfInspect;
	}

	public void setSelfInspect(boolean selfInspect) {
		this.selfInspect = selfInspect;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

}
