/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.config;

import java.util.TimeZone;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Component;

@Component
public class Configuration {
    
    @PostConstruct
    public void init() {
        System.out.println("############################################");
        System.out.println("Initializing application wise configuration!");
        System.out.println("############################################");
        
        TimeZone.setDefault(TimeZone.getTimeZone("IST"));
    }
}
