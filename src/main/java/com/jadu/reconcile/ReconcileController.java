package com.jadu.reconcile;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jadu.dao.UserDAOImpl;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.model.CaseType;
import com.jadu.model.User;
import com.jadu.service.AppConfigService;
import com.jadu.service.ReconcileService;

@ControllerAdvice
@RestController
@RequestMapping("/reconcile")
public class ReconcileController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReconcileController.class);

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private ReconcileService reconcileService;

	@Autowired
	private UserDAOImpl userDAO;

	@RequestMapping(value = "/appConfig/refresh")
	@ResponseBody
	public ResponseEntity<?> reloadAppConfiguration(
			@RequestParam(value = "secret_key", required = true) String secret_key) {
		if (secret_key.equals(appConfigService.getProperty("APP_CONFIG_RECONCILE_API_PASSWORD", "magic@W!mwisure"))) {
			appConfigService.refresh();
			LOGGER.info("AppConfiguration has been refreshed");
			return new ResponseEntity<>(HttpStatus.OK);

		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

	}

	@RequestMapping(value = "/appConfig/update")
	@ResponseBody
	public ResponseEntity<?> updateAppConfiguration(
			@RequestParam(value = "secret_key", required = true) String secret_key,
			@RequestParam(value = "tag", required = true) String tag,
			@RequestParam(value = "value", required = true) String value) {
		if (secret_key.equals(appConfigService.getProperty("APP_CONFIG_RECONCILE_API_PASSWORD", "magic@W!mwisure"))) {
			appConfigService.updateTag(tag, value);
			LOGGER.info("AppConfiguration has been refreshed");
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

	}

	@RequestMapping(value = "/offline/case/reminder", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> remindCaseInspectionOffline(
			@RequestParam(value = "secret_key", required = true) String secret_key,
			@RequestParam(value = "from", required = false) long from,
			@RequestParam(value = "to", required = false) long to,
			@RequestParam(value = "self_inspection", required = false) boolean self_inspection,
			@RequestParam(value = "case_stage", required = false) int caseStage) {

		if (secret_key.equals(appConfigService.getProperty("APP_CONFIG_RECONCILE_API_PASSWORD", "magic@W!mwisure"))) {
			LOGGER.info("reconcile/offline/case/reminder job started at", new Date());
			CaseType caseType = CaseType.OFFLINE;
			appConfigService.updateTag("IS_TO_RUN_SYSTEM_AUTOMATED_CALL_FOR_" + caseType, "TRUE");
			User user = userDAO
					.getUserByUsername(appConfigService.getProperty("USER_FOR_AUTOMATED_CALLS", "enjoy15rk@gmail.com"));
			reconcileService.remindCaseInspection(from, to, self_inspection, user, caseType, caseStage);
			LOGGER.info("reconcile/offline/case/reminder job finished at ", new Date());
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

	}

	@RequestMapping(value = "/online/case/reminder", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> remindCaseInspectionOnline(
			@RequestParam(value = "secret_key", required = true) String secret_key,
			@RequestParam(value = "from", required = false) long from,
			@RequestParam(value = "to", required = false) long to,
			@RequestParam(value = "self_inspection", required = false) boolean self_inspection,
			@RequestParam(value = "case_stage", required = false) int caseStage) {

		if (secret_key.equals(appConfigService.getProperty("APP_CONFIG_RECONCILE_API_PASSWORD", "magic@W!mwisure"))) {
			LOGGER.info("reconcile/online/case/reminder job started at", new Date());
			CaseType caseType = CaseType.ONLINE;
			appConfigService.updateTag("IS_TO_RUN_SYSTEM_AUTOMATED_CALL_FOR_" + caseType, "TRUE");
			User user = userDAO
					.getUserByUsername(appConfigService.getProperty("USER_FOR_AUTOMATED_CALLS", "enjoy15rk@gmail.com"));
			reconcileService.remindCaseInspection(from, to, self_inspection, user, caseType, caseStage);
			LOGGER.info("reconcile/online/case/reminder job finished at ", new Date());
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

	}

	@RequestMapping(value = "/follow/case", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> remindCompanyCaseInspection(
			@RequestParam(value = "magic_key", required = true) String secret_key,
			@RequestParam(value = "from", required = false) Long from,
			@RequestParam(value = "to", required = false) Long to,
			@RequestParam(value = "self_inspection", required = false) boolean self_inspection,
			@RequestParam(value = "case_stage", required = false) int caseStage) {

		if (secret_key.equals(appConfigService.getProperty("APP_CONFIG_RECONCILE_API_PASSWORD", "magic@W!mwisure"))) {
			LOGGER.info("reconcile/company/case/reminder job started at", new Date());
			CaseType caseType = CaseType.ONLINE;
			appConfigService.updateTag("IS_TO_RUN_SYSTEM_AUTOMATED_CALL_FOR_" + caseType, "TRUE");
			User user = userDAO
					.getUserByUsername(appConfigService.getProperty("USER_FOR_AUTOMATED_CALLS", "enjoy15rk@gmail.com"));
			reconcileService.remindCaseInspection(
					getFrom(appConfigService.getIntProperty("ALL_ONLINE_CASES_IN_LAST_X_HOURS", 3)),
					new Date().getTime(), self_inspection, user, caseType, caseStage);
			LOGGER.info("reconcile/company/case/reminder job finished at ", new Date());
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

	}

	@RequestMapping(value = "/case/close", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> closeCompanyCaseInspection(
			@RequestParam(value = "secret_key", required = true) String secret_key,
			@RequestParam(value = "from", required = false) Long from,
			@RequestParam(value = "to", required = false) Long to,
			@RequestParam(value = "self_inspection", required = false) boolean self_inspection) {

		if (secret_key.equals(appConfigService.getProperty("APP_CONFIG_RECONCILE_API_PASSWORD", "magic@W!mwisure"))) {
			LOGGER.info("reconcile/company/case/close job started at", new Date());
			CaseType caseType = CaseType.ONLINE;
			appConfigService.updateTag("IS_TO_RUN_SYSTEM_AUTO_CLOSE_CASE_FOR_" + caseType, "TRUE");
			User user = userDAO
					.getUserByUsername(appConfigService.getProperty("USER_FOR_AUTOMATED_CALLS", "enjoy15rk@gmail.com"));
			reconcileService.closeCaseInspection(
					getFrom(appConfigService.getIntProperty("CLOSE_COMPANY_CASE_SINCE_LAST_TIME_AS FROM_IN_HOUR", 36)),
					new Date().getTime(), self_inspection, user, caseType);
			LOGGER.info("reconcile/company/case/close job finished at ", new Date());
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

	}

	private Long getFrom(Integer from) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.HOUR, -from);
		return calendar.getTimeInMillis();
	}

	@ExceptionHandler(Exception.class)
	public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
		ErrorHandler.handleError(ex, response);
		System.out.println(ex.getStackTrace());
	}
}
