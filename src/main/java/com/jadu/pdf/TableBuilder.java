package com.jadu.pdf;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

public class TableBuilder {
    private final PdfPTable table;
    private BaseColor backgroundColor = null;
    private BaseColor headerColor = null;
    private final List<CellBuilder> cells =  new ArrayList<>();
    private final int numberOfColumns;
    private int numberOfHeaderColumns = 0;
    private int colspan = 1;
    private int padding = -1;
    
    public TableBuilder(int cols, int widthPercentage, int[] columnWidths) throws DocumentException{
        this.table = new PdfPTable(cols);
        this.table.setWidthPercentage(widthPercentage);
        this.table.setWidths(columnWidths);
        this.numberOfColumns = columnWidths.length;
    }
    
    public TableBuilder(int cols, int widthPercentage) throws DocumentException{
        this.table = new PdfPTable(cols);
        this.table.setWidthPercentage(widthPercentage);
        this.numberOfColumns = cols;
    }
    
    public TableBuilder setBackgroundColor(String colorCode){
        this.backgroundColor = WebColors.getRGBColor(colorCode);
        if(this.headerColor == null)
            this.headerColor = WebColors.getRGBColor(colorCode);
        return this;
    }
    
    public TableBuilder setHeaderColor(String colorCode){
        this.headerColor = WebColors.getRGBColor(colorCode);
        return this;
    }
    
    public TableBuilder setMargin(int margin){
        setMarginBefore(margin);
        setMarginAfter(margin);
        return this;
    }
    
    public TableBuilder setMarginBefore(int margin){
        table.setSpacingBefore(margin);
        return this;
    }
    
    public TableBuilder setMarginAfter(int margin){
        table.setSpacingAfter(margin);
        return this;
    }
    
    public TableBuilder setNumberOfHeaderColumns(int n){
        this.numberOfHeaderColumns = n;
        return this;
    }
    
    public TableBuilder setColspan(int n){
        this.colspan = n;
        return this;
    }
    
    public TableBuilder addPhrase(Phrase phrase, int align){
        addPhrase(phrase, align, -1);
        return this;
    }
    
    public TableBuilder addPhrase(Phrase phrase, int align, int padding){
        Paragraph p = new ParagraphBuilder(phrase)
                .setAlignment(align)
                .build();
        
        cells.add(new CellBuilder()
                        .setBorder(Rectangle.NO_BORDER)
                        .addElement(p)
                        .setPadding(padding));
        
        return this;
    }
    
    public TableBuilder addPhraseVAlign(Phrase phrase, int align, int padding){
        Paragraph p = new ParagraphBuilder(phrase)
                .setAlignment(align)
                .build();
        
        cells.add(new CellBuilder()
                        .setBorder(Rectangle.NO_BORDER)
                        .addElement(p)
                        .setPadding(padding)
                        .setMiddle()
        );
        
        return this;
    }
    
    public TableBuilder addTextColumn(String text, int fontSize, int align){
        
        Phrase phrase = new PhraseBuilder(text).setFont(FontFactory.getFont("ARIAL", fontSize)).build();
        
        return addPhrase(phrase, align);
    }
    
    public TableBuilder addTextColumn(String text, int fontSize, int align, BaseColor color){
        Phrase phrase = new PhraseBuilder(text).setFont(FontFactory.getFont("ARIAL", fontSize, Font.NORMAL, color)).build();
        
        return addPhrase(phrase, align);
    }
    
    public TableBuilder addTextColumn(String text, int fontSize, int align, int padding){
        
        Phrase phrase = new PhraseBuilder(text).setFont(FontFactory.getFont("ARIAL", fontSize)).build();
        
        return addPhrase(phrase, align, padding);
    }
    
    public TableBuilder addTextColumnVAlign(String text, int fontSize, int align, int padding){
        
        Phrase phrase = new PhraseBuilder(text).setFont(FontFactory.getFont("ARIAL", fontSize)).build();
        
        return addPhraseVAlign(phrase, align, padding);
    }
    
    public TableBuilder addTextColumn(String text, int align){
        
        Phrase phrase = new PhraseBuilder(text).setFont(FontFactory.getFont("ARIAL")).build();
        
        return addPhrase(phrase, align);
    }
    
    public TableBuilder addTextColumns(LinkedHashMap<String,String> hm, int fontSize, int align){
        Set<String> keys = hm.keySet();
        for(String k:keys){
            String key = k;
            String value = hm.get(k);
            addTextColumn(key, fontSize, align);
            addTextColumn(value, fontSize, align);
        }
        
        return this;
    }
        
    public TableBuilder addTextColumns(LinkedHashMap<String,String> hm, int fontSize, int alignLeft, int alignRight){
        Set<String> keys = hm.keySet();
        for(String k:keys){
            String key = k;
            String value = hm.get(k);
            addTextColumn(key, fontSize, alignLeft);
            addTextColumn(value, fontSize, alignRight);
        }
        
        return this;
    }
    
    public TableBuilder addImageColumn(String path) throws BadElementException, IOException{
        Image img = Image.getInstance(path);
        
        cells.add(new CellBuilder(img, true)
                .setBorder(Rectangle.NO_BORDER));
        
        return this;
    }
    
    public TableBuilder addImageColumn(byte[] bytes) throws BadElementException, IOException{
        Image img = Image.getInstance(bytes);
        
        cells.add(new CellBuilder(img, true)
                .setBorder(Rectangle.NO_BORDER)
                .setPadding(10)
        );
        
        return this;
    }
    
    public TableBuilder addImageColumn(byte[] bytes, int width, int height) throws BadElementException, IOException{
        Image img = Image.getInstance(bytes);
        //img.scaleAbsolute(width, height);
        //img.setWidthPercentage(50);
        img.setWidthPercentage(100);
        cells.add(new CellBuilder(img)
                .setBorder(Rectangle.NO_BORDER)
                .setPadding(10)
                //.setHeight(height) 
                
        );
        
        return this;
    }
    
    public TableBuilder addTable(PdfPTable pdfTable){
        PdfPCell cell = new PdfPCell(pdfTable);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPadding(5);
        cell.setRight(5);
        cells.add(new CellBuilder(cell));
        
        return this;
    }
    
    public TableBuilder setPadding(int padding){
        this.padding = padding;
        return this;
    }
    
    public PdfPTable build(){
        
        table.getDefaultCell().setPadding(0);
        
        for(int i=0; i<this.cells.size(); i++){
            CellBuilder cell = this.cells.get(i);
            
            if(numberOfHeaderColumns > 0){
                if(i<numberOfHeaderColumns){
                    cell.setBackgroundColor(headerColor);
                    cell.setColSpan(colspan);
                }else
                    cell.setBackgroundColor(this.backgroundColor);
            }else{
                if(i < numberOfColumns){
                    cell.setBackgroundColor(headerColor);
                }else
                    cell.setBackgroundColor(this.backgroundColor);
            }
            
                
            
            PdfPCell pdfCell = cell.build();
            
            if(this.padding != -1)
                pdfCell.setPadding(padding);
            else{
                pdfCell.setPadding(1);
                if((i+1) %table.getNumberOfColumns() != 0)
                    pdfCell.setPaddingRight(3);
                else
                    pdfCell.setPaddingRight(0);
                
            }
            
                
            
            
            table.addCell(pdfCell);
        }
        
        return table;
    }
}
