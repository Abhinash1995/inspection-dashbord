package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "cases_user_allocation")
public class CaseUserAllocation implements Serializable {
	@JsonIgnore
	@Id
	@ManyToOne
	@JoinColumn(name = "case_id", nullable = false)
	private InspectionCase inspectionCase;

	@Id
	@Column(name = "stage")
	private int stage;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@Column(name = "allocation_time")
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date allocationTime;

	public InspectionCase getInspectionCase() {
		return inspectionCase;
	}

	public void setInspectionCase(InspectionCase inspectionCase) {
		this.inspectionCase = inspectionCase;
	}

	public int getStage() {
		return stage;
	}

	public void setStage(int stage) {
		this.stage = stage;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getAllocationTime() {
		return allocationTime;
	}

	public void setAllocationTime(Date allocationTime) {
		this.allocationTime = allocationTime;
	}

	@Override
	public String toString() {
		return "CaseUserAllocation [inspectionCase=" + inspectionCase + ", stage=" + stage + ", user=" + user
				+ ", allocationTime=" + allocationTime + "]";
	}

}
