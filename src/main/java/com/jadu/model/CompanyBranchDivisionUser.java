package com.jadu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "company_branch_division_users")
public class CompanyBranchDivisionUser {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Index(name = "company")
	@Column(name = "company")
	private String company = null;
	@Column(name = "division")
	private String division = null;
	@Column(name = "branch")
	@Index(name = "branch")
	private String branch = null;
	@Column(name = "username")
	@Index(name = "username")
	private String username;
	@Column(name = "state")
	private String state;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "CompanyBranchDivisionUser [id=" + id + ", company=" + company + ", division=" + division + ", branch="
				+ branch + ", username=" + username + ", state=" + state + "]";
	}

}
