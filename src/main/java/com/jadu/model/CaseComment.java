package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
<<<<<<< HEAD
<<<<<<< HEAD
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "cases_comments")
public class CaseComment {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@JsonIgnore
	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.LAZY)
	@JoinColumn(name = "case_id", nullable = false)
	private InspectionCase inspectionCase;

	@JsonIgnore
	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.LAZY)
	@JoinColumn(name = "username", nullable = false)
	private User user;

	@Column(name = "comment")
	private String comment;

	@JsonIgnore
	@OneToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.LAZY)
	@JoinColumn(name = "default_comment_id")
	private DefaultComment defaultComment;

	@Column(name = "comment_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date commentTime;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public InspectionCase getInspectionCase() {
		return inspectionCase;
	}

	public void setInspectionCase(InspectionCase inspectionCase) {
		this.inspectionCase = inspectionCase;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public DefaultComment getDefaultComment() {
		return defaultComment;
	}

	public void setDefaultComment(DefaultComment defaultComment) {
		this.defaultComment = defaultComment;
	}

	public Date getCommentTime() {
		return commentTime;
	}

	public void setCommentTime(Date commentTime) {
		this.commentTime = commentTime;
	}

=======
=======
import javax.persistence.OneToOne;
>>>>>>> inspection-dashbord
import javax.persistence.Table;
import javax.persistence.Temporal;


@Entity
@Table(name="cases_comments")
public class CaseComment {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    
    @JsonIgnore  
    @ManyToOne( cascade = { CascadeType.DETACH }, fetch = FetchType.LAZY)
    @JoinColumn(name="case_id", nullable=false)
    private InspectionCase inspectionCase ;
    
    @JsonIgnore  
    @ManyToOne( cascade = { CascadeType.DETACH }, fetch = FetchType.LAZY)
    @JoinColumn(name="username", nullable=false)
    private User user;
    
    
    @Column(name="comment")
    private String comment;
    
<<<<<<< HEAD
=======
//    @JsonIgnore
//	@OneToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.LAZY)
//	@JoinColumn(name = "default_comment_id")
//	private DefaultComment defaultComment;
>>>>>>> inspection-dashbord
    
    @Column(name="comment_time")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date commentTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public InspectionCase getInspectionCase() {
        return inspectionCase;
    }

    public void setInspectionCase(InspectionCase inspectionCase) {
        this.inspectionCase = inspectionCase;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
<<<<<<< HEAD

    public Date getCommentTime() {
=======
    

	public Date getCommentTime() {
>>>>>>> inspection-dashbord
        return commentTime;
    }

    public void setCommentTime(Date commentTime) {
        this.commentTime = commentTime;
    }
    
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb
}
