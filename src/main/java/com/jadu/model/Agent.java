package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue("AGENT")
public class Agent extends User implements Serializable {
    
    @JsonIgnore
    @OneToOne(mappedBy="user", cascade=CascadeType.ALL)
    private KYC kyc;
    
    @OneToOne(mappedBy="agent", cascade=CascadeType.ALL)
    private AgentDetail agentDetail;

    public KYC getKyc() {
        return kyc;
    }

    public void setKyc(KYC kyc) {
        this.kyc = kyc;
    }

    public AgentDetail getAgentDetails() {
        return agentDetail;
    }

    public void setAgentDetails(AgentDetail agentDetail) {
        this.agentDetail = agentDetail;
    }
}
