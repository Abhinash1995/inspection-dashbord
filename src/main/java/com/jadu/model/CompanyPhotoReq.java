package com.jadu.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="company_photo_req")
public class CompanyPhotoReq implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer  id;
    @ManyToOne
    @JoinColumn(name="company", nullable=false)
    private InsuranceCompany company;
    @ManyToOne
    @JoinColumn(name="vehicle_type", nullable=false)
    private VehicleType vehicleType;
    
    @ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
    @JoinColumn(name="photo_type")
    private PhotoType photoType;
    
    
    public Integer getId() {
        return id;
    }

    public void setCompany(Integer id) {
        this.id = id;
    }

    public InsuranceCompany getCompany() {
        return company;
    }

    public void setCompany(InsuranceCompany company) {
        this.company = company;
    }

    

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public PhotoType getPhotoType() {
        return photoType;
    }

    public void setPhotoType(PhotoType photoType) {
        this.photoType = photoType;
    }
}
