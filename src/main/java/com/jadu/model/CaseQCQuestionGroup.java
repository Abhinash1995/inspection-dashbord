package com.jadu.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="cases_qc_question_groups")
public class CaseQCQuestionGroup implements Serializable {
    
    @Id
    private String id;
    
    @OneToMany(mappedBy="questionGroup", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    private List<CaseQCQuestionOption> options;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<CaseQCQuestionOption> getOptions() {
        return options;
    }

    public void setOptions(List<CaseQCQuestionOption> options) {
        this.options = options;
    }
    
}
