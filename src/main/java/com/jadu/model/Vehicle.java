package com.jadu.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "vehicles")
public class Vehicle implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "type")
	private VehicleType vehicleType;

	@Column(name = "make")
	private String make;

	@Column(name = "model")
	private String model;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "sub_type")
	private VehicleSubType subType;

	@Column(name = "enabled")
	private boolean enabled;

	@Column(name = "created_by")
	private String createdBy;

	public VehicleType getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(VehicleType vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public VehicleSubType getSubType() {
		return subType;
	}

	public void setSubType(VehicleSubType subType) {
		this.subType = subType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public String toString() {
		return "Vehicle [id=" + id + ", vehicleType=" + vehicleType + ", make=" + make + ", model=" + model
				+ ", subType=" + subType + ", enabled=" + enabled + ", createdBy=" + createdBy + "]";
	}

}
