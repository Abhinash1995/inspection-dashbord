package com.jadu.helpers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Arrays;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
<<<<<<< HEAD
import java.io.ByteArrayInputStream;
=======
>>>>>>> 8288f974b4db0c02f5ad73fbd52efd6863d0e3eb

/**
 * Created by Asad Ali on 04/04/2019.
 */
public class CompressionUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(CompressionUtil.class);

    public static byte[] compressString(String s) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        GZIPOutputStream gzipOutputStream = null;
        try {
            gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gzipOutputStream.write(s.getBytes());
            gzipOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            LOGGER.error("Error in compressing for redis: " + e.getMessage());
        }finally {
            if(gzipOutputStream != null){
                try {
                    gzipOutputStream.close();
                } catch (IOException e) {
                    LOGGER.error("Error in compressing for redis: " + e.getMessage());
                }
            }
        }
        return null;
    }

    public static String decompress(byte[] bytes) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream in = null;
        try {
            in = new GZIPInputStream(new ByteArrayInputStream(bytes));
            byte[] buffer = new byte[8192];
            int len;
            while((len = in.read(buffer))>0)
                baos.write(buffer, 0, len);
            return new String(baos.toByteArray());
        } catch (IOException e) {
            LOGGER.error("Error in decompressing from redis: " + e.getMessage());
        }finally {
            if(in != null){
                try {
                    in.close();
                } catch (IOException e) {
                    LOGGER.error("Error in decompressing from redis: " + e.getMessage());
                }
            }
        }
        return null;
    }

    public static void main(String[] args) {
        byte[] tests = compressString("test");
        System.out.println(Arrays.toString(tests));
        System.out.println(decompress(tests));
    }
}
