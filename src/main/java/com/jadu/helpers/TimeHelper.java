package com.jadu.helpers;

import java.sql.Timestamp;

public class TimeHelper {
    public static String getCurrentTimestampString(){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        return String.valueOf(timestamp.getTime());
    }
    
    public static String getTimeString(int duration){
        
        if(duration < 0)
            return "0 minutes";
        
        if(duration <  60)
            return duration + " minutes";
            
        int hour = duration/60;
        int minutes = duration%60;
        
        return (hour > 0 ? (hour > 1 ? hour + " hours" : hour +  " hour") : "")  + (minutes > 0 ? ", " + minutes + " minutes" : "");
    }
}
