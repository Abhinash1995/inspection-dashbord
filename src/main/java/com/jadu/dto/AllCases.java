package com.jadu.dto;

import java.io.Serializable;
import java.util.Date;

public class AllCases implements Serializable {
	private long id;
	private String vehicleNumber;
	private String customerName;
	private String customerPhoneNumber;
	private Date inspectionTime;
	private Date closeTime;
	private int currentStage;
	private String inspectionType;
	private String insuranceCompanyName;
	private String branch;
	private String logoUrl;
	private String requestorPhoneNumber;
	private String remark;
	private long countComments;
	private String downloadKey;
	private String comment;

	public AllCases(long id, String vehicleNumber, String customerName, String customerPhoneNumber, Date inspectionTime,
			Date closeTime, int currentStage, String inspectionType, String insuranceCompanyName, String branch,
			String logoUrl, String requestorPhoneNumber, String remark, String downloadKey, long countComments,
			String comment) {
		this.id = id;
		this.vehicleNumber = vehicleNumber;
		this.customerName = customerName;
		this.customerPhoneNumber = customerPhoneNumber;
		this.inspectionTime = inspectionTime;
		this.closeTime = closeTime;
		this.currentStage = currentStage;
		this.inspectionType = inspectionType;
		this.insuranceCompanyName = insuranceCompanyName;
		this.branch = branch;
		this.logoUrl = logoUrl;
		this.requestorPhoneNumber = requestorPhoneNumber;
		this.remark = remark;
		this.countComments = countComments;
		this.downloadKey = downloadKey;
		this.setComment(comment);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPhoneNumber() {
		return customerPhoneNumber;
	}

	public void setCustomerPhoneNumber(String customerPhoneNumber) {
		this.customerPhoneNumber = customerPhoneNumber;
	}

	public Date getInspectionTime() {
		return inspectionTime;
	}

	public void setInspectionTime(Date inspectionTime) {
		this.inspectionTime = inspectionTime;
	}

	public Date getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(Date closeTime) {
		this.closeTime = closeTime;
	}

	public int getCurrentStage() {
		return currentStage;
	}

	public void setCurrentStage(int currentStage) {
		this.currentStage = currentStage;
	}

	public String getInspectionType() {
		return inspectionType;
	}

	public void setInspectionType(String inspectionType) {
		this.inspectionType = inspectionType;
	}

	public String getInsuranceCompanyName() {
		return insuranceCompanyName;
	}

	public void setInsuranceCompanyName(String insuranceCompanyName) {
		this.insuranceCompanyName = insuranceCompanyName;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getRequestorPhoneNumber() {
		return requestorPhoneNumber;
	}

	public String getRemark() {
		return remark;
	}

	public long getCountComments() {
		return countComments;
	}

	public void setCountComments(long countComments) {
		this.countComments = countComments;
	}

	public String getDownloadKey() {
		return downloadKey;
	}

	public void setDownloadKey(String downloadKey) {
		this.downloadKey = downloadKey;
	}

	public void setRequestorPhoneNumber(String requestorPhoneNumber) {
		this.requestorPhoneNumber = requestorPhoneNumber;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	@Override
	public String toString() {
		return "AllCases [id=" + id + ", vehicleNumber=" + vehicleNumber + ", customerName=" + customerName
				+ ", customerPhoneNumber=" + customerPhoneNumber + ", inspectionTime=" + inspectionTime + ", closeTime="
				+ closeTime + ", currentStage=" + currentStage + ", inspectionType=" + inspectionType
				+ ", insuranceCompanyName=" + insuranceCompanyName + ", branch=" + branch + ", logoUrl=" + logoUrl
				+ ", requestorPhoneNumber=" + requestorPhoneNumber + ", remark=" + remark + ", countComments="
				+ countComments + ", downloadKey=" + downloadKey + "]";
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
