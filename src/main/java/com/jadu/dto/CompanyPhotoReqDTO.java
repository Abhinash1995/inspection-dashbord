/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.dto;

import java.io.Serializable;

/**
 *
 * @author gauta
 */
public class CompanyPhotoReqDTO implements Serializable{
    private Integer id;
    private String company;
    private String vehicleType;
    private String photoType;
    
    public CompanyPhotoReqDTO(
            Integer id,
            String vehicleType,
            String company,
            String photoType
    ){
        this.id = id;
        this.vehicleType = vehicleType;
        this.photoType = photoType;
        this.company = company;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getPhotoType() {
        return photoType;
    }

    public void setPhotoType(String photoType) {
        this.photoType = photoType;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
