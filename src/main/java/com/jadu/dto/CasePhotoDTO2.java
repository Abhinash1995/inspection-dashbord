/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.dto;

import java.io.Serializable;

/**
 *
 * @author gautam
 */
public class CasePhotoDTO2 implements Serializable{
    private String filename;
    private String type;

    public CasePhotoDTO2(String filename, String type) {
        this.filename = filename;
        this.type = type;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFilename() {
        return filename;
    }

    public String getType() {
        return type;
    }
    
    
}
