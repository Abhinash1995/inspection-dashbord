package com.jadu.dto;

import java.io.Serializable;

public class VehiclePhotoReqDTO implements Serializable{
    private Integer id;
    private String vehicleType;
    private String vehicleSubType;
    private String purposeOfInspection;
    private String vehicleFuelType;
    private String photoType;
    
    public VehiclePhotoReqDTO(
            Integer id,
            String vehicleType,
            String vehicleSubType,
            String purposeOfInspection,
            String vehicleFuelType,
            String photoType
    ){
        this.id = id;
        this.vehicleType = vehicleType;
        this.vehicleSubType =  vehicleSubType;
        this.purposeOfInspection = purposeOfInspection;
        this.vehicleFuelType = vehicleFuelType;
        this.photoType = photoType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleSubType() {
        return vehicleSubType;
    }

    public void setVehicleSubType(String vehicleSubType) {
        this.vehicleSubType = vehicleSubType;
    }

    public String getPurposeOfInspection() {
        return purposeOfInspection;
    }

    public void setPurposeOfInspection(String purposeOfInspection) {
        this.purposeOfInspection = purposeOfInspection;
    }

    public String getVehicleFuelType() {
        return vehicleFuelType;
    }

    public void setVehicleFuelType(String vehicleFuelType) {
        this.vehicleFuelType = vehicleFuelType;
    }

    public String getPhotoType() {
        return photoType;
    }

    public void setPhotoType(String photoType) {
        this.photoType = photoType;
    }
    
    
}
