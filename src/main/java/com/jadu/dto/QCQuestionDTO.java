package com.jadu.dto;

import java.io.Serializable;

public class QCQuestionDTO implements Serializable{
    private int id;
    private String value;
    private String vehicleType;
    private String group;
    private String photoTag;
    
    public QCQuestionDTO(int id, String value, String vehicleType, String group, String photoTag){
        this.id = id;
        this.value = value;
        this.vehicleType = vehicleType;
        this.group = group;
        this.photoTag = photoTag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getPhotoTag() {
        return photoTag;
    }

    public void setPhotoTag(String photoTag) {
        this.photoTag = photoTag;
    }
    
    
}
