package com.jadu.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.jadu.model.VehicleType;

public class ScheduledCasesDTO implements Serializable {
	private long id;
	private String vehicleNumber;
	private String customerName;
	private String customerPhoneNumber;
	private Date inspectionTime;
	private Date closeTime;
	private int currentStage;
	private String inspectionType;
	private String insuranceCompanyName;
	private String logoUrl;
	private String requestorPhoneNumber;
	private long countComments;
	private Integer inspectionStage;
	private String vehicleType;
	private String branch;
	private List<CaseCommentDTO> comments;

	public ScheduledCasesDTO(long id, String vehicleNumber, String customerName, String customerPhoneNumber,
			Date inspectionTime, Date closeTime, int currentStage, String inspectionType, String insuranceCompanyName,
			String branch, String logoUrl, String requestorPhoneNumber, Integer inspectionStage, String vehicleType,
			long countComments) {
		this.id = id;
		this.vehicleNumber = vehicleNumber;
		this.customerName = customerName;
		this.customerPhoneNumber = customerPhoneNumber;
		this.inspectionTime = inspectionTime;
		this.closeTime = closeTime;
		this.currentStage = currentStage;
		this.inspectionType = inspectionType;
		this.insuranceCompanyName = insuranceCompanyName;
		this.branch = branch;
		this.logoUrl = logoUrl;
		this.requestorPhoneNumber = requestorPhoneNumber;
		this.inspectionStage = inspectionStage;
		this.vehicleType = vehicleType;
		this.countComments = countComments;
	}

	public ScheduledCasesDTO(long id, String vehicleNumber, String customerName, String customerPhoneNumber,
			Date inspectionTime, Date closeTime, int currentStage, String inspectionType, String insuranceCompanyName,
			String branch, String logoUrl, String requestorPhoneNumber, Integer inspectionStage, String vehicleType,
			long countComments, List<CaseCommentDTO> comments) {
		this.id = id;
		this.vehicleNumber = vehicleNumber;
		this.customerName = customerName;
		this.customerPhoneNumber = customerPhoneNumber;
		this.inspectionTime = inspectionTime;
		this.closeTime = closeTime;
		this.currentStage = currentStage;
		this.inspectionType = inspectionType;
		this.insuranceCompanyName = insuranceCompanyName;
		this.branch = branch;
		this.logoUrl = logoUrl;
		this.requestorPhoneNumber = requestorPhoneNumber;
		this.inspectionStage = inspectionStage;
		this.vehicleType = vehicleType;
		this.countComments = countComments;
		this.comments = comments;
	}

	public List<CaseCommentDTO> getComments() {
		return comments;
	}

	public void setComments(List<CaseCommentDTO> comments) {
		this.comments = comments;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPhoneNumber() {
		return customerPhoneNumber;
	}

	public void setCustomerPhoneNumber(String customerPhoneNumber) {
		this.customerPhoneNumber = customerPhoneNumber;
	}

	public Date getInspectionTime() {
		return inspectionTime;
	}

	public void setInspectionTime(Date inspectionTime) {
		this.inspectionTime = inspectionTime;
	}

	public Date getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(Date closeTime) {
		this.closeTime = closeTime;
	}

	public int getCurrentStage() {
		return currentStage;
	}

	public void setCurrentStage(int currentStage) {
		this.currentStage = currentStage;
	}

	public String getInspectionType() {
		return inspectionType;
	}

	public void setInspectionType(String inspectionType) {
		this.inspectionType = inspectionType;
	}

	public String getInsuranceCompanyName() {
		return insuranceCompanyName;
	}

	public void setInsuranceCompanyName(String insuranceCompanyName) {
		this.insuranceCompanyName = insuranceCompanyName;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getRequestorPhoneNumber() {
		return requestorPhoneNumber;
	}

	public long getCountComments() {
		return countComments;
	}

	public void setRequestorPhoneNumber(String requestorPhoneNumber) {
		this.requestorPhoneNumber = requestorPhoneNumber;
	}

	public void setCountComments(long countComments) {
		this.countComments = countComments;
	}

	public Integer getInspectionStage() {
		return inspectionStage;
	}

	public void setInspectionStage(Integer inspectionStage) {
		this.inspectionStage = inspectionStage;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	@Override
	public String toString() {
		return "ScheduledCasesDTO [id=" + id + ", vehicleNumber=" + vehicleNumber + ", customerName=" + customerName
				+ ", customerPhoneNumber=" + customerPhoneNumber + ", inspectionTime=" + inspectionTime + ", closeTime="
				+ closeTime + ", currentStage=" + currentStage + ", inspectionType=" + inspectionType
				+ ", insuranceCompanyName=" + insuranceCompanyName + ", logoUrl=" + logoUrl + ", requestorPhoneNumber="
				+ requestorPhoneNumber + ", countComments=" + countComments + ", inspectionStage=" + inspectionStage
				+ ", vehicleType=" + vehicleType + ", branch=" + branch + ", comments=" + comments + "]";
	}

}
