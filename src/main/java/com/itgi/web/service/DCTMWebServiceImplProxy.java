package com.itgi.web.service;

public class DCTMWebServiceImplProxy implements com.itgi.web.service.DCTMWebServiceImpl {
  private String _endpoint = null;
  private com.itgi.web.service.DCTMWebServiceImpl dCTMWebServiceImpl = null;
  
  public DCTMWebServiceImplProxy() {
    _initDCTMWebServiceImplProxy();
  }
  
  public DCTMWebServiceImplProxy(String endpoint) {
    _endpoint = endpoint;
    _initDCTMWebServiceImplProxy();
  }
  
  private void _initDCTMWebServiceImplProxy() {
    try {
      dCTMWebServiceImpl = (new com.itgi.web.service.DCTMWebServiceImplServiceLocator()).getDCTMWebServiceImpl();
      if (dCTMWebServiceImpl != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)dCTMWebServiceImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)dCTMWebServiceImpl)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (dCTMWebServiceImpl != null)
      ((javax.xml.rpc.Stub)dCTMWebServiceImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.itgi.web.service.DCTMWebServiceImpl getDCTMWebServiceImpl() {
    if (dCTMWebServiceImpl == null)
      _initDCTMWebServiceImplProxy();
    return dCTMWebServiceImpl;
  }
  
  public com.itgi.web.service.DCTMWebServiceResult upload(com.itgi.web.service.WebContent[] webContents) throws java.rmi.RemoteException{
    if (dCTMWebServiceImpl == null)
      _initDCTMWebServiceImplProxy();
    return dCTMWebServiceImpl.upload(webContents);
  }
  
  public com.itgi.web.service.DCTMWebServiceResult download(com.itgi.web.service.KeyValueMap[] metadata) throws java.rmi.RemoteException{
    if (dCTMWebServiceImpl == null)
      _initDCTMWebServiceImplProxy();
    return dCTMWebServiceImpl.download(metadata);
  }
  
  
}