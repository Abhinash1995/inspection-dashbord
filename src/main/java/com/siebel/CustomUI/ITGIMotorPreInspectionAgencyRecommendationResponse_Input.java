/**
 * ITGIMotorPreInspectionAgencyRecommendationResponse_Input.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.siebel.CustomUI;

public class ITGIMotorPreInspectionAgencyRecommendationResponse_Input  implements java.io.Serializable {
    private java.lang.String preInspectionNumber;

    private java.lang.String recommendationResponse;

    private java.lang.String latitudeLongitude;

    private java.lang.String inspectionLocation;

    private java.lang.String preInspectionIntegrationId;

    private java.lang.String remarks;

    private java.lang.String chassisNumber;

    private java.lang.String inspectionDateTime;

    private java.lang.String engineNumber;

    public ITGIMotorPreInspectionAgencyRecommendationResponse_Input() {
    }

    public ITGIMotorPreInspectionAgencyRecommendationResponse_Input(
           java.lang.String preInspectionNumber,
           java.lang.String recommendationResponse,
           java.lang.String latitudeLongitude,
           java.lang.String inspectionLocation,
           java.lang.String preInspectionIntegrationId,
           java.lang.String remarks,
           java.lang.String chassisNumber,
           java.lang.String inspectionDateTime,
           java.lang.String engineNumber) {
           this.preInspectionNumber = preInspectionNumber;
           this.recommendationResponse = recommendationResponse;
           this.latitudeLongitude = latitudeLongitude;
           this.inspectionLocation = inspectionLocation;
           this.preInspectionIntegrationId = preInspectionIntegrationId;
           this.remarks = remarks;
           this.chassisNumber = chassisNumber;
           this.inspectionDateTime = inspectionDateTime;
           this.engineNumber = engineNumber;
    }


    /**
     * Gets the preInspectionNumber value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @return preInspectionNumber
     */
    public java.lang.String getPreInspectionNumber() {
        return preInspectionNumber;
    }


    /**
     * Sets the preInspectionNumber value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @param preInspectionNumber
     */
    public void setPreInspectionNumber(java.lang.String preInspectionNumber) {
        this.preInspectionNumber = preInspectionNumber;
    }


    /**
     * Gets the recommendationResponse value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @return recommendationResponse
     */
    public java.lang.String getRecommendationResponse() {
        return recommendationResponse;
    }


    /**
     * Sets the recommendationResponse value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @param recommendationResponse
     */
    public void setRecommendationResponse(java.lang.String recommendationResponse) {
        this.recommendationResponse = recommendationResponse;
    }


    /**
     * Gets the latitudeLongitude value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @return latitudeLongitude
     */
    public java.lang.String getLatitudeLongitude() {
        return latitudeLongitude;
    }


    /**
     * Sets the latitudeLongitude value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @param latitudeLongitude
     */
    public void setLatitudeLongitude(java.lang.String latitudeLongitude) {
        this.latitudeLongitude = latitudeLongitude;
    }


    /**
     * Gets the inspectionLocation value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @return inspectionLocation
     */
    public java.lang.String getInspectionLocation() {
        return inspectionLocation;
    }


    /**
     * Sets the inspectionLocation value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @param inspectionLocation
     */
    public void setInspectionLocation(java.lang.String inspectionLocation) {
        this.inspectionLocation = inspectionLocation;
    }


    /**
     * Gets the preInspectionIntegrationId value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @return preInspectionIntegrationId
     */
    public java.lang.String getPreInspectionIntegrationId() {
        return preInspectionIntegrationId;
    }


    /**
     * Sets the preInspectionIntegrationId value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @param preInspectionIntegrationId
     */
    public void setPreInspectionIntegrationId(java.lang.String preInspectionIntegrationId) {
        this.preInspectionIntegrationId = preInspectionIntegrationId;
    }


    /**
     * Gets the remarks value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @return remarks
     */
    public java.lang.String getRemarks() {
        return remarks;
    }


    /**
     * Sets the remarks value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @param remarks
     */
    public void setRemarks(java.lang.String remarks) {
        this.remarks = remarks;
    }


    /**
     * Gets the chassisNumber value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @return chassisNumber
     */
    public java.lang.String getChassisNumber() {
        return chassisNumber;
    }


    /**
     * Sets the chassisNumber value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @param chassisNumber
     */
    public void setChassisNumber(java.lang.String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }


    /**
     * Gets the inspectionDateTime value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @return inspectionDateTime
     */
    public java.lang.String getInspectionDateTime() {
        return inspectionDateTime;
    }


    /**
     * Sets the inspectionDateTime value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @param inspectionDateTime
     */
    public void setInspectionDateTime(java.lang.String inspectionDateTime) {
        this.inspectionDateTime = inspectionDateTime;
    }


    /**
     * Gets the engineNumber value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @return engineNumber
     */
    public java.lang.String getEngineNumber() {
        return engineNumber;
    }


    /**
     * Sets the engineNumber value for this ITGIMotorPreInspectionAgencyRecommendationResponse_Input.
     * 
     * @param engineNumber
     */
    public void setEngineNumber(java.lang.String engineNumber) {
        this.engineNumber = engineNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ITGIMotorPreInspectionAgencyRecommendationResponse_Input)) return false;
        ITGIMotorPreInspectionAgencyRecommendationResponse_Input other = (ITGIMotorPreInspectionAgencyRecommendationResponse_Input) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.preInspectionNumber==null && other.getPreInspectionNumber()==null) || 
             (this.preInspectionNumber!=null &&
              this.preInspectionNumber.equals(other.getPreInspectionNumber()))) &&
            ((this.recommendationResponse==null && other.getRecommendationResponse()==null) || 
             (this.recommendationResponse!=null &&
              this.recommendationResponse.equals(other.getRecommendationResponse()))) &&
            ((this.latitudeLongitude==null && other.getLatitudeLongitude()==null) || 
             (this.latitudeLongitude!=null &&
              this.latitudeLongitude.equals(other.getLatitudeLongitude()))) &&
            ((this.inspectionLocation==null && other.getInspectionLocation()==null) || 
             (this.inspectionLocation!=null &&
              this.inspectionLocation.equals(other.getInspectionLocation()))) &&
            ((this.preInspectionIntegrationId==null && other.getPreInspectionIntegrationId()==null) || 
             (this.preInspectionIntegrationId!=null &&
              this.preInspectionIntegrationId.equals(other.getPreInspectionIntegrationId()))) &&
            ((this.remarks==null && other.getRemarks()==null) || 
             (this.remarks!=null &&
              this.remarks.equals(other.getRemarks()))) &&
            ((this.chassisNumber==null && other.getChassisNumber()==null) || 
             (this.chassisNumber!=null &&
              this.chassisNumber.equals(other.getChassisNumber()))) &&
            ((this.inspectionDateTime==null && other.getInspectionDateTime()==null) || 
             (this.inspectionDateTime!=null &&
              this.inspectionDateTime.equals(other.getInspectionDateTime()))) &&
            ((this.engineNumber==null && other.getEngineNumber()==null) || 
             (this.engineNumber!=null &&
              this.engineNumber.equals(other.getEngineNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPreInspectionNumber() != null) {
            _hashCode += getPreInspectionNumber().hashCode();
        }
        if (getRecommendationResponse() != null) {
            _hashCode += getRecommendationResponse().hashCode();
        }
        if (getLatitudeLongitude() != null) {
            _hashCode += getLatitudeLongitude().hashCode();
        }
        if (getInspectionLocation() != null) {
            _hashCode += getInspectionLocation().hashCode();
        }
        if (getPreInspectionIntegrationId() != null) {
            _hashCode += getPreInspectionIntegrationId().hashCode();
        }
        if (getRemarks() != null) {
            _hashCode += getRemarks().hashCode();
        }
        if (getChassisNumber() != null) {
            _hashCode += getChassisNumber().hashCode();
        }
        if (getInspectionDateTime() != null) {
            _hashCode += getInspectionDateTime().hashCode();
        }
        if (getEngineNumber() != null) {
            _hashCode += getEngineNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ITGIMotorPreInspectionAgencyRecommendationResponse_Input.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://siebel.com/CustomUI", ">ITGIMotorPreInspectionAgencyRecommendationResponse_Input"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("preInspectionNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://siebel.com/CustomUI", "PreInspectionNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recommendationResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://siebel.com/CustomUI", "RecommendationResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitudeLongitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://siebel.com/CustomUI", "LatitudeLongitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inspectionLocation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://siebel.com/CustomUI", "InspectionLocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("preInspectionIntegrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://siebel.com/CustomUI", "PreInspectionIntegrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("remarks");
        elemField.setXmlName(new javax.xml.namespace.QName("http://siebel.com/CustomUI", "Remarks"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chassisNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://siebel.com/CustomUI", "ChassisNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inspectionDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://siebel.com/CustomUI", "InspectionDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("engineNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://siebel.com/CustomUI", "EngineNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
