/**
 * ITGI_spcMotor_spcPreInspection_spcAgency_spcResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.siebel.CustomUI;

public interface ITGI_spcMotor_spcPreInspection_spcAgency_spcResponse extends javax.xml.rpc.Service {
    public java.lang.String getITGIMotorPreInspectionAgencyResponseAddress();

    public com.siebel.CustomUI.ITGIMotorPreInspectionAgencyResponse getITGIMotorPreInspectionAgencyResponse() throws javax.xml.rpc.ServiceException;

    public com.siebel.CustomUI.ITGIMotorPreInspectionAgencyResponse getITGIMotorPreInspectionAgencyResponse(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
