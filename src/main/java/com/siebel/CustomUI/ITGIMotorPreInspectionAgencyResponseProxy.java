package com.siebel.CustomUI;

public class ITGIMotorPreInspectionAgencyResponseProxy implements com.siebel.CustomUI.ITGIMotorPreInspectionAgencyResponse {
  private String _endpoint = null;
  private com.siebel.CustomUI.ITGIMotorPreInspectionAgencyResponse iTGIMotorPreInspectionAgencyResponse = null;
  
  public ITGIMotorPreInspectionAgencyResponseProxy() {
    _initITGIMotorPreInspectionAgencyResponseProxy();
  }
  
  public ITGIMotorPreInspectionAgencyResponseProxy(String endpoint) {
    _endpoint = endpoint;
    _initITGIMotorPreInspectionAgencyResponseProxy();
  }
  
  private void _initITGIMotorPreInspectionAgencyResponseProxy() {
    try {
      iTGIMotorPreInspectionAgencyResponse = (new com.siebel.CustomUI.ITGI_spcMotor_spcPreInspection_spcAgency_spcResponseLocator()).getITGIMotorPreInspectionAgencyResponse();
      if (iTGIMotorPreInspectionAgencyResponse != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iTGIMotorPreInspectionAgencyResponse)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iTGIMotorPreInspectionAgencyResponse)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iTGIMotorPreInspectionAgencyResponse != null)
      ((javax.xml.rpc.Stub)iTGIMotorPreInspectionAgencyResponse)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.siebel.CustomUI.ITGIMotorPreInspectionAgencyResponse getITGIMotorPreInspectionAgencyResponse() {
    if (iTGIMotorPreInspectionAgencyResponse == null)
      _initITGIMotorPreInspectionAgencyResponseProxy();
    return iTGIMotorPreInspectionAgencyResponse;
  }
  
  public com.siebel.CustomUI.ITGIMotorPreInspectionAgencyRecommendationResponse_Output ITGIMotorPreInspectionAgencyRecommendationResponse(com.siebel.CustomUI.ITGIMotorPreInspectionAgencyRecommendationResponse_Input ITGIMotorPreInspectionAgencyRecommendationResponse_Input) throws java.rmi.RemoteException{
    if (iTGIMotorPreInspectionAgencyResponse == null)
      _initITGIMotorPreInspectionAgencyResponseProxy();
    return iTGIMotorPreInspectionAgencyResponse.ITGIMotorPreInspectionAgencyRecommendationResponse(ITGIMotorPreInspectionAgencyRecommendationResponse_Input);
  }
  
  
}