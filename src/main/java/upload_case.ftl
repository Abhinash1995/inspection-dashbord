<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cus="http://siebel.com/CustomUI">
    <soapenv:Header />
    <soapenv:Body>
        <cus:ITGIMotorPreInspectionAgencyRecommendationResponse_Input>
            <cus:PreInspectionNumber>${PreInspectionNumber}</cus:PreInspectionNumber>
            <cus:RecommendationResponse>${RecommendationResponse}</cus:RecommendationResponse>
            <cus:LatitudeLongitude>${LatitudeLongitude}</cus:LatitudeLongitude>
            <cus:InspectionLocation>${InspectionLocation}</cus:InspectionLocation>
            <cus:PreInspectionIntegrationId>${PreInspectionIntegrationId}</cus:PreInspectionIntegrationId>
            <cus:Remarks>${Remarks}</cus:Remarks>
            <cus:ChassisNumber>${ChassisNumber}</cus:ChassisNumber>
            <cus:InspectionDateTime>${InspectionDateTime}</cus:InspectionDateTime>
            <cus:EngineNumber>${EngineNumber}</cus:EngineNumber>
        </cus:ITGIMotorPreInspectionAgencyRecommendationResponse_Input>
    </soapenv:Body>
</soapenv:Envelope>